//
//  MPIRank__2_.h
//  MPIKit
//
//  Created by KYUNGGUK MIN on 7/29/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef MPIRank__2__h
#define MPIRank__2__h

#include <MPIKit/MPIKit-config.h>

// MARK:- Version 2
//
#if defined(__APPLE__)
#include <mpich2/mpi.h>
#else
#include <mpi.h>
#endif

MPIKIT_BEGIN_NAMESPACE
#if defined(MPIKIT_INLINE_VERSION) && MPIKIT_INLINE_VERSION == 2
inline
#endif
namespace __2_ {
    /**
     @brief Light-weight wrapper for MPI rank.
     @discussion On construction, no range check is performed for the given rank.

     The range of valid values for rank is 0 <= rank <= n − 1 and MPI_PROC_NULL, where n is the number of processes in the group.
     */
    class Rank {
        int _rank{MPI_UNDEFINED};

    public:
        constexpr operator int() const noexcept { return _rank; }
        int const &operator*() const noexcept { return _rank; }
        int       &operator*()       noexcept { return _rank; }
        int const *operator&() const noexcept { return &_rank; }
        int       *operator&()       noexcept { return &_rank; }

        explicit constexpr Rank() noexcept = default;
        explicit constexpr Rank(int const rank) noexcept : _rank(rank) {}
        static constexpr Rank any() noexcept { return Rank{MPI_ANY_SOURCE}; }
        static constexpr Rank null() noexcept { return Rank{MPI_PROC_NULL}; } // used to define a "dummy" destination or source in any send or receive call
    };
} // namespace __2_
MPIKIT_END_NAMESPACE

#endif /* MPIRank__2__h */
