//
//  MPIComm__2_.cc
//  MPIKit
//
//  Created by KYUNGGUK MIN on 7/30/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "MPIComm__2_.h"
#include <stdexcept>
#include <string>

// MARK:- MPK::__2_::Comm
//
auto MPK::__2_::Comm::split(int const color, int const key) const
-> Comm {
    MPI_Comm c;
    if (MPI_Comm_split(**this, color, key, &c) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Comm_split(...) returned error");
    }
    return Comm{std::move(c)}; // custom error handler is set on construction
}

auto MPK::__2_::Comm::duplicated() const
-> Comm {
    MPI_Comm c;
    if (MPI_Comm_dup(**this, &c) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Comm_dup(...) returned error");
    }
    return Comm{std::move(c)}; // custom error handler is set on construction
}

auto MPK::__2_::Comm::created(Group const &g) const
-> Comm {
    MPI_Comm c;
    if (MPI_Comm_create(**this, *g, &c) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Comm_create(...) returned error");
    }
    return Comm{std::move(c)}; // custom error handler is set on construction
}
