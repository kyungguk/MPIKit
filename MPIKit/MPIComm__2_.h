//
//  MPIComm__2_.h
//  MPIKit
//
//  Created by KYUNGGUK MIN on 7/30/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef MPIComm__2__h
#define MPIComm__2__h

#include <MPIKit/MPIKit-config.h>

// MARK:- Version 2
//
#include <MPIKit/MPIBaseComm__2_.h>
#include <MPIKit/MPIP2P__2_.h>
#include <MPIKit/MPICollective__2_.h>
#include <MPIKit/MPIReduction__2_.h>

MPIKIT_BEGIN_NAMESPACE
#if defined(MPIKIT_INLINE_VERSION) && MPIKIT_INLINE_VERSION == 2
inline
#endif
namespace __2_ {
    using Communicator = Comm;

    /**
     @brief Wrapper for MPI_Comm.
     @discussion Only intracommunicator is supported.
     */
    class Comm : public _Comm, public P2P<Comm>, public Collective<Comm>, public Reduction<Comm> {
        explicit Comm(MPI_Comm const c, decltype(nullptr)) noexcept : _Comm(c, nullptr) {} // this is for predefined comm's
        explicit Comm(MPI_Comm &&c) : _Comm(std::move(c)) {} // custom error handler is set

    public:
        // inhereted:
        explicit Comm() noexcept = default;
        Comm(Comm &&o) noexcept : _Comm(std::move(o)) {}
        Comm &operator=(Comm &&o) noexcept { _Comm::operator=(std::move(o)); return *this; }
        using _Comm::swap;

        /**
         @brief Returns an instance wrapping MPI_COMM_WORLD.
         @discussion No custom error handler is set.
         */
        static Comm world() noexcept { return Comm{MPI_COMM_WORLD, nullptr}; }

        /**
         @brief Returns an instance wrapping MPI_COMM_SELF.
         @discussion No custom error handler is set.
         */
        static Comm self() noexcept { return Comm{MPI_COMM_SELF, nullptr}; }

#pragma mark Derived Comm
        /**
         @brief Returns an instance by partitioning the group associated with *this into disjoint subgroups, one for each value of color.
         @discussion Each subgroup contains all processes of the same color.
         Within each subgroup, the processes are ranked in the order defined by the value of the argument key, with ties broken according to their rank in the old group.

         A new communicator is created for each subgroup.

         A process may supply the color value MPI_UNDEFINED, in which case newcomm returns MPI_COMM_NULL.

         This is a collective call, but each process is permitted to provide different values for color and key.
         @param color Non-negative integer or MPI_UNDEFINED.
         @param key Process ordering hint.
         */
        Comm split(int const color, int const key) const;
        Comm split(int const color) const { return split(color, this->rank()); }

        /**
         @brief Returns an instance by duplicating *this.
         */
        Comm duplicated() const;

        /**
         @brief Construct a new communicator with communication group defined by the group argument.
         @discussion Each process must call this with a group argument that is a subgroup of the group associated with comm; this could be MPI_GROUP_EMPTY.
         The processes may specify different values for the group argument.

         If a process calls with a non-empty group then all processes in that group must call the function with the same group as argument, that is the same processes in the same order.
         Otherwise, the call is erroneous.
         This implies that the set of groups specified across the processes must be disjoint.
         If the calling process is a member of the group given as group argument, then the instance is a communicator with group as its associated group.

         In the case that a process calls with a group to which it does not belong, e.g., MPI_GROUP_EMPTY, then an instance with MPI_COMM_NULL is returned as newcomm.

         This call is collective and must be called by all processes in the group of comm.
         */
        Comm created(Group const &g) const;
    };
} // namespace __2_
MPIKIT_END_NAMESPACE

#endif /* MPIComm__2__h */
