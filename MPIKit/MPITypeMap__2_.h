//
//  MPITypeMap__2_.h
//  MPIKit
//
//  Created by KYUNGGUK MIN on 7/30/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef MPITypeMap__2__h
#define MPITypeMap__2__h

#include <MPIKit/MPIKit-config.h>

// MARK:- Version 2
//
#include <MPIKit/MPIType__2_.h>
#include <UtilityKit/UtilityKit.h>
#include <type_traits>
#include <utility>
#include <tuple>
#include <array>

MPIKIT_BEGIN_NAMESPACE
#if defined(MPIKIT_INLINE_VERSION) && MPIKIT_INLINE_VERSION == 2
inline
#endif
namespace __2_ {
    /**
     @brief std::type_traits-like typemapping facility.
     @discussion For a given type `T', requirements are:

     1) Make `TypeMap<T>' class default-constructible;

     2) Define `static [cv-qualifier] Type [&] value()' that returns the Type instance corresponding to `T';

     3) All subsequent call of `TypeMap<T>::value()' must return a Type instance whose typemap is equivalent to that of the first call
     (that is, any operations using all Type instances returned from this call are expected to behave the same as using any of the Type instances);

     4) Optionally, declare type alias `type = T'.
     */
    template <class T> struct TypeMap { TypeMap() = delete; };

    /**
     @brief Construct a Type instance for the given type `T' using the TypeMap facility.
     @discussion The argument, if any, is untouched.
     */
    template <class T>
    auto make_type() -> decltype(TypeMap<typename std::decay<T>::type>::value()) {
        return TypeMap<typename std::decay<T>::type>::value();
    }
    template <class T>
    auto make_type(T const&) -> decltype(TypeMap<typename std::decay<T>::type>::value()) {
        return TypeMap<typename std::decay<T>::type>::value();
    }
    template <class T, long N>
    Type make_type(T const (&)[N]) { // note rvalue return
        return TypeMap<typename std::decay<T>::type>::value().template vector<N>();
    }

    /**
     @brief Composition of multiple types.
     @discussion The arguments, if any, are untouched.
     */
    template <class... Types> typename std::enable_if<(sizeof...(Types) > 1),
    Type>::type make_type() { return { make_type<Types>()... }; }
    template <class... Types> typename std::enable_if<(sizeof...(Types) > 1),
    Type>::type make_type(Types&&... args) { return { make_type<Types>(std::forward<Types>(args))... }; }

    // MARK: Native Types
    //
    // native (unsigned) char
    //
    template <> struct TypeMap<char> {
        using type = char;
        static Type value() noexcept { return Type::native<type>(); }
    };
    template <> struct TypeMap<unsigned char> {
        using type = unsigned char;
        static Type value() noexcept { return Type::native<type>(); }
    };

    // native (unsigned) short
    //
    template <> struct TypeMap<short> {
        using type = short;
        static Type value() noexcept { return Type::native<type>(); }
    };
    template <> struct TypeMap<unsigned short> {
        using type = unsigned short;
        static Type value() noexcept { return Type::native<type>(); }
    };
    
    // native (unsigned) int
    //
    template <> struct TypeMap<int> {
        using type = int;
        static Type value() noexcept { return Type::native<type>(); }
    };
    template <> struct TypeMap<unsigned> {
        using type = unsigned;
        static Type value() noexcept { return Type::native<type>(); }
    };

    // native (unsigned) long
    //
    template <> struct TypeMap<long> {
        using type = long;
        static Type value() noexcept { return Type::native<type>(); }
    };
    template <> struct TypeMap<unsigned long> {
        using type = unsigned long;
        static Type value() noexcept { return Type::native<type>(); }
    };

    // native float/double
    //
    template <> struct TypeMap<float> {
        using type = float;
        static Type value() noexcept { return Type::native<type>(); }
    };
    template <> struct TypeMap<double> {
        using type = double;
        static Type value() noexcept { return Type::native<type>(); }
    };

    // MARK: UTL::Vector
    //
    template <class T, long S> struct TypeMap<UTL::Vector<T, S>> {
        using type = UTL::Vector<T, S>;
        static Type value() noexcept { return TypeMap<T>::value().template vector<S>(); }
    };

    // MARK: UTL::SIMDVector
    //
    template <class T, long S> struct TypeMap<UTL::SIMDVector<T, S>> {
        using type = UTL::SIMDVector<T, S>;
        static Type value() noexcept { return TypeMap<T>::value().template vector<S>().template realigned<alignof(type)>(); }
    };
    template <class T        > struct TypeMap<UTL::SIMDVector<T, 3>> {
        using type = UTL::SIMDVector<T, 3>; // be careful with padding
        static Type value() noexcept { return TypeMap<T>::value().template vector<type::size()>().template realigned<alignof(type)>(); }
    };

    // MARK: UTL::SIMDComplex
    //
    template <class T> struct TypeMap<UTL::SIMDComplex<T>> {
        using type = UTL::SIMDComplex<T>;
        static Type value() noexcept { return TypeMap<T>::value().template vector<sizeof(type)/sizeof(T)>().template realigned<alignof(type)>(); }
    };

    // MARK: std::complex
    //
    template <class T> struct TypeMap<std::complex<T>> {
        using type = std::complex<T>;
        static Type value() noexcept { return TypeMap<T>::value().template vector<sizeof(type)/sizeof(T)>(); }
    };

    // MARK: std::array
    //
    template <class T, unsigned long S> struct TypeMap<std::array<T, S>> {
        using type = std::array<T, S>;
        static Type value() noexcept { return TypeMap<T>::value().template vector<S>(); }
    };

    // MARK: std::tuple
    //
    template <class... Ts> struct TypeMap<std::tuple<Ts...>> {
        using type = std::tuple<Ts...>;
        static Type value() noexcept { return Type{TypeMap<Ts>::value()...}; }
    };

    // MARK: std::pair
    //
    template <class T, class U> struct TypeMap<std::pair<T, U>> : public TypeMap<std::tuple<T, U>> {
        using type = std::pair<T, U>;
    };
} // namespace __2_
MPIKIT_END_NAMESPACE

#endif /* MPITypeMap__2__h */
