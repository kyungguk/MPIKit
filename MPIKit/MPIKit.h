//
//  MPIKit.h
//  MPIKit
//
//  Created by KYUNGGUK MIN on 11/12/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#if defined(__APPLE__) && defined(__OBJC__)
#import <Cocoa/Cocoa.h>

//! Project version number for MPIKit.
FOUNDATION_EXPORT double MPIKitVersionNumber;

//! Project version string for MPIKit.
FOUNDATION_EXPORT const unsigned char MPIKitVersionString[];
#endif

// In this header, you should import all the public headers of your framework using statements like #import <MPIKit/PublicHeader.h>


#if defined(__cplusplus)


// default version selection
//
#if !defined(MPIKIT_INLINE_VERSION)
#define MPIKIT_INLINE_VERSION 2
#endif


// Version 1
//
#include <MPIKit/MPITypeMap.h>
#include <MPIKit/MPIEnvelope.h>
#include <MPIKit/MPIRequest.h>
#include <MPIKit/MPICommunicator.h>


// Version 2
//
#include <MPIKit/MPIType__2_.h>
#include <MPIKit/MPITypeMap__2_.h>

#include <MPIKit/MPITag__2_.h>
#include <MPIKit/MPIRank__2_.h>

#include <MPIKit/MPIRequest__2_.h>
#include <MPIKit/MPIGroup__2_.h>
#include <MPIKit/MPIBaseComm__2_.h>
#include <MPIKit/MPIComm__2_.h>
#include <MPIKit/MPIP2P__2_.h>
#include <MPIKit/MPICollective__2_.h>
#include <MPIKit/MPIReduction__2_.h>


// TODO: Remove this namespace alias.
namespace MPIK = MPK;


#endif
