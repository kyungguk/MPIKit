//
//  MPITag__2_.h
//  MPIKit
//
//  Created by KYUNGGUK MIN on 7/29/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef MPITag__2__h
#define MPITag__2__h

#include <MPIKit/MPIKit-config.h>

// MARK:- Version 2
//
#if defined(__APPLE__)
#include <mpich2/mpi.h>
#else
#include <mpi.h>
#endif

MPIKIT_BEGIN_NAMESPACE
#if defined(MPIKIT_INLINE_VERSION) && MPIKIT_INLINE_VERSION == 2
inline
#endif
namespace __2_ {
    /**
     @brief Light-weight wrapper for MPI tag.
     @discussion On construction, no range check is performed for the given tag.

     The range of valid tag values is 0 <= tag <= MPI_TAG_UB.

     MPI requires that MPI_TAG_UB be no less than 32767.
     */
    class Tag {
        int _tag{MPI_UNDEFINED};

    public:
        constexpr operator int() const noexcept { return _tag; }
        int const &operator*() const noexcept { return _tag; }
        int       &operator*()       noexcept { return _tag; }
        int const *operator&() const noexcept { return &_tag; }
        int       *operator&()       noexcept { return &_tag; }

        explicit constexpr Tag() noexcept = default;
        explicit constexpr Tag(int const tag) noexcept : _tag(tag) {}
        static constexpr Tag any() noexcept { return Tag{MPI_ANY_TAG}; }
    };
} // namespace __2_
MPIKIT_END_NAMESPACE

#endif /* MPITag__2__h */
