//
//  MPIGroup__2_.h
//  MPIKit
//
//  Created by KYUNGGUK MIN on 7/29/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef MPIGroup__2__h
#define MPIGroup__2__h

#include <MPIKit/MPIKit-config.h>

// MARK:- Version 2
//
#include <MPIKit/MPIRank__2_.h>
#include <UtilityKit/UtilityKit.h>
#include <type_traits>
#include <iterator>
#include <utility>

#if defined(__APPLE__)
#include <mpich2/mpi.h>
#else
#include <mpi.h>
#endif

MPIKIT_BEGIN_NAMESPACE
#if defined(MPIKIT_INLINE_VERSION) && MPIKIT_INLINE_VERSION == 2
inline
#endif
namespace __2_ {
    class _Comm;

    /**
     @brief Wrapper for MPI_Group.
     @discussion A group is an ordered set of process identifiers (henceforth processes).
     Each process in a group is associated with an integer rank.
     Ranks are contiguous and start from zero.

     A group is used within a communicator to describe the participants in a communication "universe" and to rank such participants.

     There is a special pre-defined group: MPI_GROUP_EMPTY, which is a group with no members.
     The predefined constant MPI_GROUP_NULL is the value used for invalid group handles.
     */
    class Group {
        friend _Comm;

        MPI_Group _g{MPI_GROUP_NULL};

        explicit Group(MPI_Group &&g) noexcept { std::swap(_g, g); }
    public:
        ~Group();

        /**
         @brief Create an empty group.
         */
        constexpr explicit Group() noexcept : _g(MPI_GROUP_EMPTY) {}

        // move
        Group(Group &&o) noexcept : Group() { swap(o); }
        Group &operator=(Group &&) noexcept;

        /**
         @brief Swap the content of *this with that of other.
         */
        void swap(Group &o) noexcept { std::swap(_g, o._g); }

#pragma mark Query
        /**
         @brief Returns true if *this is valid, i.e., not MPI_GROUP_NULL.
         */
        explicit operator bool() const noexcept { return _g != MPI_GROUP_NULL; }

        /**
         @brief Returns the associated MPI_Group handle.
         */
        MPI_Group const &operator*() const noexcept { return _g; }

        /**
         @brief Determines the number of processes in the group associated with *this.
         */
        int size() const;

        /**
         @brief Determines the rank of the calling process in the group associated with *this.
         @discussion A nil optional is returned if the calling process is not a member of *this.
         */
        UTL::Optional<Rank> rank() const;

        /**
         @brief Determines the relative numbering of the same process in two different groups.
         @discussion Rank::null is a valid rank argument for which the translated rank is also Rank::null.
         @param a_rank The rank has to be non-netative and less than this->size(), or Rank::null().
         */
        UTL::Optional<Rank> translate(Rank const a_rank, Group const &to) const;
        UTL::Optional<Rank> translate(Rank const a_rank, _Comm const &to) const;

        /**
         @brief Comparison result enum.
         */
        enum Comparison : long { identical = MPI_IDENT, similar = MPI_SIMILAR, unequal = MPI_UNEQUAL };
        /**
         @brief Compare two groups.
         @return
         Comparison::identical results if the group members and group order is exactly the same in both groups.
         This happens for instance if group1 and group2 are the same handle.
         Comparison::similar results if the group members are the same but the order is different.
         Comparison::unequal results otherwise.
         */
        Comparison compare(Group const &other) const;
        Comparison compare(_Comm const &other) const;

#pragma mark Derived Group
        // Note on set-like operations:
        // Note that for these operations the order of processes in the output group is determined primarily by order in the first group (if possible) and then, if necessary, by order in the second group.
        // Neither union nor intersection are commutative, but both are associative.
        // The new group can be empty, that is, equal to MPI_GROUP_EMPTY.
        //
        /**
         @brief Construct a new group which is union of *this and other.
         @discussion All elements of the first group (*this), followed by all elements of second group (other) not in the first group.

         Neither union nor intersection are commutative, but both are associative.
         */
        Group operator|(Group const &other) const;

        /**
         @brief Construct a new group which is intersection of *this and other.
         @discussion All elements of the first group (*this) that are also in the second group (other), ordered as in the first group.

         Neither union nor intersection are commutative, but both are associative.
         */
        Group operator&(Group const &other) const;

        /**
         @brief Construct a new group which is difference of *this from other.
         @discussion All elements of the first group (*this) that are not in the second group (other), ordered as in the first group.
         */
        Group operator-(Group const &other) const;

        /**
         @brief Construct a new group that consists of N processes in *this with ranks.
         @discussion The process with rank `i' in new group is the process with rank `ranks[i]' in *this.

         Each of the N elements of ranks must be a valid rank in *this and all elements must be distinct, or else the program is erroneous.

         If N = 0, then the new group is MPI_GROUP_EMPTY.

         This function can, for instance, be used to reorder the elements of a group.
         */
        template <long N>
        Group included(UTL::Vector<Rank, N> const &ranks) const { return _included({ranks.begin(), ranks.end()}); }
        /**
         @brief Construct a new group that consists of processes in *this with ranks referenced in the iterator.
         @tparam It A forward iterator.
         */
        template <class It> typename std::enable_if<
        std::is_base_of<std::forward_iterator_tag, typename std::iterator_traits<It>::iterator_category>::value,
        Group>::type included(It first, It last) const { return _included({first, last}); }

        /**
         @brief Construct a new group of processes that is obtained by deleting from *this those processes with ranks.
         @discussion The ordering of processes in the new group is identical to the ordering in *this.

         Each of the N elements of ranks must be a valid rank in *this and all elements must be distinct; otherwise, the program is erroneous.

         If N = 0, then the new group is identical to *this.
         */
        template <long N>
        Group excluded(UTL::Vector<Rank, N> const &ranks) const { return _excluded({ranks.begin(), ranks.end()}); }
        /**
         @brief Construct a new group of processes that is obtained by deleting from *this those processes with ranks referenced in the iterator.
         @tparam It A forward iterator.
         */
        template <class It> typename std::enable_if<
        std::is_base_of<std::forward_iterator_tag, typename std::iterator_traits<It>::iterator_category>::value,
        Group>::type excluded(It first, It last) const { return _excluded({first, last}); }

    private:
        // first translate(rank_first, rank_last, to)

        Group _included(UTL::DynamicArray<int> const &ranks) const;
        Group _excluded(UTL::DynamicArray<int> const &ranks) const;
    };
} // namespace __2_
MPIKIT_END_NAMESPACE

#endif /* MPIGroup__2__h */
