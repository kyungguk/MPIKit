//
//  MPITypeMap.h
//  MPIKit
//
//  Created by KYUNGGUK MIN on 11/13/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef MPITypeMap_h
#define MPITypeMap_h

#include <MPIKit/MPIKit-config.h>

// MARK:- Version 1
//
#if defined(__APPLE__)
#include <mpich2/mpi.h>
#else
#include <mpi.h>
#endif

MPIKIT_BEGIN_NAMESPACE
#if defined(MPIKIT_INLINE_VERSION) && MPIKIT_INLINE_VERSION == 1
inline
#endif
namespace __1_ {
    /**
     @brief Type-safe mapping from native arithematic types to MPI datatypes.
     */
    template <class> struct TypeMap;
    template <> struct TypeMap<          char> { static constexpr MPI_Datatype value = MPI_CHAR; };
    template <> struct TypeMap<unsigned  char> { static constexpr MPI_Datatype value = MPI_UNSIGNED_CHAR; };
    template <> struct TypeMap<         short> { static constexpr MPI_Datatype value = MPI_SHORT; };
    template <> struct TypeMap<unsigned short> { static constexpr MPI_Datatype value = MPI_UNSIGNED_SHORT; };
    template <> struct TypeMap<           int> { static constexpr MPI_Datatype value = MPI_INT; };
    template <> struct TypeMap<unsigned      > { static constexpr MPI_Datatype value = MPI_UNSIGNED; };
    template <> struct TypeMap<          long> { static constexpr MPI_Datatype value = MPI_LONG; };
    template <> struct TypeMap<unsigned  long> { static constexpr MPI_Datatype value = MPI_UNSIGNED_LONG; };
    template <> struct TypeMap<         float> { static constexpr MPI_Datatype value = MPI_FLOAT; };
    template <> struct TypeMap<        double> { static constexpr MPI_Datatype value = MPI_DOUBLE; };

} // namespace __1_
MPIKIT_END_NAMESPACE

#endif /* MPITypeMap_h */
