//
//  MPIP2P__2_.h
//  MPIKit
//
//  Created by KYUNGGUK MIN on 7/30/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef MPIP2P__2__h
#define MPIP2P__2__h

#include <MPIKit/MPIKit-config.h>

// MARK:- Version 2
//
#include <MPIKit/MPITag__2_.h>
#include <MPIKit/MPIRank__2_.h>
#include <MPIKit/MPIType__2_.h>
#include <MPIKit/MPITypeMap__2_.h>
#include <MPIKit/MPIRequest__2_.h>
#include <UtilityKit/UtilityKit.h>
#include <type_traits>
#include <stdexcept>
#include <utility>
#include <vector>

#if defined(__APPLE__)
#include <mpich2/mpi.h>
#else
#include <mpi.h>
#endif

MPIKIT_BEGIN_NAMESPACE
#if defined(MPIKIT_INLINE_VERSION) && MPIKIT_INLINE_VERSION == 2
inline
#endif
namespace __2_ {
    template <class Comm>
    class P2P;
    class Comm;

    /**
     @brief Abstraction for point-to-point communication.
     */
    template <>
    class P2P<Comm> {
        inline MPI_Comm comm() const;

    protected:
        ~P2P() = default;
        explicit P2P() noexcept = default;
        P2P(P2P &&) noexcept = default;
        P2P &operator=(P2P &&) noexcept = default;

    public:
#pragma mark Persistent Communication Requests
        /**
         @brief Creates a persistent communication request for a synchronous mode send operation.
         */
        Request ssend_init(std::pair<void const*, Type const &> const &data, long const count, std::pair<Rank, Tag> const &to) const;
        template <class T> typename std::enable_if<std::is_default_constructible<TypeMap<T>>::value,
        Request>::type ssend_init(T const *data, long const count, std::pair<Rank, Tag> const &to) const {
            static Type const t{make_type<T>()};
            return ssend_init({data, t}, count, to);
        }
        /**
         @brief Creates a persistent communication request for a receive operation.
         */
        Request recv_init(std::pair<void*, Type const &> const &data, long const count, std::pair<Rank, Tag> const &from) const;
        template <class T> typename std::enable_if<std::is_default_constructible<TypeMap<T>>::value,
        Request>::type recv_init(T *data, long const count, std::pair<Rank, Tag> const &from) const {
            static Type const t{make_type<T>()};
            return recv_init({data, t}, count, from);
        }

#pragma mark Non-blocking Send/Blocking Recv
        /**
         @brief Start a synchronous mode, nonblocking send.
         */
        Request issend(std::pair<void const*, Type const&> const &data, long const count, std::pair<Rank, Tag> const &to) const;
        template <class T> typename std::enable_if<std::is_default_constructible<TypeMap<T>>::value,
        Request>::type issend(T const *data, long const count, std::pair<Rank, Tag> const &to) const {
            static Type const t{make_type<T>()};
            return issend({data, t}, count, to);
        }

        /**
         @brief Blocks until there is a message that can be received and that matches the pattern specified by the arguments.
         */
        MPI_Status probe(std::pair<Rank, Tag> const &from) const;
        /**
         @brief Matching probe.
         */
        MPI_Status probe(std::pair<Rank, Tag> const &from, MPI_Message *msg) const;

        /**
         @brief Light-weight wrapper for blocking receive.
         @discussion The length of the received message must be less than or equal to the length of the receive buffer.
         An overflow error occurs if all incoming data does not fit, without truncation, into the receive buffer.
         If a message that is shorter than the receive buffer arrives, then only those locations corresponding to the (shorter) message are modified.
         @return MPI_Status token, which can be used to retrieve received message count.
         */
        MPI_Status recv(std::pair<void*, Type const&> const &data, long const count, std::pair<Rank, Tag> const &from) const;
        template <class T> typename std::enable_if<std::is_default_constructible<TypeMap<T>>::value,
        MPI_Status>::type recv(T *data, long const count, std::pair<Rank, Tag> const &from) const {
            static Type const t{make_type<T>()};
            return recv({data, t}, count, from);
        }
        /**
         @brief Matched receive.
         */
        static MPI_Status recv(std::pair<void*, Type const&> const &data, long const count, MPI_Message &&msg);
        template <class T>
        static auto recv(T *data, long const count, MPI_Message &&msg) -> typename std::enable_if<std::is_default_constructible<TypeMap<T>>::value,
        MPI_Status>::type {
            static Type const t{make_type<T>()};
            return recv({data, t}, count, std::move(msg));
        }

        /**
         @brief Non-blocking synchronous send of a scalar.
         @discussion make_type<T>() should return a valid typemap and operator&() should return the address of T.
         */
        template <class T> typename std::enable_if<std::is_default_constructible<TypeMap<T>>::value,
        Request>::type issend(T const &x, std::pair<Rank, Tag> const &to) const {
            return issend(&x, 1, to);
        }
        /**
         @brief Blocking receive of a scalar.
         @discussion make_type<T>() should return a valid typemap and operator&() should return the address of T.
         */
        template <class T> typename std::enable_if<std::is_default_constructible<TypeMap<T>>::value,
        void>::type recv(T &x, std::pair<Rank, Tag> const &from) const {
            recv(&x, 1, from);
        }

        /**
         @brief Non-blocking synchronous send of a fixed-size contiguous array.
         @discussion make_type<T>() should return a valid typemap.
         */
        template <class T>
        Request issend(UTL::Array<T> const &A, std::pair<Rank, Tag> const &to) const {
            return issend(A.begin(), A.size(), to);
        }
        /**
         @brief Blocking receive of a fixed-size contiguous array.
         @discussion make_type<T>() should return a valid typemap.
         */
        template <class T>
        void recv(UTL::Array<T> &A, std::pair<Rank, Tag> const &from) const {
            recv(A.begin(), A.size(), from);
        }

        /**
         @brief Non-blocking synchronous send of a fixed-size padded array.
         @discussion make_type<T>() should return a valid typemap.
         */
        template <class T, long Pad>
        Request issend(UTL::PaddedArray<T, Pad> const &A, std::pair<Rank, Tag> const &to) const {
            return issend(A.data(), A.max_size(), to);
        }
        /**
         @brief Blocking receive of a fixed-size padded array.
         @discussion make_type<T>() should return a valid typemap.
         */
        template <class T, long Pad>
        void recv(UTL::PaddedArray<T, Pad> &A, std::pair<Rank, Tag> const &from) const {
            recv(A.data(), A.max_size(), from);
        }

        /**
         @brief Non-blocking synchronous send of a fixed-size ND array.
         @discussion make_type<T>() should return a valid typemap.
         */
        template <class T, long ND, long Pad>
        Request issend(UTL::ArrayND<T, ND, Pad> const &A, std::pair<Rank, Tag> const &to) const {
            return issend(A.flat_array(), to);
        }
        /**
         @brief Blocking receive of a fixed-size ND array.
         @discussion make_type<T>() should return a valid typemap.
         */
        template <class T, long ND, long Pad>
        void recv(UTL::ArrayND<T, ND, Pad> &A, std::pair<Rank, Tag> const &from) const {
            recv(A.flat_array(), from);
        }

        /**
         @brief Non-blocking synchronous send of a fixed-size ND array slice.
         @discussion make_type<T>() should return a valid typemap.

         The behavior is the same as if one would pass an ND array of the same dimension sizes.
         */
        template <class T, long ND, long Pad>
        Request issend(UTL::ArraySliceND<T, ND, Pad> const &S, std::pair<Rank, Tag> const &to) const {
            UTL::ArrayND<T, ND, Pad> A{S}; // do not use of stack after return
            Request req = issend(A, to);
            req.set_resource(std::move(A)); // now the send buffer is managed by the request object
            return req;
        }
        /**
         @brief Blocking receive of a fixed-size ND array slice.
         @discussion make_type<T>() should return a valid typemap.

         The behavior is the same as if one would pass an ND array of the same dimension sizes.
         */
        template <class T, long ND, long Pad>
        void recv(UTL::ArraySliceND<T, ND, Pad> &S, std::pair<Rank, Tag> const &from) const {
            UTL::ArrayND<T, ND, Pad> A{S.dims()}; // do not use of stack after return
            recv(A, from), S = std::move(A);
        }

        /**
         @brief Blocking receive of a variable-size dynamic array.
         @discussion make_type<T>() should return a valid typemap.

         The array is resized to fit the receiving message.
         */
        template <class T>
        void recv(UTL::DynamicArray<T> &A, std::pair<Rank, Tag> const &from) const {
            MPI_Message msg;
            static Type const t{make_type<T>()};
            auto const count = t.get_count(probe(from, &msg));
            if (!count) throw std::runtime_error(__PRETTY_FUNCTION__);
            A.resize(*count);
            recv(A.data(), A.size(), std::move(msg));
        }

        /**
         @brief Blocking receive of a variable-size static array.
         @discussion make_type<T>() should return a valid typemap.

         The array is resized to fit the receiving message.
         */
        template <class T, long MaxSize>
        void recv(UTL::StaticArray<T, MaxSize> &A, std::pair<Rank, Tag> const &from) const {
            MPI_Message msg;
            static Type const t{make_type<T>()};
            auto const count = t.get_count(probe(from, &msg));
            if (!count) throw std::runtime_error(__PRETTY_FUNCTION__);
            A.resize(*count);
            recv(A.data(), A.size(), std::move(msg));
        }

        /**
         @brief Non-blocking synchronous send of a std::vector (contiguous storage).
         @discussion make_type<T>() should return a valid typemap.
         */
        template <class T, class Alloc>
        Request issend(std::vector<T, Alloc> const &A, std::pair<Rank, Tag> const &to) const {
            return issend(A.data(), static_cast<long>(A.size()), to);
        }
        /**
         @brief Blocking receive of a std::vector (contiguous storage).
         @discussion make_type<T>() should return a valid typemap.

         The array is resized to fit the receiving message.
         */
        template <class T, class Alloc>
        void recv(std::vector<T, Alloc> &A, std::pair<Rank, Tag> const &from) const {
            MPI_Message msg;
            static Type const t{make_type<T>()};
            auto const count = t.get_count(probe(from, &msg));
            if (!count) throw std::runtime_error(__PRETTY_FUNCTION__);
            A.resize(static_cast<unsigned long>(*count));
            recv(A.data(), static_cast<long>(A.size()), std::move(msg));
        }

#pragma mark Send-Recv
        /**
         @brief Execute a blocking send and receive operation.
         @discussion Both send and receive use the same communicator, but possibly different tags.
         The send buffer and receive buffers must be disjoint, and may have different lengths and datatypes.
         */
        MPI_Status send_recv(std::pair<void const*, Type const&> const &send_data, long const send_count, std::pair<Rank, Tag> const &send_to,
                             std::pair<void      *, Type const&> const &recv_data, long const recv_count, std::pair<Rank, Tag> const &recv_from) const;
        template <class T, class U> typename std::enable_if<
        std::is_default_constructible<TypeMap<T>>::value && std::is_default_constructible<TypeMap<U>>::value,
        MPI_Status>::type send_recv(T const *send_data, long const send_count, std::pair<Rank, Tag> const &send_to,
                                    U       *recv_data, long const recv_count, std::pair<Rank, Tag> const &recv_from) const {
            static Type const t{make_type<T>()}, u{make_type<U>()};
            return send_recv({send_data, t}, send_count, send_to,
                             {recv_data, u}, recv_count, recv_from);
        }
        /**
         @brief Execute a blocking send and receive.
         @discussion The same buffer is used both for the send and for the receive, so that the message sent is replaced by the message received.
         */
        MPI_Status send_recv(std::pair<void*, Type const&> const &data, long const count, std::pair<Rank, Tag> const &send_to, std::pair<Rank, Tag> const &recv_from) const;
        template <class T> typename std::enable_if<std::is_default_constructible<TypeMap<T>>::value,
        MPI_Status>::type send_recv(T *data, long const count, std::pair<Rank, Tag> const &send_to, std::pair<Rank, Tag> const &recv_from) const {
            static Type const t{make_type<T>()};
            return send_recv({data, t}, count, send_to, recv_from);
        }

        /**
         @brief Send-recv replace on a scalar.
         @discussion make_type<T>() should return a valid typemap and operator&() should return the address of T.
         */
        template <class T> typename std::enable_if<std::is_default_constructible<TypeMap<T>>::value,
        void>::type send_recv(T &x, std::pair<Rank, Tag> const &send_to, std::pair<Rank, Tag> const &recv_from) const {
            send_recv(&x, 1, send_to, recv_from);
        }

        /**
         @brief Send-recv replace on a fixed-size contiguous array.
         @discussion make_type<T>() should return a valid typemap.
         */
        template <class T>
        void send_recv(UTL::Array<T> &A, std::pair<Rank, Tag> const &send_to, std::pair<Rank, Tag> const &recv_from) const {
            send_recv(A.begin(), A.size(), send_to, recv_from);
        }

        /**
         @brief Send-recv replace on a fixed-size padded array.
         @discussion make_type<T>() should return a valid typemap.
         */
        template <class T, long Pad>
        void send_recv(UTL::PaddedArray<T, Pad> &A, std::pair<Rank, Tag> const &send_to, std::pair<Rank, Tag> const &recv_from) const {
            send_recv(A.data(), A.max_size(), send_to, recv_from);
        }

        /**
         @brief Send-recv replace on a fixed-size ND array.
         @discussion make_type<T>() should return a valid typemap.
         */
        template <class T, long ND, long Pad>
        void send_recv(UTL::ArrayND<T, ND, Pad> &A, std::pair<Rank, Tag> const &send_to, std::pair<Rank, Tag> const &recv_from) const {
            send_recv(A.flat_array(), send_to, recv_from);
        }

        /**
         @brief Send-recv replace on a fixed-size ND array slice.
         @discussion make_type<T>() should return a valid typemap.

         The behavior is the same as if one would pass an ND array of the same dimension sizes.
         */
        template <class T, long ND, long Pad>
        void send_recv(UTL::ArraySliceND<T, ND, Pad> &S, std::pair<Rank, Tag> const &send_to, std::pair<Rank, Tag> const &recv_from) const {
            UTL::ArrayND<T, ND, Pad> A{S}; // do not use of stack after return
            send_recv(A, send_to, recv_from), S = std::move(A);
        }

        /**
         @brief Send-recv replace on a variable-size dynamic array.
         @discussion make_type<T>() should return a valid typemap.

         Internally, a non-blocking, synchronous send is called with a copy of the array `A' followed by a matching probing.
         Upon return from the matching probing, the receiving message count is retrieved to resize the array and a matched receive call is posted.
         */
        template <class T>
        void send_recv(UTL::DynamicArray<T> &A, std::pair<Rank, Tag> const &send_to, std::pair<Rank, Tag> const &recv_from) const {
            std::vector<T> const tmp{A.begin(), A.end()}; // intentionally std::vector
            Request req = issend(tmp, send_to);
            recv(A, recv_from), req.wait();
        }

        /**
         @brief Send-recv replace on a variable-size static array.
         @discussion make_type<T>() should return a valid typemap.

         Internally, a non-blocking, synchronous send is called with a copy of the array `A' followed by a matching probing.
         Upon return from the matching probing, the receiving message count is retrieved to resize the array and a matched receive call is posted.
         */
        template <class T, long MaxSize>
        void send_recv(UTL::StaticArray<T, MaxSize> &A, std::pair<Rank, Tag> const &send_to, std::pair<Rank, Tag> const &recv_from) const {
            std::vector<T> const tmp{A.begin(), A.end()}; // intentionally std::vector
            Request req = issend(tmp, send_to);
            recv(A, recv_from), req.wait();
        }

        /**
         @brief Send-recv replace on a std::vector (contiguous storage).
         @discussion make_type<T>() should return a valid typemap.

         Internally, a non-blocking, synchronous send is called with a copy of the array `A' followed by a matching probing.
         Upon return from the matching probing, the receiving message count is retrieved to resize the array and a matched receive call is posted.
         */
        template <class T, class Alloc>
        void send_recv(std::vector<T, Alloc> &A, std::pair<Rank, Tag> const &send_to, std::pair<Rank, Tag> const &recv_from) const {
            std::vector<T, Alloc> const tmp{A.begin(), A.end()};
            Request req = issend(tmp, send_to);
            recv(A, recv_from), req.wait();
        }
    };
} // namespace __2_
MPIKIT_END_NAMESPACE

#endif /* MPIP2P__2__h */
