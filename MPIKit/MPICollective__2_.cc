//
//  MPICollective__2_.cc
//  MPIKit
//
//  Created by KYUNGGUK MIN on 7/30/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "MPICollective__2_.h"
#include "MPIComm__2_.h"
#include <stdexcept>
#include <string>
#include <limits>

// MARK:- MPK::__2_::Collective<Comm>
//
using MPK::__2_::Comm;

MPI_Comm MPK::__2_::Collective<Comm>::comm() const
{
    return static_cast<Comm const*>(this)->operator*();
}

void MPK::__2_::Collective<Comm>::barrier() const
{
    if (MPI_Barrier(comm()) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Barrier(...) returned error");
    }
}

void MPK::__2_::Collective<Comm>::bcast(std::pair<void*, Type const &> const &data, long const count, Rank const root) const
{
    if (count > std::numeric_limits<int>::max()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - number of elements greater than int::max");
    }
    if (MPI_Bcast(data.first, static_cast<int>(count), *data.second, root, comm()) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Bcast(...) returned error");
    }
}

void MPK::__2_::Collective<Comm>::gather(std::pair<void const*, Type const &> const &send_data, long const send_count,
                                          std::pair<void      *, Type const &> const &recv_data, long const recv_count,
                                          Rank const root) const
{
    if (send_count > std::numeric_limits<int>::max()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - number of send elements greater than int::max");
    }
    if (recv_count > std::numeric_limits<int>::max()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - number of receive elements greater than int::max");
    }
    if (MPI_Gather(send_data.first, static_cast<int>(send_count), *send_data.second,
                   recv_data.first, static_cast<int>(recv_count), *recv_data.second, root, comm()) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Gather(...) returned error");
    }
}
void MPK::__2_::Collective<Comm>::gather(std::pair<void const*, Type const &> const &send_data, long const send_count,
                                          std::pair<void      *, Type const &> const &recv_data, long const recv_count) const
{
    if (send_count > std::numeric_limits<int>::max()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - number of send elements greater than int::max");
    }
    if (recv_count > std::numeric_limits<int>::max()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - number of receive elements greater than int::max");
    }
    if (MPI_Allgather(send_data.first, static_cast<int>(send_count), *send_data.second,
                      recv_data.first, static_cast<int>(recv_count), *recv_data.second, comm()) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Allgather(...) returned error");
    }
}

void MPK::__2_::Collective<Comm>::scatter(std::pair<void const*, Type const &> const &send_data, long const send_count,
                                           std::pair<void      *, Type const &> const &recv_data, long const recv_count,
                                           Rank const root) const
{
    if (send_count > std::numeric_limits<int>::max()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - number of send elements greater than int::max");
    }
    if (recv_count > std::numeric_limits<int>::max()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - number of receive elements greater than int::max");
    }
    if (MPI_Scatter(send_data.first, static_cast<int>(send_count), *send_data.second,
                    recv_data.first, static_cast<int>(recv_count), *recv_data.second, root, comm()) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Scatter(...) returned error");
    }
}
void MPK::__2_::Collective<Comm>::all2all(std::pair<void const*, Type const &> const &send_data, long const send_count,
                                           std::pair<void      *, Type const &> const &recv_data, long const recv_count) const
{
    if (send_count > std::numeric_limits<int>::max()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - number of send elements greater than int::max");
    }
    if (recv_count > std::numeric_limits<int>::max()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - number of receive elements greater than int::max");
    }
    if (MPI_Alltoall(send_data.first, static_cast<int>(send_count), *send_data.second,
                     recv_data.first, static_cast<int>(recv_count), *recv_data.second, comm()) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Alltoall(...) returned error");
    }
}
