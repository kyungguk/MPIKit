//
//  MPICommunicator.h
//  MPIKit
//
//  Created by KYUNGGUK MIN on 11/13/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef MPICommunicator_h
#define MPICommunicator_h

#include <MPIKit/MPIKit-config.h>

// MARK:- Version 1
//
#include <UtilityKit/UtilityKit.h>
#include <type_traits>

#if defined(__APPLE__)
#include <mpich2/mpi.h>
#else
#include <mpi.h>
#endif

MPIKIT_BEGIN_NAMESPACE
#if defined(MPIKIT_INLINE_VERSION) && MPIKIT_INLINE_VERSION == 1
inline
#endif
namespace __1_ {
    class Envelope;
    class Request;

    /**
     @brief MPI communicator.
     */
    class Communicator {
        MPI_Comm _comm;
        bool _should_free;

        explicit Communicator(MPI_Comm const comm, bool const should_free);
    public:
        Communicator(Communicator const&) = delete;
        Communicator &operator=(Communicator const&) = delete;

        void swap(Communicator &) noexcept;
        Communicator(Communicator &&) noexcept;
        Communicator &operator=(Communicator &&) noexcept;

        ~Communicator();
        explicit Communicator() noexcept;

#pragma mark MPI Init/Finalize
        /**
         @brief Initialize the MPI execution environment.
         @param argc Pointer to the number of arguments.
         @param argv Pointer to the argument vector.
         */
        static int init(int *argc, char ***argv) noexcept;
        /**
         @brief Initialize the MPI execution environment.
         @param argc Pointer to the number of arguments.
         @param argv Pointer to the argument vector.
         @param required Level of desired thread support.
         @param [out] provided Level of provided thread support.
         */
        static int init(int *argc, char ***argv, int const required, int &provided) noexcept;
        /**
         @brief Terminates MPI execution environment.
         */
        static int deinit() noexcept;

#pragma mark Default Communicators
        // init must be called first
        //
        static Communicator world();
        static Communicator self();

        Communicator split(int const color, int const key) const;
        Communicator split(int const color) const { return split(color, this->rank()); }

#pragma mark Observers
        /**
         Native handle accessor.
         */
        MPI_Comm const &operator*() const noexcept { return _comm; }
        /**
         @brief Determines the rank of the calling process in the communicator.
         */
        int rank() const;
        /**
         @brief Determines the size of the group associated with a communicator
         */
        int size() const;

#pragma mark Synchronization
        /**
         @brief Blocks until all processes in the communicator have reached this routine.
         */
        void barrier();

#pragma mark Reduction
        /**
         Scalar min.
         */
        template <class T>
        T min(T const &x) { return min(UTL::Vector<T, 1>{x}).front(); }
        /**
         Vector min.
         */
        template <class T, long S>
        UTL::Vector<T, S> min(UTL::Vector<T, S> const &v);

        /**
         Scalar max.
         */
        template <class T>
        T max(T const &x) { return max(UTL::Vector<T, 1>{x}).front(); }
        /**
         Vector max.
         */
        template <class T, long S>
        UTL::Vector<T, S> max(UTL::Vector<T, S> const &v);

        /**
         Scalar plus.
         */
        template <class T>
        T plus(T const &x) { return plus(UTL::Vector<T, 1>{x}).front(); }
        /**
         Vector plus.
         */
        template <class T, long S>
        UTL::Vector<T, S> plus(UTL::Vector<T, S> const &v);

        /**
         Scalar prod.
         */
        template <class T>
        T prod(T const &x) { return prod(UTL::Vector<T, 1>{x}).front(); }
        /**
         Vector prod.
         */
        template <class T, long S>
        UTL::Vector<T, S> prod(UTL::Vector<T, S> const &v);

        /**
         Scalar mean.
         */
        template <class T>
        typename std::enable_if<std::is_floating_point<T>::value, T>::type mean(T const &x) { return mean(UTL::Vector<T, 1>{x}).front(); }
        /**
         Vector mean.
         */
        template <class T, long S>
        typename std::enable_if<std::is_floating_point<T>::value, UTL::Vector<T, S>>::type mean(UTL::Vector<T, S> const &v) { return plus(v)/static_cast<T>(size()); }

        /**
         In-place reduce-plus on fixed-size array of scalar type.
         */
        template <class T>
        void plus(UTL::Array<T> &A); // funnel point of all reduce-plus
        /**
         In-place reduce-plus on fixed-size array of vector type.
         */
        template <class T, long S>
        inline void plus(UTL::Array<UTL::Vector<T, S>> &V);

        /**
         In-place reduce-plus on fixed-size padded array.
         */
        template <class T, long Pad>
        inline void plus(UTL::PaddedArray<T, Pad> &S);

        /**
         In-place reduce-plus on fixed-size N-dimensional array.
         */
        template <class T, long ND, long Pad>
        inline void plus(UTL::ArrayND<T, ND, Pad> &M);

#pragma mark Send/Recv Replace
        /**
         Send/recv fixed-size array of scalar type.
         */
        template <class T>
        void send_recv(UTL::Array<T> &A, Envelope const &send_to, Envelope const &recv_from); // funnel point of all send_recv
        /**
         Send/recv fixed-size array of vector type.
         */
        template <class T, long S>
        inline void send_recv(UTL::Array<UTL::Vector<T, S>> &V, Envelope const &send_to, Envelope const &recv_from);

        /**
         Send/recv scalar/vector.
         */
        template <class T>
        inline void send_recv(T &x, Envelope const &send_to, Envelope const &recv_from);

        /**
         Send/recv fixed-size padded array.
         */
        template <class T, long Pad>
        inline void send_recv(UTL::PaddedArray<T, Pad> &S, Envelope const &send_to, Envelope const &recv_from);

        /**
         Send/recv fixed-size N-dimensional array.
         */
        template <class T, long ND, long Pad>
        inline void send_recv(UTL::ArrayND<T, ND, Pad> &M, Envelope const &send_to, Envelope const &recv_from);

        /**
         Send/recv variable size dynamic array.
         */
        template <class T>
        inline void send_recv(UTL::DynamicArray<T> &var, Envelope const &send_to, Envelope const &recv_from);

        /**
         Send/recv variable size static array.
         */
        template <class T, long MaxSize>
        inline void send_recv(UTL::StaticArray<T, MaxSize> &var, Envelope const &send_to, Envelope const &recv_from);

#pragma mark Non-blocking Send/Blocking Recv
        // (note that non-const in send argument to make things simple)
        /**
         Non-blocking synchronous send of a fixed-size array of scalars.
         */
        template <class T>
        Request issend(UTL::Array<T> &A, Envelope const &to);
        /**
         Blocking receive of a fixed-size array of scalars.
         */
        template <class T>
        void recv(UTL::Array<T> &A, Envelope const &from);

        /**
         Non-blocking synchronous send of a fixed-size array of vectors.
         */
        template <class T, long S>
        inline Request issend(UTL::Array<UTL::Vector<T, S>> &V, Envelope const &to);
        /**
         Blocking receive of a fixed-size array of vectors.
         */
        template <class T, long S>
        inline void recv(UTL::Array<UTL::Vector<T, S>> &V, Envelope const &from);

        /**
         Non-blocking synchronous send of a scalar/vector.
         */
        template <class T>
        inline Request issend(T &x, Envelope const &to);
        /**
         Blocking receive of a scalar/vector.
         */
        template <class T>
        inline void recv(T &x, Envelope const &from);

        /**
         Non-blocking synchronous send of a fixed-size padded array.
         */
        template <class T, long Pad>
        inline Request issend(UTL::PaddedArray<T, Pad> &S, Envelope const &to);
        /**
         Blocking receive of a fixed-size padded array.
         */
        template <class T, long Pad>
        inline void recv(UTL::PaddedArray<T, Pad> &S, Envelope const &from);

        /**
         Non-blocking synchronous send of a variable size dynamic array.
         */
        template <class T>
        inline Request issend(UTL::DynamicArray<T> &var, Envelope const &to);
        /**
         Blocking receive of a variable size dynamic array of scalars.
         */
        template <class T>
        void recv(UTL::DynamicArray<T> &A, Envelope const &from);
        /**
         Blocking receive of a variable size dynamic array of vectors.
         */
        template <class T, long S>
        void recv(UTL::DynamicArray<UTL::Vector<T, S>> &A, Envelope const &from);

        /**
         Non-blocking synchronous send of a variable size static array.
         */
        template <class T, long Cap>
        inline Request issend(UTL::StaticArray<T, Cap> &var, Envelope const &to);
        /**
         Blocking receive of a variable size dynamic array of scalars.
         */
        template <class T, long Cap>
        void recv(UTL::StaticArray<T, Cap> &A, Envelope const &from);
        /**
         Blocking receive of a variable size dynamic array of vectors.
         */
        template <class T, long S, long Cap>
        void recv(UTL::StaticArray<UTL::Vector<T, S>, Cap> &A, Envelope const &from);

    private:
        // fetch count of receiving elements
        template <class T>
        inline int _count(Envelope const& from);
    };
} // namespace __1_
MPIKIT_END_NAMESPACE


// template member implementations
//
#include <MPIKit/MPICommunicator.hh>

#endif /* MPICommunicator_h */
