//
//  MPIRequest__2_.cc
//  MPIKit
//
//  Created by KYUNGGUK MIN on 7/30/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "MPIRequest__2_.h"
#include <UtilityKit/UtilityKit.h>
#include <stdexcept>
#include <iostream>
#include <string>

// MARK:- MPK::__2_::Request
//
MPK::__2_::Request::~Request()
{
    // Any other type of request is deallocated and the request handle is set to MPI_REQUEST_NULL.
    // One is allowed to call MPI_WAIT with a null or inactive request argument.
    if (MPI_REQUEST_NULL != _r && MPI_Wait(&_r, MPI_STATUS_IGNORE) != MPI_SUCCESS) {
        println(std::cerr, "%% ", __PRETTY_FUNCTION__, " - MPI_Wait(...) returned error");
#if defined(MPIKIT_SHOULD_TERMINATE_ON_FAILURE_IN_DESTRUCTOR) && MPIKIT_SHOULD_TERMINATE_ON_FAILURE_IN_DESTRUCTOR
        std::terminate();
#endif
    }
    if (MPI_REQUEST_NULL != _r && MPI_Request_free(&_r) != MPI_SUCCESS) {
        println(std::cerr, "%% ", __PRETTY_FUNCTION__, " - MPI_Request_free(...) returned error");
#if defined(MPIKIT_SHOULD_TERMINATE_ON_FAILURE_IN_DESTRUCTOR) && MPIKIT_SHOULD_TERMINATE_ON_FAILURE_IN_DESTRUCTOR
        std::terminate();
#endif
    }
}

auto MPK::__2_::Request::operator=(Request &&o) noexcept
-> Request &{
    if (this != &o) {
        Request{std::move(o)}.swap(*this);
    }
    return *this;
}

void MPK::__2_::Request::wait()
{
    if (MPI_Wait(&_r, MPI_STATUS_IGNORE) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Wait(...) returned error");
    }
}
bool MPK::__2_::Request::test()
{
    int flag;
    if (MPI_Test(&_r, &flag, MPI_STATUS_IGNORE) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Test(...) returned error");
    }
    return flag;
}
void MPK::__2_::Request::cancel()
{
    if (MPI_Cancel(&_r) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Cancel(...) returned error");
    }
}

void MPK::__2_::Request::start()
{
    if (MPI_Start(&_r) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Start(...) returned error");
    }
}
