//
//  MPICollective__2_.h
//  MPIKit
//
//  Created by KYUNGGUK MIN on 7/30/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef MPICollective__2__h
#define MPICollective__2__h

#include <MPIKit/MPIKit-config.h>

// MARK:- Version 2
//
#include <MPIKit/MPIRank__2_.h>
#include <MPIKit/MPIType__2_.h>
#include <MPIKit/MPITypeMap__2_.h>
#include <UtilityKit/UtilityKit.h>
#include <type_traits>
#include <utility>
#include <vector>

#if defined(__APPLE__)
#include <mpich2/mpi.h>
#else
#include <mpi.h>
#endif

MPIKIT_BEGIN_NAMESPACE
#if defined(MPIKIT_INLINE_VERSION) && MPIKIT_INLINE_VERSION == 2
inline
#endif
namespace __2_ {
    template <class Comm>
    class Collective;
    class Comm;

    /**
     @brief Abstraction for collective communication.
     @discussion Collective operations on intracomminucators are assumed.
     */
    template <>
    class Collective<Comm> {
        inline MPI_Comm comm() const;

    protected:
        ~Collective() = default;
        explicit Collective() noexcept = default;
        Collective(Collective &&) noexcept = default;
        Collective &operator=(Collective &&) noexcept = default;

    public:
#pragma mark Synchronization
        /**
         @brief Blocks until all processes in the communicator have reached this routine.
         */
        void barrier() const;

#pragma mark Blocking Broadcast
        /**
         @brief Broadcasts a message from the process with rank root to all processes of the group, itself included.
         @discussion It is called by all members of the group using the same arguments for comm and root.
         On return, the content of root's buffer is copied to all other processes.

         General, derived datatypes are allowed for datatype.
         The type signature of count, datatype on any process must be equal to the type signature of count, datatype at the root.
         This implies that the amount of data sent must be equal to the amount received, pairwise between each process and the root.
         Distinct type maps between sender and receiver are still allowed.
         */
        void bcast(std::pair<void*, Type const &> const &data, long const count, Rank const root) const;
        template <class T> typename std::enable_if<std::is_default_constructible<TypeMap<T>>::value,
        void>::type bcast(T *data, long const count, Rank const root) const {
            static Type const t{make_type<T>()};
            bcast({data, t}, count, root);
        }

        /**
         @brief Broadcast a scalar.
         @discussion make_type<T>() should return a valid typemap and operator&() should return the address of T.
         */
        template <class T> typename std::enable_if<std::is_default_constructible<TypeMap<T>>::value,
        void>::type bcast(T &x, Rank const root) const {
            bcast(&x, 1, root);
        }

        /**
         @brief Broadcast a fixed-size contiguous array.
         @discussion make_type<T>() should return a valid typemap.
         */
        template <class T>
        void bcast(UTL::Array<T> &A, Rank const root) const {
            bcast(A.begin(), A.size(), root);
        }

        /**
         @brief Broadcast a fixed-size padded array.
         @discussion make_type<T>() should return a valid typemap.
         */
        template <class T, long Pad>
        void bcast(UTL::PaddedArray<T, Pad> &A, Rank const root) const {
            bcast(A.data(), A.max_size(), root);
        }

        /**
         @brief Broadcast a fixed-size ND array.
         @discussion make_type<T>() should return a valid typemap.
         */
        template <class T, long ND, long Pad>
        void bcast(UTL::ArrayND<T, ND, Pad> &A, Rank const root) const {
            bcast(A.flat_array(), root);
        }

        /**
         @brief Broadcast a fixed-size ND array slice.
         @discussion make_type<T>() should return a valid typemap.

         The behavior is the same as if one would pass an ND array of the same dimension sizes.
         */
        template <class T, long ND, long Pad>
        void bcast(UTL::ArraySliceND<T, ND, Pad> &S, Rank const root) const {
            UTL::ArrayND<T, ND, Pad> A{S}; // do not use of stack after return
            bcast(A, root), S = std::move(A);
        }

        /**
         @brief Broadcast a std::vector (contiguous storage).
         @discussion make_type<T>() should return a valid typemap.

         The number of elements must be the same in all processes in the group.
         */
        template <class T, class Alloc>
        void bcast(std::vector<T, Alloc> &A, Rank const root) const {
            bcast(A.data(), static_cast<long>(A.size()), root);
        }

#pragma mark Blocking Gather
        /**
         @brief Each process (root process included) sends the contents of its send buffer to the root process.
         @discussion The root process receives the messages and stores them in rank order.
         The receive buffer is ignored for all non-root processes.

         General, derived datatypes are allowed for both sendtype and recvtype.
         The type signature of sendcount, sendtype on each process must be equal to the type signature of recvcount, recvtype at the root.
         This implies that the amount of data sent must be equal to the amount of data received, pairwise between each process and the root.
         Distinct type maps between sender and receiver are still allowed.

         All arguments to the function are significant on process root, while on other processes, only arguments sendbuf, sendcount, sendtype, root, and comm are significant.
         The arguments root and comm must have identical values on all processes.

         The specification of counts and types should not cause any location on the root to be written more than once.
         Such a call is erroneous.

         Note that the recvcount argument at the root indicates the number of items it receives from each process, not the total number of items it receives.
         */
        void gather(std::pair<void const*, Type const &> const &send_data, long const send_count,
                    std::pair<void      *, Type const &> const &recv_data, long const recv_count,
                    Rank const root) const;
        template <class T, class U> typename std::enable_if<
        std::is_default_constructible<TypeMap<T>>::value && std::is_default_constructible<TypeMap<U>>::value,
        void>::type gather(T const *send_data, long const send_count,
                           U       *recv_data, long const recv_count,
                           Rank const root) const {
            static Type const t{make_type<T>()}, u{make_type<U>()};
            gather({send_data, t}, send_count, {recv_data, u}, recv_count, root);
        }

        /**
         @brief In-place gather on root process.
         @discussion The "in place" option for intracommunicators is specified by passing MPI_IN_PLACE as the value of sendbuf at the root.
         In such a case, sendcount and sendtype are ignored, and the contribution of the root to the gathered vector is assumed to be already in the correct place in the receive buffer.
         */
        template <class T> typename std::enable_if<std::is_default_constructible<TypeMap<T>>::value,
        void>::type gather(decltype(nullptr), long, T *recv_data, long const recv_count, Rank const root) const {
            static Type const t{make_type<T>()};
            gather({MPI_IN_PLACE, Type{}}, long{}, {recv_data, t}, recv_count, root);
        }
        /**
         @brief Gather on other processes.
         */
        template <class T> typename std::enable_if<std::is_default_constructible<TypeMap<T>>::value,
        void>::type gather(T const *send_data, long const send_count, decltype(nullptr), long, Rank const root) const {
            static Type const t{make_type<T>()};
            gather({send_data, t}, send_count, {nullptr, Type{}}, long{}, root);
        }

        /**
         @brief Gather-to-all.
         @discussion All processes receive the result, instead of just the root.
         The block of data sent from the j-th process is received by every process and placed in the j-th block of the buffer recvbuf.

         The type signature associated with sendcount, sendtype, at a process must be equal to the type signature associated with recvcount, recvtype at any other process.
         */
        void gather(std::pair<void const*, Type const &> const &send_data, long const send_count,
                    std::pair<void      *, Type const &> const &recv_data, long const recv_count) const;
        template <class T, class U> typename std::enable_if<
        std::is_default_constructible<TypeMap<T>>::value && std::is_default_constructible<TypeMap<U>>::value,
        void>::type gather(T const *send_data, long const send_count,
                           U       *recv_data, long const recv_count) const {
            static Type const t{make_type<T>()}, u{make_type<U>()};
            gather({send_data, t}, send_count, {recv_data, u}, recv_count);
        }

        /**
         @brief In-place gather-to-all.
         @discussion The "in place" option for intracommunicators is specified by passing the value MPI_IN_PLACE to the argument sendbuf at all processes. sendcount and sendtype are ignored.
         Then the input data of each process is assumed to be in the area where that process would receive its own contribution to the receive buffer.
         */
        template <class T> typename std::enable_if<std::is_default_constructible<TypeMap<T>>::value,
        void>::type gather(T *data, long const count) const {
            static Type const t{make_type<T>()};
            gather({MPI_IN_PLACE, Type{}}, long{}, {data, t}, count);
        }

#pragma mark Blocking Scatter
        /**
         @brief Inverse of gather operation.
         @discussion The send buffer is ignored for all non-root processes.

         The type signature associated with sendcount, sendtype at the root must be equal to the type signature associated with recvcount, recvtype at all processes (however, the type maps may be different).
         This implies that the amount of data sent must be equal to the amount of data received, pairwise between each process and the root.
         Distinct type maps between sender and receiver are still allowed.

         All arguments to the function are significant on process root, while on other processes, only arguments recvbuf, recvcount, recvtype, root, and comm are significant.
         The arguments root and comm must have identical values on all processes.

         The specification of counts and types should not cause any location on the root to be read more than once.
         */
        void scatter(std::pair<void const*, Type const &> const &send_data, long const send_count,
                     std::pair<void      *, Type const &> const &recv_data, long const recv_count,
                     Rank const root) const;
        template <class T, class U> typename std::enable_if<
        std::is_default_constructible<TypeMap<T>>::value && std::is_default_constructible<TypeMap<U>>::value,
        void>::type scatter(T const *send_data, long const send_count,
                            U       *recv_data, long const recv_count,
                            Rank const root) const {
            static Type const t{make_type<T>()}, u{make_type<U>()};
            scatter({send_data, t}, send_count, {recv_data, u}, recv_count, root);
        }

        /**
         @brief In-place scatter on root process.
         @discussion The "in place" option for intracommunicators is specified by passing MPI_IN_PLACE as the value of recvbuf at the root.
         In such a case, recvcount and recvtype are ignored, and root "sends" no data to itself.
         The scattered vector is still assumed to contain n segments, where n is the group size; the root-th segment, which root should "send to itself," is not moved.
         */
        template <class T> typename std::enable_if<std::is_default_constructible<TypeMap<T>>::value,
        void>::type scatter(T const *send_data, long const send_count, decltype(nullptr), long, Rank const root) const {
            static Type const t{make_type<T>()};
            scatter({send_data, t}, send_count, {MPI_IN_PLACE, Type{}}, long{}, root);
        }
        /**
         @brief Scatter on other processes.
         */
        template <class T> typename std::enable_if<std::is_default_constructible<TypeMap<T>>::value,
        void>::type scatter(decltype(nullptr), long, T *recv_data, long const recv_count, Rank const root) const {
            static Type const t{make_type<T>()};
            scatter({nullptr, Type{}}, long{}, {recv_data, t}, recv_count, root);
        }

        /**
         @brief All-to-all.
         @discussion MPI_ALLTOALL is an extension of MPI_ALLGATHER to the case where each process sends distinct data to each of the receivers.
         The j-th block sent from process i is received by process j and is placed in the i-th block of recvbuf.

         The type signature associated with sendcount, sendtype, at a process must be equal to the type signature associated with recvcount, recvtype at any other process.
         This implies that the amount of data sent must be equal to the amount of data received, pairwise between every pair of processes.
         As usual, however, the type maps may be different.

         All arguments on all processes are significant. The argument comm must have identical values on all processes.
         */
        void all2all(std::pair<void const*, Type const &> const &send_data, long const send_count,
                     std::pair<void      *, Type const &> const &recv_data, long const recv_count) const;
        template <class T, class U> typename std::enable_if<
        std::is_default_constructible<TypeMap<T>>::value && std::is_default_constructible<TypeMap<U>>::value,
        void>::type all2all(T const *send_data, long const send_count,
                            U       *recv_data, long const recv_count) const {
            static Type const t{make_type<T>()}, u{make_type<U>()};
            all2all({send_data, t}, send_count, {recv_data, u}, recv_count);
        }

        /**
         @brief In-place all-to-all.
         @discussion The "in place" option for intracommunicators is specified by passing MPI_IN_PLACE to the argument sendbuf at all processes.
         In such a case, sendcount and sendtype are ignored.
         The data to be sent is taken from the recvbuf and replaced by the received data.
         Data sent and received must have the same type map as specified by recvcount and recvtype.
         */
        template <class T> typename std::enable_if<std::is_default_constructible<TypeMap<T>>::value,
        void>::type all2all(T *data, long const count) const {
            static Type const t{make_type<T>()};
            all2all({MPI_IN_PLACE, Type{}}, long{}, {data, t}, count);
        }
    };
} // namespace __2_
MPIKIT_END_NAMESPACE

#endif /* MPICollective__2__h */
