//
//  MPIReduceOp__2_.cc
//  MPIKit
//
//  Created by KYUNGGUK MIN on 8/7/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "MPIReduceOp__2_.h"
#include "MPIComm__2_.h"
#include <UtilityKit/UtilityKit.h>
#include <stdexcept>
#include <iostream>
#include <string>

// MARK:- MPK::__2_::ReduceOp
//
MPK::__2_::ReduceOp::~ReduceOp()
{
    // test of MPI_Finalized is to prevent *this from calling MPI_Type_free after MPI has been finalized
    //
    if (*this && _should_free && !Comm::finalized() && MPI_Op_free(&_op) != MPI_SUCCESS) {
        println(std::cerr, "%% ", __PRETTY_FUNCTION__, " - MPI_Op_free(...) returned error");
#if defined(MPIKIT_SHOULD_TERMINATE_ON_FAILURE_IN_DESTRUCTOR) && MPIKIT_SHOULD_TERMINATE_ON_FAILURE_IN_DESTRUCTOR
        std::terminate();
#endif
    }
}

auto MPK::__2_::ReduceOp::operator=(ReduceOp &&o) noexcept
-> ReduceOp &{
    if (this != &o) {
        ReduceOp{std::move(o)}.swap(*this);
    }
    return *this;
}

MPK::__2_::ReduceOp::ReduceOp(bool const commutative, void (*f)(void *invec, void *inoutvec, int *vlen, MPI_Datatype *))
: ReduceOp() {
    MPI_Op op;
    if (MPI_Op_create(f, commutative, &op) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Op_create(...) returned error");
    }
    ReduceOp{std::move(op)}.swap(*this);
}

bool MPK::__2_::ReduceOp::commutative() const
{
    int commute;
    if (MPI_Op_commutative(**this, &commute) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Op_commutative(...) returned error");
    }
    return commute;
}
