//
//  MPIReduceOp__2_.h
//  MPIKit
//
//  Created by KYUNGGUK MIN on 8/7/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef MPIReduceOp__2__h
#define MPIReduceOp__2__h

#include <MPIKit/MPIKit-config.h>

// MARK:- Version 2
//
#include <utility>

#if defined(__APPLE__)
#include <mpich2/mpi.h>
#else
#include <mpi.h>
#endif

MPIKIT_BEGIN_NAMESPACE
#if defined(MPIKIT_INLINE_VERSION) && MPIKIT_INLINE_VERSION == 2
inline
#endif
namespace __2_ {
    /**
     @brief Wrapper for user-defined reduction operator.
     */
    class ReduceOp {
        MPI_Op _op{MPI_OP_NULL};
        bool _should_free{false};

        explicit ReduceOp(MPI_Op const op, decltype(nullptr)) noexcept : _op(op) {}
        explicit ReduceOp(MPI_Op &&op) noexcept : _should_free(true) { std::swap(_op, op); }
    public:
        ~ReduceOp();
        ReduceOp(ReduceOp &&o) noexcept : ReduceOp() { swap(o); }
        ReduceOp &operator=(ReduceOp &&o) noexcept;
        void swap(ReduceOp &o) noexcept { std::swap(_op, o._op); std::swap(_should_free, o._should_free); }

        explicit ReduceOp() noexcept = default;
        explicit ReduceOp(bool const commutative, void (*f)(void *invec, void *inoutvec, int *vlen, MPI_Datatype *));

        explicit operator bool() const noexcept { return _op != MPI_OP_NULL; }
        MPI_Op const &operator*() const noexcept { return _op; }
        bool commutative() const;

        static ReduceOp max() noexcept { return ReduceOp{MPI_MAX, nullptr}; }
        static ReduceOp min() noexcept { return ReduceOp{MPI_MIN, nullptr}; }
        static ReduceOp plus() noexcept { return ReduceOp{MPI_SUM, nullptr}; }
        static ReduceOp prod() noexcept { return ReduceOp{MPI_PROD, nullptr}; }
        static ReduceOp logic_and() noexcept { return ReduceOp{MPI_LAND, nullptr}; }
        static ReduceOp bit_and() noexcept { return ReduceOp{MPI_BAND, nullptr}; }
        static ReduceOp logic_or() noexcept { return ReduceOp{MPI_LOR, nullptr}; }
        static ReduceOp bit_or() noexcept { return ReduceOp{MPI_BOR, nullptr}; }
        static ReduceOp logic_xor() noexcept { return ReduceOp{MPI_LXOR, nullptr}; }
        static ReduceOp bit_xor() noexcept { return ReduceOp{MPI_BXOR, nullptr}; }
        static ReduceOp loc_min() noexcept { return ReduceOp{MPI_MINLOC, nullptr}; }
        static ReduceOp loc_max() noexcept { return ReduceOp{MPI_MAXLOC, nullptr}; }
        //replace = MPI_REPLACE
        //no_op = MPI_NO_OP
    };
} // namespace __2_
MPIKIT_END_NAMESPACE

#endif /* MPIReduceOp__2__h */
