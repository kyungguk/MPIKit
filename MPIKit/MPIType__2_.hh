//
//  MPIType__2_.hh
//  MPIKit
//
//  Created by KYUNGGUK MIN on 7/29/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef MPIType__2__hh
#define MPIType__2__hh

#include <stdexcept>
#include <string>

// MARK:- MPK::__2_::Type
//
template <long block_size, long block, long stride>
auto MPK::__2_::Type::vector() const
-> Type {
    static_assert(block_size >= 0 && block_size < int_max, "invalid block_size parameter");
    static_assert(block >= 0 && block < int_max, "invalid block parameter");
    static_assert(stride > -int_max && stride < int_max, "invalid stride parameter");

    MPI_Datatype t;
    if (MPI_Type_vector(block_size, block, stride, **this, &t) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Type_vector(...) returned error");
    }
    return Type{std::move(t), alignment()}; // commit is done in construction
}
template <long block_size>
auto MPK::__2_::Type::indexed(UTL::Vector<long, block_size> const &block, UTL::Vector<long, block_size> const &displacement) const
-> Type {
    static_assert(block_size >= 0 && block_size < int_max, "invalid block_size parameter");

    // check integer bound
    //
    using lV = UTL::Vector<long, block_size>;
    if (UTL::reduce_bit_or((block < lV{0L}) | (block >= lV{int_max}))) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid block size(s)");
    }
    if (UTL::reduce_bit_or((displacement <= lV{-int_max}) | (displacement >= lV{int_max}))) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid displacement(s)");
    }
    using iV = UTL::Vector<int, block_size>;
    iV const i_block = lV_to_iV(block), i_disp = lV_to_iV(displacement);

    // create
    //
    MPI_Datatype t;
    if (MPI_Type_indexed(block_size, i_block.data(), i_disp.data(), **this, &t) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Type_indexed(...) returned error");
    }
    return Type{std::move(t), alignment()}; // commit is done in construction
}
template <long S, long block>
auto MPK::__2_::Type::indexed(UTL::Vector<long, S> const &displacement) const
-> Type {
    static_assert(S >= 0 && S < int_max, "invalid S parameter");
    static_assert(block >= 0 && block < int_max, "invalid block parameter");
    UTL::Vector<int, S> const i_disp = lV_to_iV(displacement);

    // create
    //
    MPI_Datatype t;
    if (MPI_Type_create_indexed_block(S, block, i_disp.data(), **this, &t) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Type_indexed_block(...) returned error");
    }
    return Type{std::move(t), alignment()}; // commit is done in construction
}
template <long ND>
auto MPK::__2_::Type::subarray(UTL::Vector<long, ND> const &dims, UTL::Vector<long, ND> const &slice_locs, UTL::Vector<long, ND> const &slice_lens) const
-> Type {
    static_assert(ND > 0 && ND < int_max, "invalid ND parameter");

    // check integer bound
    //
    using lV = UTL::Vector<long, ND>;
    if (UTL::reduce_bit_or((dims <= lV{0L}) | (dims >= lV{int_max}))) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid dimension size(s)");
    }
    if (UTL::reduce_bit_or((slice_locs < lV{0L}) | (slice_locs >= dims))) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid slice location(s)");
    }
    if (UTL::reduce_bit_or((slice_lens <= lV{0L}) | (slice_locs + slice_lens > dims))) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - invalid displacement(s)");
    }
    using iV = UTL::Vector<int, ND>;
    iV const i_dims = lV_to_iV(dims), i_locs = lV_to_iV(slice_locs), i_lens = lV_to_iV(slice_lens);

    // create
    //
    MPI_Datatype t;
    if (MPI_Type_create_subarray(ND, i_dims.data(), i_lens.data(), i_locs.data(), MPI_ORDER_C, **this, &t) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Type_create_subarray(...) returned error");
    }
    return Type{std::move(t), alignment()}; // commit is done in construction
}
template <long Alignment>
auto MPK::__2_::Type::realigned() const
-> Type {
    static_assert(Alignment > 0 && UTL::is_power_of_2(Alignment), "invalid alignment");
    if (Alignment < alignment()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - the new alignment has to be greater than or equal to the current alignment");
    }
    return _resized(Alignment); // commit is done in construction
}

#endif /* MPIType__2__hh */
