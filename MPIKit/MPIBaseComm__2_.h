//
//  MPIBaseComm__2_.h
//  MPIKit
//
//  Created by KYUNGGUK MIN on 7/29/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef MPIBaseComm__2__h
#define MPIBaseComm__2__h

#include <MPIKit/MPIKit-config.h>

// MARK:- Version 2
//
#include <MPIKit/MPIRank__2_.h>
#include <MPIKit/MPIGroup__2_.h>
#include <utility>
#include <string>

#if defined(__APPLE__)
#include <mpich2/mpi.h>
#else
#include <mpi.h>
#endif

MPIKIT_BEGIN_NAMESPACE
#if defined(MPIKIT_INLINE_VERSION) && MPIKIT_INLINE_VERSION == 2
inline
#endif
namespace __2_ {
    class Comm;

    /**
     @brief Wrapper for MPI_Comm.
     */
    class _Comm {
        MPI_Comm _c{MPI_COMM_NULL};

    protected:
        ~_Comm();

        constexpr explicit _Comm(MPI_Comm const c, decltype(nullptr)) noexcept : _c(c) {} // this is for predefined comm's
        explicit _Comm(MPI_Comm &&c); // custom error handler is set
        constexpr explicit _Comm() noexcept = default;

        void swap(_Comm &o) noexcept { std::swap(_c, o._c); }
        _Comm(_Comm &&o) noexcept : _Comm() { swap(o); }
        _Comm &operator=(_Comm &&) noexcept;

    public:
#pragma mark Init/Finalize
        /**
         @brief Initialize the MPI execution environment.
         @param argc Pointer to the number of arguments.
         @param argv Pointer to the argument vector.
         */
        static int init(int *argc, char ***argv) noexcept;
        /**
         @brief Initialize the MPI execution environment.
         @param argc Pointer to the number of arguments.
         @param argv Pointer to the argument vector.
         @param required Level of desired thread support.
         @param [out] provided Level of provided thread support.
         */
        static int init(int *argc, char ***argv, int const required, int &provided) noexcept;
        /**
         @brief Terminates MPI execution environment.
         */
        static int deinit() noexcept;

        /**
         @brief Return true if MPI_Finalize is called.
         */
        static bool finalized() noexcept;

#pragma mark Query
        /**
         @brief Returns true if *this is valid, i.e., not MPI_COMM_NULL.
         */
        explicit operator bool() const noexcept { return _c != MPI_COMM_NULL; }

        /**
         @brief Returns the associated MPI_Comm handle.
         */
        MPI_Comm const &operator*() const noexcept { return _c; }

        /**
         @brief Associates a label with *this.
         */
        void set_label(char const *label);
        /**
         @brief Returns the label associated with *this.
         */
        std::string label() const;

        /**
         @brief Determines the size of the group associated with *this.
         */
        int size() const;

        /**
         @brief Determines the rank of the calling process in *this.
         */
        Rank rank() const;

        /**
         @brief Comparison result enum.
         */
        enum Comparison : long { identical = MPI_IDENT, congruent =MPI_CONGRUENT, similar = MPI_SIMILAR, unequal = MPI_UNEQUAL };
        /**
         @brief Compare two communicators.
         @return
         Comparison::identical results if and only if *this and other are handles for the same object (identical groups and same contexts).
         Comparison::congruent results if the underlying groups are identical in constituents and rank order; these communicators differ only by context.
         Comparison::similar results if the group members of *this and other are the same but the rank order differs.
         Comparison::unequal results otherwise.
         */
        Comparison compare(_Comm const &other) const;

        /**
         @brief Returns Group associated with *this.
         */
        Group group() const;
    };
} // namespace __2_
MPIKIT_END_NAMESPACE

#endif /* MPIBaseComm__2__h */
