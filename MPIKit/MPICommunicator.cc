//
//  MPICommunicator.cc
//  MPIKit
//
//  Created by KYUNGGUK MIN on 11/13/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#include "MPICommunicator.h"

// MARK:- Version 1
//
#include <MPIKit/MPIEnvelope.h>
#include <stdexcept>
#include <string>

int MPK::__1_::Communicator::init(int *argc, char ***argv) noexcept
{
    return MPI_Init(argc, argv);
}
int MPK::__1_::Communicator::init(int *argc, char ***argv, int const required, int &provided) noexcept
{
    return MPI_Init_thread(argc, argv, required, &provided);
}
int MPK::__1_::Communicator::deinit() noexcept
{
    return MPI_Finalize();
}

void MPK::__1_::Communicator::swap(Communicator &o) noexcept
{
    std::swap(_comm, o._comm);
    std::swap(_should_free, o._should_free);
}
MPK::__1_::Communicator::Communicator(Communicator &&o) noexcept
: _comm(o._comm), _should_free(o._should_free) {
    o._comm = MPI_COMM_NULL;
    o._should_free = false;
}
MPK::__1_::Communicator &MPK::__1_::Communicator::operator=(Communicator &&o) noexcept
{
    if (this != &o) {
        Communicator{std::move(o)}.swap(*this);
    }
    return *this;
}

MPK::__1_::Communicator::~Communicator()
{
    if (_should_free) {
        MPI_Comm_free(&_comm);
        _should_free = false;
    }
}
MPK::__1_::Communicator::Communicator() noexcept
: _comm(MPI_COMM_NULL), _should_free(false) {
}
MPK::__1_::Communicator::Communicator(MPI_Comm const comm, bool const should_free)
: _comm(comm), _should_free(should_free) {
    if (MPI_Comm_set_errhandler(_comm, MPI_ERRORS_RETURN) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Comm_set_errhandler returned error");
    }
}

MPK::__1_::Communicator MPK::__1_::Communicator::world()
{
    return Communicator{MPI_COMM_WORLD, false};
}
MPK::__1_::Communicator MPK::__1_::Communicator::self()
{
    return Communicator{MPI_COMM_SELF, false};
}
MPK::__1_::Communicator MPK::__1_::Communicator::split(int const color, int const key) const
{
    MPI_Comm split;
    if (MPI_Comm_split(**this, color, key, &split) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Comm_split returned error");
    }
    return Communicator{split, true};
}

int MPK::__1_::Communicator::rank() const
{
    int rank;
    if (MPI_Comm_rank(**this, &rank) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Comm_rank returned error");
    }
    return rank;
}
int MPK::__1_::Communicator::size() const
{
    int size;
    if (MPI_Comm_size(**this, &size) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Comm_size returned error");
    }
    return size;
}

void MPK::__1_::Communicator::barrier()
{
    if (MPI_Barrier(**this) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Barrier returned error");
    }
}
