//
//  MPIBaseComm__2_.cc
//  MPIKit
//
//  Created by KYUNGGUK MIN on 7/29/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "MPIBaseComm__2_.h"
#include <UtilityKit/UtilityKit.h>
#include <stdexcept>
#include <iostream>
#include <string>

// MARK:- MPK::__2_::_Comm
//
int MPK::__2_::_Comm::init(int *argc, char ***argv) noexcept
{
    return MPI_Init(argc, argv);
}
int MPK::__2_::_Comm::init(int *argc, char ***argv, int const required, int &provided) noexcept
{
    return MPI_Init_thread(argc, argv, required, &provided);
}
int MPK::__2_::_Comm::deinit() noexcept
{
    return MPI_Finalize();
}
bool MPK::__2_::_Comm::finalized() noexcept
{
    int finalized;
    MPI_Finalized(&finalized);
    return finalized;
}

MPK::__2_::_Comm::~_Comm()
{
    if (MPI_COMM_NULL != _c && MPI_COMM_WORLD != _c && MPI_COMM_SELF != _c && MPI_Comm_free(&_c) != MPI_SUCCESS) {
        println(std::cerr, "%% ", __PRETTY_FUNCTION__, " - MPI_Comm_free(...) returned error");
#if defined(MPIKIT_SHOULD_TERMINATE_ON_FAILURE_IN_DESTRUCTOR) && MPIKIT_SHOULD_TERMINATE_ON_FAILURE_IN_DESTRUCTOR
        std::terminate();
#endif
    }
}
MPK::__2_::_Comm::_Comm(MPI_Comm &&c)
{
#if defined(MPIKIT_COMM_SET_ERROR_HANDLER) && MPIKIT_COMM_SET_ERROR_HANDLER
    if (MPI_COMM_NULL != c && MPI_Comm_set_errhandler(c, MPI_ERRORS_RETURN) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Comm_set_errhandler(...) returned error");
    }
#endif
    std::swap(_c, c);
}

auto MPK::__2_::_Comm::operator=(_Comm &&o) noexcept
-> _Comm &{
    if (this != &o) {
        _Comm{std::move(o)}.swap(*this);
    }
    return *this;
}

void MPK::__2_::_Comm::set_label(char const *label)
{
    if (MPI_Comm_set_name(**this, label) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Comm_set_name(...) returned error");
    }
}
std::string MPK::__2_::_Comm::label() const
{
    char label[MPI_MAX_OBJECT_NAME];
    int len;
    if (MPI_Comm_get_name(**this, label, &len) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Comm_get_name(...) returned error");
    }
    return label;
}

int MPK::__2_::_Comm::size() const
{
    int size;
    if (MPI_Comm_size(**this, &size) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Comm_size(...) returned error");
    }
    return size;
}
auto MPK::__2_::_Comm::rank() const
-> Rank {
    int rank;
    if (MPI_Comm_rank(**this, &rank) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Comm_rank(...) returned error");
    }
    return Rank{rank};
}

auto MPK::__2_::_Comm::compare(_Comm const &other) const
-> Comparison {
    int _result;
    if (MPI_Comm_compare(**this, *other, &_result) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Comm_compare(...) returned error");
    }
    Comparison result;
    switch (_result) {
        case MPI_IDENT:
            result = identical;
            break;
        case MPI_CONGRUENT:
            result = congruent;
            break;
        case MPI_SIMILAR:
            result = similar;
            break;
        case MPI_UNEQUAL:
            result = unequal;
            break;
        default:
            throw std::domain_error(std::string(__PRETTY_FUNCTION__) + " - unknown Comparison enum");
    }
    return result;
}

auto MPK::__2_::_Comm::group() const
-> Group {
    MPI_Group g;
    if (MPI_Comm_group(**this, &g) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Comm_group(...) returned error");
    }
    return Group{std::move(g)};
}
