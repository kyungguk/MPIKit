//
//  MPIKit-config.h
//  MPIKit
//
//  Created by KYUNGGUK MIN on 11/12/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef MPIKit_config_h
#define MPIKit_config_h


// root namespace
//
#ifndef MPIKIT_NAMESPACE
#define MPIKIT_NAMESPACE MPK
#define MPIKIT_BEGIN_NAMESPACE namespace MPIKIT_NAMESPACE {
#define MPIKIT_END_NAMESPACE }
#endif


// GCC Deprecated attribute
//
#ifndef MPIKIT_DEPRECATED_ATTRIBUTE
#define MPIKIT_DEPRECATED_ATTRIBUTE __attribute__ ((deprecated))
#endif


// flag to indicate error handler setting
//
#ifndef MPIKIT_COMM_SET_ERROR_HANDLER
#define MPIKIT_COMM_SET_ERROR_HANDLER 1
#endif


// flag to indicate termination on error return in destructor
//
#ifndef MPIKIT_SHOULD_TERMINATE_ON_FAILURE_IN_DESTRUCTOR
#define MPIKIT_SHOULD_TERMINATE_ON_FAILURE_IN_DESTRUCTOR 1
#endif


#endif /* MPIKit_config_h */
