//
//  MPIReduction__2_.cc
//  MPIKit
//
//  Created by KYUNGGUK MIN on 7/30/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "MPIReduction__2_.h"
#include "MPIComm__2_.h"
#include <stdexcept>
#include <string>
#include <limits>

// MARK:- MPK::__2_::Reduction<Comm>
//
using MPK::__2_::Comm;

MPI_Comm MPK::__2_::Reduction<Comm>::comm() const
{
    return static_cast<Comm const*>(this)->operator*();
}

void MPK::__2_::Reduction<Comm>::reduce(ReduceOp const &op, void const *send_data, void *recv_data, Type const &t, long const count, Rank const root) const
{
    if (count > std::numeric_limits<int>::max()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - number of elements greater than int::max");
    }
    if (MPI_Reduce(send_data, recv_data, static_cast<int>(count), *t, *op, root, comm()) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Reduce(...) returned error");
    }
}
void MPK::__2_::Reduction<Comm>::reduce(ReduceOp const &op, void const *send_data, void *recv_data, Type const &t, long const count) const
{
    if (count > std::numeric_limits<int>::max()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - number of elements greater than int::max");
    }
    if (MPI_Allreduce(send_data, recv_data, static_cast<int>(count), *t, *op, comm()) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Allreduce(...) returned error");
    }
}
