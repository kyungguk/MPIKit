//
//  MPICommunicator.hh
//  MPIKit
//
//  Created by KYUNGGUK MIN on 11/13/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef MPICommunicator_hh
#define MPICommunicator_hh

// MARK:- Version 1
//
#include <MPIKit/MPITypeMap.h>
#include <MPIKit/MPIEnvelope.h>
#include <MPIKit/MPIRequest.h>
#include <stdexcept>
#include <string>
#include <limits>

// MARK: Reductions
//
template <class T, long S>
UTL::Vector<T, S> MPK::__1_::Communicator::min(UTL::Vector<T, S> const &x) {
    UTL::Vector<T, S> y;
    if (MPI_Allreduce(x.data(), y.data(), S, TypeMap<T>::value, MPI_MIN, **this) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Allreduce returned error");
    }
    return y;
}

template <class T, long S>
UTL::Vector<T, S> MPK::__1_::Communicator::max(UTL::Vector<T, S> const &x) {
    UTL::Vector<T, S> y;
    if (MPI_Allreduce(x.data(), y.data(), S, TypeMap<T>::value, MPI_MAX, **this) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Allreduce returned error");
    }
    return y;
}

template <class T, long S>
UTL::Vector<T, S> MPK::__1_::Communicator::plus(UTL::Vector<T, S> const &x) {
    UTL::Vector<T, S> y;
    if (MPI_Allreduce(x.data(), y.data(), S, TypeMap<T>::value, MPI_SUM, **this) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Allreduce returned error");
    }
    return y;
}

template <class T, long S>
UTL::Vector<T, S> MPK::__1_::Communicator::prod(UTL::Vector<T, S> const &x) {
    UTL::Vector<T, S> y;
    if (MPI_Allreduce(x.data(), y.data(), S, TypeMap<T>::value, MPI_PROD, **this) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Allreduce returned error");
    }
    return y;
}

template <class T>
void MPK::__1_::Communicator::plus(UTL::Array<T> &A) {
    long const count = A.size();
    if (count > std::numeric_limits<int>::max()) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - the number of elements greater than int::max");
    }
    if (MPI_Allreduce(MPI_IN_PLACE, A.begin(), static_cast<int>(count), TypeMap<T>::value, MPI_SUM, **this) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Allreduce returned error");
    }
}
template <class T, long S>
void MPK::__1_::Communicator::plus(UTL::Array<UTL::Vector<T, S>> &V) {
    UTL::Array<typename std::remove_cv<T>::type> A{reinterpret_cast<T *>(V.begin()), V.size() * S};
    plus(A);
}

template <class T, long Pad>
void MPK::__1_::Communicator::plus(UTL::PaddedArray<T, Pad> &S) {
    UTL::Array<typename std::remove_cv<T>::type> A{S.data(), S.max_size()};
    plus(A);
}

template <class T, long ND, long Pad>
void MPK::__1_::Communicator::plus(UTL::ArrayND<T, ND, Pad> &M) {
    plus(M.flat_array());
}

// MARK: Send/Recv Replace
//
template <class T>
void MPK::__1_::Communicator::send_recv(UTL::Array<T> &A, Envelope const &send_to, Envelope const &recv_from) {
    long const count = A.size();
    if (count > std::numeric_limits<int>::max()) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - the number of elements greater than int::max");
    }
    if (MPI_Sendrecv_replace(A.begin(), static_cast<int>(count), TypeMap<T>::value, send_to.rank(), send_to.tag(), recv_from.rank(), recv_from.tag(), **this, MPI_STATUS_IGNORE) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Sendrecv_replace returned error");
    }
}
template <class T, long S>
void MPK::__1_::Communicator::send_recv(UTL::Array<UTL::Vector<T, S>> &V, Envelope const &send_to, Envelope const &recv_from) {
    UTL::Array<typename std::remove_cv<T>::type> A{reinterpret_cast<T *>(V.begin()), V.size() * S};
    send_recv(A, send_to, recv_from);
}

template <class T>
void MPK::__1_::Communicator::send_recv(T &x, Envelope const &send_to, Envelope const &recv_from) {
    UTL::Array<typename std::remove_cv<T>::type> A{&x, 1L};
    send_recv(A, send_to, recv_from);
}

template <class T, long Pad>
void MPK::__1_::Communicator::send_recv(UTL::PaddedArray<T, Pad> &S, Envelope const &send_to, Envelope const &recv_from) {
    UTL::Array<typename std::remove_cv<T>::type> A{S.data(), S.max_size()};
    send_recv(A, send_to, recv_from);
}

template <class T, long ND, long Pad>
void MPK::__1_::Communicator::send_recv(UTL::ArrayND<T, ND, Pad> &M, Envelope const &send_to, Envelope const &recv_from) {
    send_recv(M.flat_array(), send_to, recv_from);
}

template <class T>
void MPK::__1_::Communicator::send_recv(UTL::DynamicArray<T> &var, Envelope const &send_to, Envelope const &recv_from) {
    typename std::remove_reference<decltype(var)>::type tmp(var);
    Request req = issend(tmp, send_to);
    recv(var, recv_from);
    req.wait();
}

template <class T, long MaxSize>
void MPK::__1_::Communicator::send_recv(UTL::StaticArray<T, MaxSize> &var, Envelope const &send_to, Envelope const &recv_from) {
    typename std::remove_reference<decltype(var)>::type tmp(var);
    Request req = issend(tmp, send_to);
    recv(var, recv_from);
    req.wait();
}

// MARK: Non-blocking Send/Blocking Recv
//
template <class T>
MPK::__1_::Request MPK::__1_::Communicator::issend(UTL::Array<T> &A, Envelope const &to) {
    long const count = A.size();
    if (count > std::numeric_limits<int>::max()) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - the number of elements greater than int::max");
    }
    Request req;
    if (MPI_Issend(A.begin(), static_cast<int>(count), TypeMap<T>::value, to.rank(), to.tag(), **this, &req) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Issend returned error");
    }
    return req;
}
template <class T>
void MPK::__1_::Communicator::recv(UTL::Array<T> &A, Envelope const &from) {
    int const count = _count<T>(from);
    if (A.size() != count) {
        throw std::logic_error(std::string(__PRETTY_FUNCTION__) + " - incompatible array size");
    }
    if (MPI_Recv(A.begin(), count, TypeMap<T>::value, from.rank(), from.tag(), **this, MPI_STATUS_IGNORE) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Recv returned error");
    }
}

template <class T, long S>
MPK::__1_::Request MPK::__1_::Communicator::issend(UTL::Array<UTL::Vector<T, S>> &V, Envelope const &to) {
    UTL::Array<typename std::remove_cv<T>::type> A{reinterpret_cast<T *>(V.begin()), V.size() * S};
    return issend(A, to);
}
template <class T, long S>
void MPK::__1_::Communicator::recv(UTL::Array<UTL::Vector<T, S>> &V, Envelope const &from) {
    UTL::Array<typename std::remove_cv<T>::type> A{reinterpret_cast<T *>(V.begin()), V.size() * S};
    recv(A, from);
}

template <class T>
MPK::__1_::Request MPK::__1_::Communicator::issend(T &x, Envelope const &to) {
    UTL::Array<typename std::remove_cv<T>::type> A{&x, 1L};
    return issend(A, to);
}
template <class T>
void MPK::__1_::Communicator::recv(T &x, Envelope const &from) {
    UTL::Array<typename std::remove_cv<T>::type> A{&x, 1L};
    recv(A, from);
}

template <class T, long Pad>
MPK::__1_::Request MPK::__1_::Communicator::issend(UTL::PaddedArray<T, Pad> &S, Envelope const &to) {
    UTL::Array<typename std::remove_cv<T>::type> A{S.data(), S.max_size()};
    return issend(A, to);
}
template <class T, long Pad>
void MPK::__1_::Communicator::recv(UTL::PaddedArray<T, Pad> &S, Envelope const &from) {
    UTL::Array<typename std::remove_cv<T>::type> A{S.data(), S.max_size()};
    recv(A, from);
}

template <class T>
MPK::__1_::Request MPK::__1_::Communicator::issend(UTL::DynamicArray<T> &var, Envelope const &to) {
    UTL::Array<typename std::remove_cv<T>::type> A{var.data(), var.size()};
    return issend(A, to);
}
template <class T>
void MPK::__1_::Communicator::recv(UTL::DynamicArray<T> &A, Envelope const &from) {
    int const count = _count<T>(from);
    A.resize(count);
    if (MPI_Recv(A.data(), count, TypeMap<T>::value, from.rank(), from.tag(), **this, MPI_STATUS_IGNORE) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Recv returned error");
    }
}
template <class T, long S>
void MPK::__1_::Communicator::recv(UTL::DynamicArray<UTL::Vector<T, S>> &A, Envelope const &from) {
    int const count = _count<T>(from);
    if (count%S) {
        throw std::logic_error(std::string(__PRETTY_FUNCTION__) + " - total count of scalar values not divisible by vector size");
    }
    A.resize(count/S);
    if (MPI_Recv(A.data(), count, TypeMap<T>::value, from.rank(), from.tag(), **this, MPI_STATUS_IGNORE) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Recv returned error");
    }
}

template <class T, long Cap>
MPK::__1_::Request MPK::__1_::Communicator::issend(UTL::StaticArray<T, Cap> &var, Envelope const &to) {
    UTL::Array<typename std::remove_cv<T>::type> A{var.data(), var.size()};
    return issend(A, to);
}
template <class T, long Cap>
void MPK::__1_::Communicator::recv(UTL::StaticArray<T, Cap> &A, Envelope const &from) {
    int const count = _count<T>(from);
    if (count > Cap) {
        throw std::logic_error(std::string(__PRETTY_FUNCTION__) + " - the number of receiving elements exceeds the array capacity");
    }
    A.resize(count);
    if (MPI_Recv(A.data(), count, TypeMap<T>::value, from.rank(), from.tag(), **this, MPI_STATUS_IGNORE) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Recv returned error");
    }
}
template <class T, long S, long Cap>
void MPK::__1_::Communicator::recv(UTL::StaticArray<UTL::Vector<T, S>, Cap> &A, Envelope const &from) {
    int const count = _count<T>(from);
    if (count/S > Cap) {
        throw std::logic_error(std::string(__PRETTY_FUNCTION__) + " - the number of receiving elements exceeds the array capacity");
    }
    if (count%S) {
        throw std::logic_error(std::string(__PRETTY_FUNCTION__) + " - total count of scalar values not divisible by vector size");
    }
    A.resize(count/S);
    if (MPI_Recv(A.data(), count, TypeMap<T>::value, from.rank(), from.tag(), **this, MPI_STATUS_IGNORE) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Recv returned error");
    }
}

template <class T>
int MPK::__1_::Communicator::_count(Envelope const& from) {
    int count;
    MPI_Status status;
    if (MPI_Probe(from.rank(), from.tag(), **this, &status) != MPI_SUCCESS ||
        MPI_Get_count(&status, TypeMap<T>::value, &count) != MPI_SUCCESS ||
        MPI_UNDEFINED == count) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - failed to retrieve element count");
    }
    return count;
}

#endif /* MPICommunicator_hh */
