//
//  MPIEnvelope.h
//  MPIKit
//
//  Created by KYUNGGUK MIN on 11/13/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef MPIEnvelope_h
#define MPIEnvelope_h

#include <MPIKit/MPIKit-config.h>

// MARK:- Version 1
//

MPIKIT_BEGIN_NAMESPACE
#if defined(MPIKIT_INLINE_VERSION) && MPIKIT_INLINE_VERSION == 1
inline
#endif
namespace __1_ {
    /**
     @brief MPI message envelope.
     */
    class Envelope {
        int _dest;
        int _tag;

    public:
        /**
         Destination rank.
         */
        int const &rank() const noexcept { return _dest; }
        /**
         Tag.
         */
        int const &tag() const noexcept { return _tag; }

        /**
         @param destination Destination rank.
         @param tag Unique tag.
         */
        constexpr Envelope(int destination, int tag) noexcept : _dest(destination), _tag(tag) {}
    };
} // namespace __1_
MPIKIT_END_NAMESPACE

#endif /* MPIEnvelope_h */
