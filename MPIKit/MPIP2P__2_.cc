//
//  MPIP2P__2_.cc
//  MPIKit
//
//  Created by KYUNGGUK MIN on 7/30/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "MPIP2P__2_.h"
#include "MPIComm__2_.h"
#include <stdexcept>
#include <string>
#include <limits>

// MARK:- MPK::__2_::P2P<Comm>
//
using MPK::__2_::Comm;

MPI_Comm MPK::__2_::P2P<Comm>::comm() const
{
    return static_cast<Comm const*>(this)->operator*();
}

// MARK: Persistent Communication Requests
//
auto MPK::__2_::P2P<Comm>::ssend_init(std::pair<void const*, Type const &> const &data, long const count, std::pair<Rank, Tag> const &to) const
-> Request {
    if (count > std::numeric_limits<int>::max()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - number of elements greater than int::max");
    }
    MPI_Request r;
    if (MPI_Ssend_init(data.first, static_cast<int>(count), *data.second, to.first, to.second, comm(), &r) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Ssend_init(...) returned error");
    }
    return Request{std::move(r)};
}
auto MPK::__2_::P2P<Comm>::recv_init(std::pair<void*, Type const &> const &data, long const count, std::pair<Rank, Tag> const &from) const
-> Request {
    if (count > std::numeric_limits<int>::max()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - number of elements greater than int::max");
    }
    MPI_Request r;
    if (MPI_Recv_init(data.first, static_cast<int>(count), *data.second, from.first, from.second, comm(), &r) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Ssend_init(...) returned error");
    }
    return Request{std::move(r)};
}

// MARK: Non-blocking Send/Blocking Recv
//
auto MPK::__2_::P2P<Comm>::issend(std::pair<void const*, Type const&> const &data, long const count, std::pair<Rank, Tag> const &to) const
-> Request {
    if (count > std::numeric_limits<int>::max()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - number of elements greater than int::max");
    }
    MPI_Request r;
    if (MPI_Issend(data.first, static_cast<int>(count), *data.second, to.first, to.second, comm(), &r) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Issend(...) returned error");
    }
    return Request{std::move(r)};
}

MPI_Status MPK::__2_::P2P<Comm>::probe(std::pair<Rank, Tag> const &from) const
{
    MPI_Status s;
    if (MPI_Probe(from.first, from.second, comm(), &s) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Probe(...) returned error");
    }
    return s;
}
MPI_Status MPK::__2_::P2P<Comm>::recv(std::pair<void*, Type const&> const &data, long const count, std::pair<Rank, Tag> const &from) const
{
    if (count > std::numeric_limits<int>::max()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - number of elements greater than int::max");
    }
    MPI_Status s;
    if (MPI_Recv(data.first, static_cast<int>(count), *data.second, from.first, from.second, comm(), &s) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Recv(...) returned error");
    }
    return s;
}

MPI_Status MPK::__2_::P2P<Comm>::probe(std::pair<Rank, Tag> const &from, MPI_Message *msg) const
{
    MPI_Status s;
    if (MPI_Mprobe(from.first, from.second, comm(), msg, &s) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Mprobe(...) returned error");
    }
    return s;
}
MPI_Status MPK::__2_::P2P<Comm>::recv(std::pair<void*, Type const&> const &data, long const count, MPI_Message &&msg)
{
    if (count > std::numeric_limits<int>::max()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - number of elements greater than int::max");
    }
    MPI_Status s;
    if (MPI_Mrecv(data.first, static_cast<int>(count), *data.second, &msg, &s) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Mrecv(...) returned error");
    }
    return s;
}

// MARK: Send/Recv
//
MPI_Status MPK::__2_::P2P<Comm>::send_recv(std::pair<void const*, Type const&> const &send_data, long const send_count, std::pair<Rank, Tag> const &send_to,
                                            std::pair<void      *, Type const&> const &recv_data, long const recv_count, std::pair<Rank, Tag> const &recv_from) const
{
    if (send_count > std::numeric_limits<int>::max()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - number of send elements greater than int::max");
    }
    if (recv_count > std::numeric_limits<int>::max()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - number of receive elements greater than int::max");
    }
    MPI_Status s;
    if (MPI_Sendrecv(send_data.first, static_cast<int>(send_count), *send_data.second, send_to.first, send_to.second,
                     recv_data.first, static_cast<int>(recv_count), *recv_data.second, recv_from.first, recv_from.second,
                     comm(), &s) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Sendrecv(...) returned error");
    }
    return s;
}
MPI_Status MPK::__2_::P2P<Comm>::send_recv(std::pair<void*, Type const&> const &data, long const count, std::pair<Rank, Tag> const &send_to, std::pair<Rank, Tag> const &recv_from) const
{
    if (count > std::numeric_limits<int>::max()) {
        throw std::invalid_argument(std::string(__PRETTY_FUNCTION__) + " - number of elements greater than int::max");
    }
    MPI_Status s;
    if (MPI_Sendrecv_replace(data.first, static_cast<int>(count), *data.second, send_to.first, send_to.second, recv_from.first, recv_from.second, comm(), &s) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Sendrecv_replace(...) returned error");
    }
    return s;
}
