//
//  MPIReduction__2_.h
//  MPIKit
//
//  Created by KYUNGGUK MIN on 7/30/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef MPIReduction__2__h
#define MPIReduction__2__h

#include <MPIKit/MPIKit-config.h>

// MARK:- Version 2
//
#include <MPIKit/MPIRank__2_.h>
#include <MPIKit/MPIType__2_.h>
#include <MPIKit/MPITypeMap__2_.h>
#include <MPIKit/MPIReduceOp__2_.h>
#include <UtilityKit/UtilityKit.h>
#include <type_traits>
#include <utility>
#include <vector>

#if defined(__APPLE__)
#include <mpich2/mpi.h>
#else
#include <mpi.h>
#endif

MPIKIT_BEGIN_NAMESPACE
#if defined(MPIKIT_INLINE_VERSION) && MPIKIT_INLINE_VERSION == 2
inline
#endif
namespace __2_ {
    template <class Comm>
    class Reduction;
    class Comm;

    /**
     @brief Abstraction for reduction operations.
     @discussion Reduction operations on intracomminucators are assumed.
     */
    template <>
    class Reduction<Comm> {
        inline MPI_Comm comm() const;

    protected:
        ~Reduction() = default;
        explicit Reduction() noexcept = default;
        Reduction(Reduction &&) noexcept = default;
        Reduction &operator=(Reduction &&) noexcept = default;

    public:
#pragma mark - High-level Blocking All Reduction
        /**
         @brief Reduce on a scalar.
         @discussion make_type<T>() should return a valid typemap and operator&() should return the address of T.
         */
        template <class T> typename std::enable_if<std::is_default_constructible<TypeMap<T>>::value,
        void>::type reduce(ReduceOp const &op, T &x) const {
            return reduce(op, &x, 1);
        }

        /**
         @brief In-place reduce on a fixed-size contiguous array.
         @discussion make_type<T>() should return a valid typemap.
         */
        template <class T>
        void reduce(ReduceOp const &op, UTL::Array<T> &A) const {
            reduce(op, A.begin(), A.size());
        }

        /**
         @brief In-place reduce on a fixed-size padded array.
         @discussion make_type<T>() should return a valid typemap.
         */
        template <class T, long Pad>
        void reduce(ReduceOp const &op, UTL::PaddedArray<T, Pad> &A) const {
            reduce(op, A.data(), A.max_size());
        }

        /**
         @brief In-place reduce on a fixed-size ND array.
         @discussion make_type<T>() should return a valid typemap.
         */
        template <class T, long ND, long Pad>
        void reduce(ReduceOp const &op, UTL::ArrayND<T, ND, Pad> &A) const {
            reduce(op, A.flat_array());
        }

        /**
         @brief In-place reduce on a fixed-size ND array slice.
         @discussion make_type<T>() should return a valid typemap.

         The behavior is the same as if one would pass an ND array of the same dimension sizes.
         */
        template <class T, long ND, long Pad>
        void reduce(ReduceOp const &op, UTL::ArraySliceND<T, ND, Pad> &S) const {
            UTL::ArrayND<T, ND, Pad> A{S}; // do not use of stack after return
            reduce(op, A), S = std::move(A);
        }

        /**
         @brief In-place reduce on a std::vector (contiguous storage).
         @discussion make_type<T>() should return a valid typemap.

         The number of elements must be the same in all processes in the group.
         */
        template <class T, class Alloc>
        void reduce(ReduceOp const &op, std::vector<T, Alloc> &A) const {
            reduce(op, A.data(), static_cast<long>(A.size()));
        }

#pragma mark - Low-level MPI Interface Layer
        /**
         @brief Blocking reduce.
         @discussion It combines the elements provided in the input buffer of each process in the group, using the operation op, and returns the combined value in the output buffer of the process with rank root.
         The input buffer is defined by the arguments sendbuf, count and datatype; the output buffer is defined by the arguments recvbuf, count and datatype; both have the same number of elements, with the same type.
         It is called by all group members using the same arguments for count, datatype, op, root and comm.
         Thus, all processes provide input buffers of the same length, with elements of the same type as the output buffer at the root.
         Each process can provide one element, or a sequence of elements, in which case the combine operation is executed element-wise on each entry of the sequence.

         The operation op is always assumed to be associative.
         All predefined operations are also assumed to be commutative.
         Users may define operations that are assumed to be associative, but not commutative.
         The "canonical" evaluation order of a reduction is determined by the ranks of the processes in the group.
         However, the implementation can take advantage of associativity, or associativity and commutativity in order to change the order of evaluation.
         This may change the result of the reduction for operations that are not strictly associative and commutative, such as floating point addition.

         The datatype argument of MPI_REDUCE must be compatible with op.
         Furthermore, the datatype and op given for predefined operators must be the same on all processes.
         Note that it is possible for users to supply different user-defined operations to MPI_REDUCE in each process.
         MPI does not define which operations are used on which operands in this case.
         User-defined operators may operate on general, derived datatypes.
         In this case, each argument that the reduce operation is applied to is one element described by such a datatype, which may contain several basic values.
         */
        void reduce(ReduceOp const &op, void const *send_data, void *recv_data, Type const &t, long const count, Rank const root) const;
        template <class T> typename std::enable_if<std::is_default_constructible<TypeMap<T>>::value,
        void>::type reduce(ReduceOp const &op, T const *send_data, T *recv_data, long const count, Rank const root) const {
            static Type const t{make_type<T>()};
            reduce(op, send_data, recv_data, t, count, root);
        }

        /**
         @brief In-place reduce on the root process.
         @discussion The "in place" option for intracommunicators is specified by passing the value MPI_IN_PLACE to the argument sendbuf at the root.
         In such a case, the input data is taken at the root from the receive buffer, where it will be replaced by the output data.
         */
        template <class T> typename std::enable_if<std::is_default_constructible<TypeMap<T>>::value,
        void>::type reduce(ReduceOp const &op, decltype(nullptr), T *recv_data, long const count, Rank const root) const {
            static Type const t{make_type<T>()};
            reduce(op, MPI_IN_PLACE, recv_data, t, count, root);
        }

        /**
         @brief Reduce on other processes.
         */
        template <class T> typename std::enable_if<std::is_default_constructible<TypeMap<T>>::value,
        void>::type reduce(ReduceOp const &op, T const *send_data, decltype(nullptr), long const count, Rank const root) const {
            static Type const t{make_type<T>()};
            reduce(op, send_data, nullptr, t, count, root);
        }

        /**
         @brief Blocking all-reduce.
         @discussion It behaves the same as reduce except that the result appears in the receive buffer of all the group members.
         */
        void reduce(ReduceOp const &op, void const *send_data, void *recv_data, Type const &t, long const count) const;
        template <class T> typename std::enable_if<std::is_default_constructible<TypeMap<T>>::value,
        void>::type reduce(ReduceOp const &op, T const *send_data, T *recv_data, long const count) const {
            static Type const t{make_type<T>()};
            reduce(op, send_data, recv_data, t, count);
        }

        /**
         @brief In-place all-reduce.
         @discussion The "in place" option for intracommunicators is specified by passing the value MPI_IN_PLACE to the argument sendbuf at all processes.
         In this case, the input data is taken at each process from the receive buffer, where it will be replaced by the output data.
         */
        template <class T> typename std::enable_if<std::is_default_constructible<TypeMap<T>>::value,
        void>::type reduce(ReduceOp const &op, T *data, long const count) const {
            static Type const t{make_type<T>()};
            reduce(op, MPI_IN_PLACE, data, t, count);
        }
    };
} // namespace __2_
MPIKIT_END_NAMESPACE

#endif /* MPIReduction__2__h */
