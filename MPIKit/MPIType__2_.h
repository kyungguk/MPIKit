//
//  MPIType__2_.h
//  MPIKit
//
//  Created by KYUNGGUK MIN on 7/29/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef MPIType__2__h
#define MPIType__2__h

#include <MPIKit/MPIKit-config.h>

// MARK:- Version 2
//
#include <UtilityKit/UtilityKit.h>
#include <initializer_list>
#include <type_traits>
#include <utility>
#include <limits>
#include <string>

#if defined(__APPLE__)
#include <mpich2/mpi.h>
#else
#include <mpi.h>
#endif

MPIKIT_BEGIN_NAMESPACE
#if defined(MPIKIT_INLINE_VERSION) && MPIKIT_INLINE_VERSION == 2
inline
#endif
namespace __2_ {
    /**
     @brief Wrapper for MPI_Datatype.
     @discussion Any operations on an invalid instance is considered to be ill-formed.
     */
    class Type {
        static constexpr int int_max = std::numeric_limits<int>::max();

        long _alignment{0};
        MPI_Datatype _t{MPI_DATATYPE_NULL};
        bool _should_free{false};

#pragma mark Constructors
        //
        /**
         @brief Construct a Type instance by wrapping MPI predefined datatypes.
         @discussion This should only be used for constructing MPI predefined datatypes.
         @tparam Alignment Alignment of the underlying type.
         @param t One of predefined MPI datatype.
        */
        template <long Alignment>
        constexpr explicit Type(MPI_Datatype const t, std::integral_constant<long, Alignment>) noexcept : _alignment(Alignment), _t(t), _should_free(false) {
            static_assert(Alignment > 0 && UTL::is_power_of_2(Alignment), "invalid alignment");
        }

        /**
         @brief Construct a Type instance by taking the ownership of the MPI datatype referenced by the handle.
         @discussion This is the funnel point of all derived Type construction

         On instantiation, the MPI datatype passed are automatically comitted.
         @param t A MPI datatype handle, which must not be one of the predefined handles (including the null type).
         Upon return from the call, `t' is set to MPI_DATATYPE_NULL.
         @param alignment Alignment of the underlying type. It must be a positive integer power of 2.
         */
        explicit Type(MPI_Datatype &&t, long const alignment);

    public:
        ~Type();

        /**
         @brief Construct a Type instance whose state is invalid.
         @discussion Unlike MPI spec, an empty datatype is not supported.
         */
        constexpr explicit Type() noexcept = default;

        /**
         @brief Construct a Type instance by compositing multiple, heterogeneous datatypes.
         @discussion Composition is done using MPI_Type_create_struct.

         It create a typemap reflecting C struct's which only contain predefined datatypes and/or derived datatypes that can themselves be a composite type.

         For example, a composite datatype
         `Type type_A{{Type::native<char>(), Type::native<long>(), Type::native<char>(), Type::native<short>()}}' corresponds to `struct A { char; long; char; short; }'.

         The new type `type_A' will have a lower bound of 0 and and an extent of sizeof(long)*3.
         The true lower bound is also 0, but the true extent will be sizeof(long)*2 + sizeof(short)*2.

         Similarly, `Type type_B{{type_A, Type::native<char>()}}' corresponds to a `struct B { A; char; }', where `type_A' itself is a composite type.

         The native alignment is respected and the extent of the new composite type is adjusted such that `extent == sizeof("struct")'.
         @param il Initializer list holding types to be composited.
         Any invalid types are silently removed from the list.
         If all are invalid, so is the returned datatype.
         */
        explicit Type(std::initializer_list<Type> const &il);
        Type &operator=(std::initializer_list<Type> const &il) { Type{il}.swap(*this); return *this; }

        // move
        Type(Type &&o) noexcept : Type() { swap(o); }
        Type &operator=(Type &&) noexcept;
        // copy; note the duplication of underlying MPI object is expensive
        Type(Type const &o); // duplication
        Type &operator=(Type const&);

        /**
         @brief Swap the content of *this with other.
         */
        void swap(Type &o) noexcept { std::swap(_alignment, o._alignment); std::swap(_t, o._t); std::swap(_should_free, o._should_free); }

#pragma mark Query
        /**
         @brief Associates a label with *this.
         */
        void set_label(char const *label);
        /**
         @brief Returns the label associated with *this.
         */
        std::string label() const;

        /**
         @brief Returns false is the state of *this is invalid.
         @discussion Any operations on an invalid instance are considered to be ill-formed.
         */
        explicit operator bool() const noexcept { return _t != MPI_DATATYPE_NULL; }

        /**
         @brief Returns MPI Datatype handle.
         @discussion Any MPI operations that modify the state of this handle is discouraged and considered to ill-formed.

         If such operations are needed, duplicate the MPI Datatype referenced by this handle using MPI_Type_dup.
         */
        MPI_Datatype const &operator*() const noexcept { return _t; }

        /**
         @brief Return C alignment associated with the underlying type.
         */
        long const &alignment() const noexcept { return _alignment; }

        /**
         @brief Get the total size, in bytes, of the entries in the type signature associated with *this.
         @discussion That is, the total size of the data in a message that would be created with this datatype.
         Entries that occur multiple times in the datatype are counted with their multiplicity.

         Internally, MPI_Type_size_x is called.
         @return Length of *this; or a nil optional if the value to be returned is too large for the integer type.
         */
        UTL::Optional<long> signature_size() const;

        /**
         @brief Returns the lower bound of datatype.
         @discussion Internally, MPI_Type_get_extent_x or MPI_Type_get_true_extent_x is called.
         @param should_get_true_lower_bound If true, the true lower bound is returned.
         @return Lower bound of *this; or a nil optional if the value to be returned is too large for the integer type.
         */
        UTL::Optional<long> lower_bound(bool const should_get_true_lower_bound = false) const { return _extent(should_get_true_lower_bound).lower_bound; }

        /**
         @brief Returns the extent of datatype.
         @discussion Internally, MPI_Type_get_extent_x or MPI_Type_get_true_extent_x is called.
         @param should_get_true_extent If true, the true extent is returned.
         @return Extent of *this; or a nil optional if the value to be returned is too large for the integer type.
         */
        UTL::Optional<long> extent(bool const should_get_true_extent = false) const { return _extent(should_get_true_extent).extent; }

        /**
         @brief Returns the number of entries received. (Again, we count entries, each of type datatype, not bytes.) 
         @discussion The datatype of *this should match the argument provided by the receive call that set the status variable. 

         Internally, MPI_Get_count is called.
         @note Use of this is intened for cases where direct interactions with MPI routines are needed.
         @return Number of received entries; or a nil optional if the value to be returned is too large for the integer type.
         */
        UTL::Optional<long> get_count(MPI_Status const &status) const;

        /**
         @brief Returns the number of received basic elements.
         @discussion The datatype of *this should match the argument provided by the receive call that set the status variable.

         `get_count' has a different behavior. It returns the number of "top-level entries" received, i.e. the number of "copies" of type datatype.

         Internally, MPI_Get_elements_x is called.
         @note Use of this is intened for cases where direct interactions with MPI routines are needed.
         @return Number of received basic elements of *this; or a nil optional if the value to be returned is too large for the integer type.
         */
        UTL::Optional<long> get_elements(MPI_Status const &status) const;

#pragma mark Derived Types
        /**
         @brief Create vector datatype by replicating *this.
         @discussion Internally, MPI_Type_vector is called.

         MPI_Type_contiguous is equivalent to calling this with block = stride = 1.
         @tparam block_size The number of blocks (non-negative integer).
         @tparam block The number of elements in each block (non-negative integer).
         @tparam stride The number of elements between start of each block (integer).
         */
        template <long block_size, long block = 1, long stride = block>
        Type vector() const;

        /**
         @brief Create indexed datatype by replicating *this.
         @discussion Internally, MPI_Type_indexed is called.
         @tparam block_size The number of blocks (non-negative integer).
         @param block The number of elements per block (array of non-negative integers).
         @param displacement Displacement for each block, in multiples of oldtype extent (array of integer).
         */
        template <long block_size>
        Type indexed(UTL::Vector<long, block_size> const &block, UTL::Vector<long, block_size> const &displacement) const;

        /**
         @brief Create indexed block datatype by replicating *this.
         @discussion Internally, MPI_Type_create_indexed_block is called.
         @tparam S The length of array of displacements (non-negative integer).
         @tparam block The size of block (non-negative integer).
         @param displacement Array of displacements (array of integer).
         */
        template <long S, long block>
        Type indexed(UTL::Vector<long, S> const &displacement) const;

        /**
         @brief Create subarray datatype by describing an ND subarray of an ND array.
         @discussion Internally, MPI_Type_create_subarray is called.

         C storage ordering is implicitly assumed.
         @tparam ND The number of array dimensions (positive integer).
         @param dims The number of elements of type `*this' in each dimension of the full array (array of positive integers).
         @param slice_locs The starting coordinates of the subarray in each dimension (array of non-negative integers).
         @param slice_lens The number of elements of type `*this' in each dimension of the subarray (array of positive integers).
         */
        template <long ND>
        Type subarray(UTL::Vector<long, ND> const &dims, UTL::Vector<long, ND> const &slice_locs, UTL::Vector<long, ND> const &slice_lens) const;

        /**
         @brief Returns a new datatype that is identical to oldtype, except that the alignment of this new datatype is set to be Alignment.
         @discussion If necessary, the extent is adjusted, using MPI_Type_create_resized call, so that it becomes a next multiple of the new alignment.
         @tparam Alignment New alignment to be associated with the datatype, which is greater than or equal to the current alignment.
         */
        template <long Alignment>
        Type realigned() const;

#pragma mark Predefined Types
        /// native char type.
        template <class T>
        static constexpr auto native() noexcept -> typename std::enable_if<
        std::is_same<char, typename std::decay<T>::type>::value, Type>::type {
            return Type{MPI_CHAR, std::integral_constant<long, alignof(char)>{}};
        }
        /// native unsigned char type.
        template <class T>
        static constexpr auto native() noexcept -> typename std::enable_if<
        std::is_same<unsigned char, typename std::decay<T>::type>::value, Type>::type {
            return Type{MPI_UNSIGNED_CHAR, std::integral_constant<long, alignof(unsigned char)>{}};
        }

        /// native short type.
        template <class T>
        static constexpr auto native() noexcept -> typename std::enable_if<
        std::is_same<short, typename std::decay<T>::type>::value, Type>::type {
            return Type{MPI_SHORT, std::integral_constant<long, alignof(short)>{}};
        }
        /// native unsigned short type.
        template <class T>
        static constexpr auto native() noexcept -> typename std::enable_if<
        std::is_same<unsigned short, typename std::decay<T>::type>::value, Type>::type {
            return Type{MPI_UNSIGNED_SHORT, std::integral_constant<long, alignof(unsigned short)>{}};
        }

        /// native int type.
        template <class T>
        static constexpr auto native() noexcept -> typename std::enable_if<
        std::is_same<int, typename std::decay<T>::type>::value, Type>::type {
            return Type{MPI_INT, std::integral_constant<long, alignof(int)>{}};
        }
        /// native unsigned int type.
        template <class T>
        static constexpr auto native() noexcept -> typename std::enable_if<
        std::is_same<unsigned, typename std::decay<T>::type>::value, Type>::type {
            return Type{MPI_UNSIGNED, std::integral_constant<long, alignof(unsigned)>{}};
        }

        /// native long type.
        template <class T>
        static constexpr auto native() noexcept -> typename std::enable_if<
        std::is_same<long, typename std::decay<T>::type>::value, Type>::type {
            return Type{MPI_LONG, std::integral_constant<long, alignof(long)>{}};
        }
        /// native unsigned long type.
        template <class T>
        static constexpr auto native() noexcept -> typename std::enable_if<
        std::is_same<unsigned long, typename std::decay<T>::type>::value, Type>::type {
            return Type{MPI_UNSIGNED_LONG, std::integral_constant<long, alignof(unsigned long)>{}};
        }

        /// native float type.
        template <class T>
        static constexpr auto native() noexcept -> typename std::enable_if<
        std::is_same<float, typename std::decay<T>::type>::value, Type>::type {
            return Type{MPI_FLOAT, std::integral_constant<long, alignof(float)>{}};
        }
        /// native double type.
        template <class T>
        static constexpr auto native() noexcept -> typename std::enable_if<
        std::is_same<double, typename std::decay<T>::type>::value, Type>::type {
            return Type{MPI_DOUBLE, std::integral_constant<long, alignof(double)>{}};
        }

    private: // helpers
        template <long S>
        static UTL::Vector<int, S> lV_to_iV(UTL::Vector<long, S> const &lv) noexcept {
            UTL::Vector<int, S> iv;
            for (long i = 0; i < S; ++i) {
                iv[i] = static_cast<int const>(lv[i]);
            }
            return iv;
        }

        struct _Extent { UTL::Optional<long> lower_bound, extent; };
        _Extent _extent(bool const should_get_true_extent) const;

        // return a new type whose extent is, if necessary, adjusted to be the next multiple of the alignment
        // no argument check is made; make sure that the proper alignment associated with *this is passed
        //
        Type _resized(long const alignment) const;
    };
} // namespace __2_
MPIKIT_END_NAMESPACE


// template member implementations
//
#include <MPIKit/MPIType__2_.hh>

#endif /* MPIType__2__h */
