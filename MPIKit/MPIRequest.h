//
//  MPIRequest.h
//  MPIKit
//
//  Created by KYUNGGUK MIN on 11/13/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#ifndef MPIRequest_h
#define MPIRequest_h

#include <MPIKit/MPIKit-config.h>

// MARK:- Version 1
//
#if defined(__APPLE__)
#include <mpich2/mpi.h>
#else
#include <mpi.h>
#endif

MPIKIT_BEGIN_NAMESPACE
#if defined(MPIKIT_INLINE_VERSION) && MPIKIT_INLINE_VERSION == 1
inline
#endif
namespace __1_ {
    class Communicator;

    /**
     @brief Wrapper for MPI request token.
     */
    class Request {
        friend class Communicator;
        MPI_Request _token{MPI_REQUEST_NULL};

    public:
        Request(Request &&) noexcept = default;
        Request &operator=(Request &&) noexcept = default;
        explicit Request() noexcept = default;

        /**
         @brief Native handle accessor.
         */
        MPI_Request *operator&() noexcept { return &_token; }

        /**
         Block until the request is done.
         */
        void wait();
        /**
         Return true if the request is done and set the internal token to MPI_REQUEST_NULL; otherwise, return false.
         */
        bool test();
    };
} // namespace __1_
MPIKIT_END_NAMESPACE

#endif /* MPIRequest_h */
