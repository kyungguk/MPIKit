//
//  MPIType__2_.cc
//  MPIKit
//
//  Created by KYUNGGUK MIN on 7/29/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "MPIType__2_.h"
#include "MPIComm__2_.h"
#include <algorithm>
#include <stdexcept>
#include <iostream>
#include <string>

// MARK:- MPK::__2_::Type
//
MPK::__2_::Type::~Type()
{
    // test of MPI_Finalized is to prevent *this from calling MPI_Type_free after MPI has been finalized
    //
    if (*this && _should_free && !Comm::finalized() && MPI_Type_free(&_t) != MPI_SUCCESS) {
        println(std::cerr, "%% ", __PRETTY_FUNCTION__, " - MPI_Type_free(...) returned error");
#if defined(MPIKIT_SHOULD_TERMINATE_ON_FAILURE_IN_DESTRUCTOR) && MPIKIT_SHOULD_TERMINATE_ON_FAILURE_IN_DESTRUCTOR
        std::terminate();
#endif
    }
}

auto MPK::__2_::Type::operator=(Type &&o) noexcept
-> Type &{
    if (this != &o) {
        Type{std::move(o)}.swap(*this);
    }
    return *this;
}
auto MPK::__2_::Type::operator=(Type const &o)
-> Type &{
    if (this != &o) {
        Type{o}.swap(*this);
    }
    return *this;
}

MPK::__2_::Type::Type(MPI_Datatype &&t, long const alignment)
{
    // commit
    //
    if (MPI_Type_commit(&t) != MPI_SUCCESS) {
        //MPI_Type_free(&t);
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Type_commit(...) returned error");
    }

    // init
    //
    std::swap(_t, t); // the argument is nullified after swap
    _should_free = true;
    _alignment = alignment;
}
MPK::__2_::Type::Type(Type const &o)
{
    // duplicate only if other is valid and not predefined datatype
    //
    if (o && o._should_free) {
        // duplicate
        //
        MPI_Datatype t;
        if (MPI_Type_dup(*o, &t) != MPI_SUCCESS) { // the newtype has the same committed state as the old oldtype
            throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Type_dup(...) returned error");
        }

        // init
        //
        std::swap(_t, t); // the argument is nullified after swap
        _should_free = true;
        _alignment = o._alignment;
    } else {
        _t = o._t;
        _alignment = o._alignment;
    }
}
MPK::__2_::Type::Type(std::initializer_list<Type> const &il)
{
    // filter out invalid types
    //
    UTL::DynamicArray<MPI_Datatype> types;
    UTL::DynamicArray<MPI_Aint> disps; // initially filled with extents
    UTL::DynamicArray<long> aligns;
    for (Type const &t : il) {
        if (t) {
            types.push_back(*t);
            disps.push_back(t.extent()()); // assume that non-nill is returned
            aligns.push_back(t.alignment());
        }
    }
    if (types.empty()) { // bail out if all are invalid
        return;
    }

    // calculate displacements
    //
    long ub = 0; // this holds the upper bound
    for (long i = 0, n = types.size(); i < n; ++i) {
        long const align = aligns.at(i);
        if (ub % align) { // if the cumulative sum IS NOT divisable by the extent, adjust so that it IS
            ub = (ub/align + 1)*align; // this is now adjusted lower bound
        }
        MPI_Aint &disp = disps.at(i);
        ub += disp; // this is the new upper bound
        disp = ub - disp; // now disp holds the lower bound
    }

    // block sizes are all 1
    //
    UTL::DynamicArray<int> const blocks(types.size(), 1);

    // create composite type
    //
    MPI_Datatype t;
    if (MPI_Type_create_struct(static_cast<int>(types.size()), blocks.data(), disps.data(), types.data(), &t) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Type_create_struct(...) returned error");
    }
    Type{std::move(t), 1 /*this is dummy alignment*/}
    ._resized(*std::max_element(aligns.begin(), aligns.end()))
    .swap(*this); // commit is done in construction
}

UTL::Optional<long> MPK::__2_::Type::signature_size() const
{
    MPI_Count size;
    if (MPI_Type_size_x(**this, &size) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Type_size_x(...) returned error");
    }
    if (MPI_UNDEFINED == size) {
        return {};
    } else {
        return size;
    }
}
auto MPK::__2_::Type::_extent(bool const should_get_true_extent) const
-> _Extent {
    MPI_Count lb, extent;
    if (should_get_true_extent) {
        if (MPI_Type_get_true_extent_x(**this, &lb, &extent) != MPI_SUCCESS) {
            throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Type_get_true_extent_x(...) returned error");
        }
    } else {
        if (MPI_Type_get_extent_x(**this, &lb, &extent) != MPI_SUCCESS) {
            throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Type_get_extent_x(...) returned error");
        }
    }
    _Extent result;
    if (MPI_UNDEFINED != lb) {
        result.lower_bound = lb;
    }
    if (MPI_UNDEFINED != extent) {
        result.extent = extent;
    }
    return result;
}
UTL::Optional<long> MPK::__2_::Type::get_count(MPI_Status const &status) const
{
    int count;
    if (MPI_Get_count(&status, **this, &count) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Get_count(...) returned error");
    }
    if (MPI_UNDEFINED == count) {
        return {};
    } else {
        return count;
    }
}
UTL::Optional<long> MPK::__2_::Type::get_elements(MPI_Status const &status) const
{
    MPI_Count count;
    if (MPI_Get_elements_x(&status, **this, &count) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Get_elements_x(...) returned error");
    }
    if (MPI_UNDEFINED == count) {
        return {};
    } else {
        return count;
    }
}

auto MPK::__2_::Type::_resized(long const alignment) const
-> Type {
    // adjust extent
    //
    long extent = this->extent()();
    if (extent % alignment) {
        extent = (extent/alignment + 1)*alignment;
    }

    // resized
    //
    constexpr long lb = 0; // all Type instances are assumed to have 0 lb (i.e., lb marker is not used)
    MPI_Datatype t;
    if (MPI_Type_create_resized(**this, lb, extent, &t) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Type_create_resized(...) returned error");
    }
    return Type{std::move(t), alignment}; // commit is done in construction
}

void MPK::__2_::Type::set_label(char const *label)
{
    if (MPI_Type_set_name(**this, label) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Type_set_name(...) returned error");
    }
}
std::string MPK::__2_::Type::label() const
{
    char label[MPI_MAX_OBJECT_NAME];
    int len;
    if (MPI_Type_get_name(**this, label, &len) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Type_get_name(...) returned error");
    }
    return label;
}
