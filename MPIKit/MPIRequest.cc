//
//  MPIRequest.cc
//  MPIKit
//
//  Created by KYUNGGUK MIN on 11/13/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#include "MPIRequest.h"

// MARK:- Version 1
//
#include <stdexcept>
#include <string>

void MPK::__1_::Request::wait()
{
    if (MPI_Wait(&*this, MPI_STATUS_IGNORE) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Wait failed");
    }
}
bool MPK::__1_::Request::test()
{
    int flag;
    if (MPI_Test(&*this, &flag, MPI_STATUS_IGNORE) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Test failed");
    }
    return flag;
}
