//
//  MPIGroup__2_.cc
//  MPIKit
//
//  Created by KYUNGGUK MIN on 7/29/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "MPIGroup__2_.h"
#include "MPIBaseComm__2_.h"
#include <stdexcept>
#include <iostream>
#include <string>

// MARK:- MPK::__2_::Group
//
MPK::__2_::Group::~Group()
{
    if (MPI_GROUP_NULL != _g && MPI_GROUP_EMPTY != _g && MPI_Group_free(&_g) != MPI_SUCCESS) {
        println(std::cerr, "%% ", __PRETTY_FUNCTION__, " - MPI_Group_free(...) returned error");
#if defined(MPIKIT_SHOULD_TERMINATE_ON_FAILURE_IN_DESTRUCTOR) && MPIKIT_SHOULD_TERMINATE_ON_FAILURE_IN_DESTRUCTOR
        std::terminate();
#endif
    }
}

auto MPK::__2_::Group::operator=(Group &&o) noexcept
-> Group &{
    if (this != &o) {
        Group{std::move(o)}.swap(*this);
    }
    return *this;
}

int MPK::__2_::Group::size() const
{
    int size;
    if (MPI_Group_size(**this, &size) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Group_size(...) returned error");
    }
    return size;
}
auto MPK::__2_::Group::rank() const
-> UTL::Optional<Rank> {
    int rank;
    if (MPI_Group_rank(**this, &rank) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Group_rank(...) returned error");
    }
    if (MPI_UNDEFINED == rank) {
        return {};
    } else {
        return Rank{rank};
    }
}

auto MPK::__2_::Group::translate(Rank const a_rank, Group const &to) const
-> UTL::Optional<Rank> {
    constexpr long n = 1;
    Rank b_rank{MPI_UNDEFINED};
    if (MPI_Group_translate_ranks(**this, n, &a_rank, *to, &b_rank) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Group_translate_ranks(...) returned error");
    }
    if (MPI_UNDEFINED == b_rank) {
        return {};
    } else {
        return b_rank;
    }
}
auto MPK::__2_::Group::translate(Rank const a_rank, _Comm const &to) const
-> UTL::Optional<Rank> {
    return translate(a_rank, to.group());
}

auto MPK::__2_::Group::compare(Group const &other) const
-> Comparison {
    int _result;
    if (MPI_Group_compare(**this, *other, &_result) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Group_compare(...) returned error");
    }
    Comparison result;
    switch (_result) {
        case MPI_IDENT:
            result = identical;
            break;
        case MPI_SIMILAR:
            result = similar;
            break;
        case MPI_UNEQUAL:
            result = unequal;
            break;
        default:
            throw std::domain_error(std::string(__PRETTY_FUNCTION__) + " - unknown Comparison enum");
    }
    return result;
}
auto MPK::__2_::Group::compare(_Comm const &other) const
-> Comparison {
    return compare(other.group());
}

auto MPK::__2_::Group::operator|(Group const &other) const
-> Group {
    MPI_Group g;
    if (MPI_Group_union(**this, *other, &g) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Group_union(...) returned error");
    }
    return Group{std::move(g)};
}
auto MPK::__2_::Group::operator&(Group const &other) const
-> Group {
    MPI_Group g;
    if (MPI_Group_intersection(**this, *other, &g) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Group_intersection(...) returned error");
    }
    return Group{std::move(g)};
}
auto MPK::__2_::Group::operator-(Group const &other) const
-> Group {
    MPI_Group g;
    if (MPI_Group_difference(**this, *other, &g) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Group_difference(...) returned error");
    }
    return Group{std::move(g)};
}

auto MPK::__2_::Group::_included(UTL::DynamicArray<int> const &ranks) const
-> Group {
    MPI_Group g;
    int const n = static_cast<int>(ranks.size());
    if (MPI_Group_incl(**this, n, ranks.data(), &g) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Group_incl(...) returned error");
    }
    return Group{std::move(g)};
}
auto MPK::__2_::Group::_excluded(UTL::DynamicArray<int> const &ranks) const
-> Group {
    MPI_Group g;
    int const n = static_cast<int>(ranks.size());
    if (MPI_Group_excl(**this, n, ranks.data(), &g) != MPI_SUCCESS) {
        throw std::runtime_error(std::string(__PRETTY_FUNCTION__) + " - MPI_Group_excl(...) returned error");
    }
    return Group{std::move(g)};
}
