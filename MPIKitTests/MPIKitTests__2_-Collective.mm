//
//  MPIKitTests__2_-Collective.m
//  MPIKitTests
//
//  Created by KYUNGGUK MIN on 8/7/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#import "MPIKitTests__2_.h"

#include <MPIKit/MPIComm__2_.h>
#include <UtilityKit/UtilityKit.h>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>

using namespace MPK::__2_;

@interface MPIKitTests__2_ (Collective)

@end

@implementation MPIKitTests__2_ (Collective)

- (void)testCollective_LowLevel {
    // barrier
    try {
        Comm::world().barrier();
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
    constexpr Rank root{0};

    // broadcast
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        UTL::DynamicArray<T> v1(100), v2;
        for (auto &v : v1) {
            v.at(0) = arc4random();
            v.at(1) = arc4random();
            v.at(2) = arc4random();
        }

        v2 = v1;
        comm.bcast(v2.data(), v2.size(), root);
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // gather
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        UTL::DynamicArray<T> v1(100), v2(v1);
        for (auto &v : v1) {
            v.at(0) = arc4random();
            v.at(1) = arc4random();
            v.at(2) = arc4random();
        }

        comm.gather(v1.data(), v1.size(), v2.data(), v2.size(), root);
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));

        if (root == comm.rank()) {
            comm.gather(nullptr, {}, v2.data(), v2.size(), root);
        } else {
            comm.gather(v2.data(), v2.size(), nullptr, {}, root);
        }
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));

        std::fill(v2.begin(), v2.end(), T{});
        comm.gather(v1.data(), v1.size(), v2.data(), v2.size()); // allgather
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));

        comm.gather(v2.data(), v2.size()); // in-place allgather
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // scatter
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        UTL::DynamicArray<T> v1(100), v2(v1);
        for (auto &v : v1) {
            v.at(0) = arc4random();
            v.at(1) = arc4random();
            v.at(2) = arc4random();
        }

        comm.scatter(v1.data(), v1.size(), v2.data(), v2.size(), root);
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));

        if (root == comm.rank()) {
            comm.scatter(v2.data(), v2.size(), nullptr, {}, root);
        } else {
            comm.scatter(nullptr, {}, v2.data(), v2.size(), root);
        }
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));

        std::fill(v2.begin(), v2.end(), T{});
        comm.all2all(v1.data(), v1.size(), v2.data(), v2.size());
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));

        comm.all2all(v2.data(), v2.size());
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testCollective_HighLevel {
    // barrier
    try {
        Comm::world().barrier();
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
    constexpr Rank root{0};

    // bcast scalar
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        T v1{arc4random(), arc4random(), arc4random()}, v2;

        comm.bcast(v2 = v1, root);
        XCTAssert(UTL::reduce_bit_and(v1 == v2));
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // bcast padded array
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        UTL::PaddedArray<T, 3> v1(100), v2(v1);
        for (auto first = v1.pad_begin(), last = v1.pad_end(); first != last; ++first) {
            first->at(0) = arc4random();
            first->at(1) = arc4random();
            first->at(2) = arc4random();
        }

        comm.bcast(v2 = v1, root);
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // bcast ND array
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        constexpr long ND = 3, Pad = 2;
        using ArrayND = UTL::ArrayND<T, ND, Pad>;
        ArrayND a1({3, 5, 9}), _a2(a1.dims() + 10L);
        ArrayND::size_vector_type const loc{1}, len = a1.dims();
        auto s2 = UTL::make_slice<ArrayND::pad_size()>(_a2, loc, len);

        for (auto &v : a1.flat_array()) {
            v.at(0) = arc4random();
            v.at(1) = arc4random();
            v.at(2) = arc4random();
        }

        {
            comm.bcast(s2 = a1, root);
            XCTAssert(std::equal(a1.leaf_pad_begin(), a1.leaf_pad_end(), s2.leaf_pad_begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
        a1.fill(T{});
        {
            comm.bcast(a1 = s2, root);
            XCTAssert(std::equal(a1.leaf_pad_begin(), a1.leaf_pad_end(), s2.leaf_pad_begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // bcast dynamic array
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        UTL::DynamicArray<T> v1(100), v2;
        for (auto &v : v1) {
            v.at(0) = arc4random();
            v.at(1) = arc4random();
            v.at(2) = arc4random();
        }

        {
            comm.bcast(v2 = v1, root);
            XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
        v1.clear();
        {
            comm.bcast(v2 = v1, root);
            XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // bcast static array
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        UTL::StaticArray<T, 100> v1(100), v2;
        for (auto &v : v1) {
            v.at(0) = arc4random();
            v.at(1) = arc4random();
            v.at(2) = arc4random();
        }

        {
            comm.bcast(v2 = v1, root);
            XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
        v1.clear();
        {
            comm.bcast(v2 = v1, root);
            XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // std::vector
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        std::vector<T> v1(100), v2;
        for (auto &v : v1) {
            v.at(0) = arc4random();
            v.at(1) = arc4random();
            v.at(2) = arc4random();
        }

        {
            comm.bcast(v2 = v1, root);
            XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
        v1.clear();
        {
            comm.bcast(v2 = v1, root);
            XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testCollective_LeakCheck {
    try {
        constexpr long n = 30000;
        for (long i = 0; i < n; ++i) {
            @autoreleasepool {
                [self testCollective_LowLevel];
                [self testCollective_HighLevel];
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

@end
