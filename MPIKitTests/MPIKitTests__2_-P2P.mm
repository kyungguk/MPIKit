//
//  MPIKitTests__2_-P2P.m
//  MPIKitTests
//
//  Created by KYUNGGUK MIN on 8/6/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#import "MPIKitTests__2_.h"

#include <MPIKit/MPIComm__2_.h>
#include <UtilityKit/UtilityKit.h>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>

using namespace MPK::__2_;

@interface MPIKitTests__2_ (P2P)

@end

@implementation MPIKitTests__2_ (P2P)

- (void)testRequest {
    try {
        Request req1, req2{std::move(req1)};
        XCTAssert(req2.test());
        req1.wait();
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testP2P_LowLevel {
    // barrier
    try {
        Comm::world().barrier();
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // persistence
    try {
        Comm const comm = Comm::world().duplicated();

        using T = std::tuple<UTL::SIMDVector<double, 3>, std::pair<char, short>>;
        UTL::DynamicArray<T> v1(10), v2(v1);

        Request send_req = comm.ssend_init(v1.data(), v1.size(), std::make_pair(comm.rank(), Tag{1}));
        Request recv_req = comm.recv_init(v2.data(), v2.size(), std::make_pair(comm.rank(), Tag::any()));

        constexpr long n = 10;
        for (long i = 0; i < n; ++i) {
            for (auto &v : v1) {
                std::get<0>(v).at(0) = arc4random();
                std::get<0>(v).at(1) = arc4random();
                std::get<0>(v).at(2) = arc4random();
            }
            send_req.start();
            recv_req.start();
            recv_req.wait();
            send_req.wait();

            for (long i = 0; i < v1.size(); ++i) {
                XCTAssert(UTL::reduce_bit_and(std::get<0>(v1.at(i)) == std::get<0>(v2.at(i))));
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // non-blocking send/blocking recv
    try {
        Comm const comm = Comm::world().split(1);

        using T = UTL::SIMDVector<double, 3>;
        UTL::DynamicArray<T> v1(10), v2;
        for (auto &v : v1) {
            v.at(0) = arc4random();
            v.at(1) = arc4random();
            v.at(2) = arc4random();
        }

        Request req = comm.issend(v1.data(), v1.size(), {comm.rank(), Tag{1}});
        {
            MPI_Message msg;
            Type const t = make_type(T{});
            v2.resize(t.get_count(comm.probe({comm.rank(), Tag::any()}, &msg))());
            comm.recv(v2.data(), v2.size(), std::move(msg));
        }
        req.wait();

        for (long i = 0; i < v1.size(); ++i) {
            XCTAssert(UTL::reduce_bit_and(v1.at(i) == v2.at(i)));
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // blocking send-recv
    try {
        Comm const comm = Comm::world().split(1);

        using T = UTL::SIMDVector<double, 3>;
        UTL::DynamicArray<T> v1(10), v2(v1);
        for (auto &v : v1) {
            v.at(0) = arc4random();
            v.at(1) = arc4random();
            v.at(2) = arc4random();
        }

        comm.send_recv(v1.data(), v1.size(), {comm.rank(), Tag{1}},
                       v2.data(), v2.size(), {comm.rank(), Tag::any()});

        comm.send_recv(v2.data(), v2.size(), {comm.rank(), Tag{1}}, {comm.rank(), Tag::any()});

        for (long i = 0; i < v1.size(); ++i) {
            XCTAssert(UTL::reduce_bit_and(v1.at(i) == v2.at(i)));
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testP2P_NonBlockingSend_BlockingRecv {
    // barrier
    try {
        Comm::world().barrier();
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // scalar
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        T v1, v2;
        for (auto &x : v1) {
            x = arc4random();
        }

        Request req = comm.issend(v1, {comm.rank(), Tag{1}});
        comm.recv(v2, {comm.rank(), Tag::any()});
        req.wait();

        XCTAssert(UTL::reduce_bit_and(v1 == v2));
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // padded array
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        UTL::PaddedArray<T, 3> v1(10), v2(v1);
        for (auto first = v1.pad_begin(), last = v1.pad_end(); first != last; ++first) {
            first->at(0) = arc4random();
            first->at(1) = arc4random();
            first->at(2) = arc4random();
        }

        Request req = comm.issend(v1, {comm.rank(), Tag{1}});
        comm.recv(v2, {comm.rank(), Tag::any()});
        req.wait();

        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // ND array
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        constexpr long ND = 3, Pad = 2;
        using ArrayND = UTL::ArrayND<T, ND, Pad>;
        ArrayND a1({3, 5, 9}), _a2(a1.dims() + 10L);
        ArrayND::size_vector_type const loc{1}, len = a1.dims();
        auto s2 = UTL::make_slice<ArrayND::pad_size()>(_a2, loc, len);

        for (auto &v : a1.flat_array()) {
            v.at(0) = arc4random();
            v.at(1) = arc4random();
            v.at(2) = arc4random();
        }

        {
            Request req = comm.issend(a1, {comm.rank(), Tag{1}});
            comm.recv(s2, {comm.rank(), Tag::any()});
            req.wait();

            XCTAssert(std::equal(a1.leaf_pad_begin(), a1.leaf_pad_end(), s2.leaf_pad_begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
        a1.fill(T{});
        {
            Request req = comm.issend(s2, {comm.rank(), Tag{1}});
            comm.recv(a1, {comm.rank(), Tag::any()});
            req.wait();

            XCTAssert(std::equal(a1.leaf_pad_begin(), a1.leaf_pad_end(), s2.leaf_pad_begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // ND subarray
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        constexpr long ND = 3, Pad = 2;
        using ArrayND = UTL::ArrayND<T, ND, Pad>;
        ArrayND v1({3, 5, 9}), v2(v1.dims());
        ArrayND::size_vector_type const loc{1}, len = v1.dims() - 1L;
        auto slice_v1 = UTL::make_slice<ArrayND::pad_size()>(v1, loc, len);
        auto slice_v2 = UTL::make_slice<ArrayND::pad_size()>(v2, loc, len);

        for (auto &v : v1.flat_array()) {
            v.at(0) = arc4random();
            v.at(1) = arc4random();
            v.at(2) = arc4random();
        }

        Type const t = make_type<T>().subarray(v1.max_dims(), loc, len + 2*Pad); // including paddings
        {
            Request req = comm.issend({v1.data(), t}, 1, {comm.rank(), Tag{1}});
            comm.recv({v2.data(), t}, 1, {comm.rank(), Tag::any()});
            req.wait();

            XCTAssert(std::equal(slice_v1.leaf_pad_begin(), slice_v1.leaf_pad_end(), slice_v2.leaf_pad_begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
        slice_v2.fill(T{});
        {
            comm.send_recv({v1.data(), t}, 1, {comm.rank(), Tag{1}},
                           {v2.data(), t}, 1, {comm.rank(), Tag::any()});

            XCTAssert(std::equal(slice_v1.leaf_pad_begin(), slice_v1.leaf_pad_end(), slice_v2.leaf_pad_begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // dynamic array
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        UTL::DynamicArray<T> v1(10), v2;
        for (auto &v : v1) {
            v.at(0) = arc4random();
            v.at(1) = arc4random();
            v.at(2) = arc4random();
        }

        {
            Request req = comm.issend(v1, {comm.rank(), Tag{1}});
            comm.recv(v2, {comm.rank(), Tag::any()});
            req.wait();

            XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
        v1.clear();
        {
            Request req = comm.issend(v1, {comm.rank(), Tag{1}});
            comm.recv(v2, {comm.rank(), Tag::any()});
            req.wait();

            XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // static array
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        UTL::StaticArray<T, 10> v1(10), v2;
        for (auto &v : v1) {
            v.at(0) = arc4random();
            v.at(1) = arc4random();
            v.at(2) = arc4random();
        }

        {
            Request req = comm.issend(v1, {comm.rank(), Tag{1}});
            comm.recv(v2, {comm.rank(), Tag::any()});
            req.wait();

            XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
        v1.clear();
        {
            Request req = comm.issend(v1, {comm.rank(), Tag{1}});
            comm.recv(v2, {comm.rank(), Tag::any()});
            req.wait();

            XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // std::vector
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        std::vector<T> v1(10), v2;
        for (auto &v : v1) {
            v.at(0) = arc4random();
            v.at(1) = arc4random();
            v.at(2) = arc4random();
        }

        {
            Request req = comm.issend(v1, {comm.rank(), Tag{1}});
            comm.recv(v2, {comm.rank(), Tag::any()});
            req.wait();

            XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
        v1.clear();
        {
            Request req = comm.issend(v1, {comm.rank(), Tag{1}});
            comm.recv(v2, {comm.rank(), Tag::any()});
            req.wait();

            XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testP2P_BlockingSendRecvReplace {
    // barrier
    try {
        Comm::world().barrier();
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // scalar
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        T v1, v2;
        for (auto &x : v1) {
            x = arc4random();
        }

        comm.send_recv(v2 = v1, {comm.rank(), Tag{1}}, {comm.rank(), Tag::any()});
        XCTAssert(UTL::reduce_bit_and(v1 == v2));
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // padded array
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        UTL::PaddedArray<T, 3> v1(10), v2(v1);
        for (auto first = v1.pad_begin(), last = v1.pad_end(); first != last; ++first) {
            first->at(0) = arc4random();
            first->at(1) = arc4random();
            first->at(2) = arc4random();
        }

        comm.send_recv(v2 = v1, {comm.rank(), Tag{1}}, {comm.rank(), Tag::any()});
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // ND array
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        constexpr long ND = 3, Pad = 2;
        using ArrayND = UTL::ArrayND<T, ND, Pad>;
        ArrayND a1({3, 5, 9}), _a2(a1.dims() + 10L);
        ArrayND::size_vector_type const loc{1}, len = a1.dims();
        auto s2 = UTL::make_slice<ArrayND::pad_size()>(_a2, loc, len);

        for (auto &v : a1.flat_array()) {
            v.at(0) = arc4random();
            v.at(1) = arc4random();
            v.at(2) = arc4random();
        }

        {
            comm.send_recv(s2 = a1, {comm.rank(), Tag{1}}, {comm.rank(), Tag::any()});

            XCTAssert(std::equal(a1.leaf_pad_begin(), a1.leaf_pad_end(), s2.leaf_pad_begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
        s2.fill(T{});
        {
            comm.send_recv(a1 = s2, {comm.rank(), Tag{1}}, {comm.rank(), Tag::any()});

            XCTAssert(std::equal(a1.leaf_pad_begin(), a1.leaf_pad_end(), s2.leaf_pad_begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // dynamic array
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        UTL::DynamicArray<T> v1(10), v2;
        for (auto &v : v1) {
            v.at(0) = arc4random();
            v.at(1) = arc4random();
            v.at(2) = arc4random();
        }

        {
            comm.send_recv(v2 = v1, {comm.rank(), Tag{1}}, {comm.rank(), Tag::any()});
            XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
        v1.clear();
        {
            comm.send_recv(v2 = v1, {comm.rank(), Tag{1}}, {comm.rank(), Tag::any()});
            XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // static array
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        UTL::StaticArray<T, 10> v1(10), v2;
        for (auto &v : v1) {
            v.at(0) = arc4random();
            v.at(1) = arc4random();
            v.at(2) = arc4random();
        }

        {
            comm.send_recv(v2 = v1, {comm.rank(), Tag{1}}, {comm.rank(), Tag::any()});
            XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
        v1.clear();
        {
            comm.send_recv(v2 = v1, {comm.rank(), Tag{1}}, {comm.rank(), Tag::any()});
            XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // std::vector
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        std::vector<T> v1(10), v2;
        for (auto &v : v1) {
            v.at(0) = arc4random();
            v.at(1) = arc4random();
            v.at(2) = arc4random();
        }

        {
            comm.send_recv(v2 = v1, {comm.rank(), Tag{1}}, {comm.rank(), Tag::any()});
            XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
        v1.clear();
        {
            comm.send_recv(v2 = v1, {comm.rank(), Tag{1}}, {comm.rank(), Tag::any()});
            XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testP2P_LeakCheck {
    try {
        constexpr long n = 10000;
        for (long i = 0; i < n; ++i) {
            @autoreleasepool {
                [self testP2P_LowLevel];
                [self testP2P_NonBlockingSend_BlockingRecv];
                [self testP2P_BlockingSendRecvReplace];
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

@end
