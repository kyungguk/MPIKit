//
//  MPIKitTests__2_-Comm.m
//  MPIKitTests
//
//  Created by KYUNGGUK MIN on 8/5/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#import "MPIKitTests__2_.h"

#include <MPIKit/MPIComm__2_.h>
#include <MPIKit/MPIGroup__2_.h>
#include <UtilityKit/UtilityKit.h>
#include <iostream>
#include <sstream>
#include <fstream>

using namespace MPK::__2_;

@interface MPIKitTests__2_ (Comm)

@end

@implementation MPIKitTests__2_ (Comm)

- (void)testComm {
    try {
        Comm c;
        XCTAssert(!c);

        c = Comm::self();
        XCTAssert(c && MPI_COMM_SELF == *c && c.compare(Comm::self()) == c.identical);
        println(std::cout, c.label());

        {
            Comm c1 = Comm::world();
            c = std::move(c1);
        }
        XCTAssert(c && MPI_COMM_WORLD == *c && c.compare(Comm::world()) == c.identical);
        println(std::cout, c.label());

        c = c.duplicated();
        c.set_label("duplicated WORLD");
        XCTAssert(c && c.compare(Comm::world()) == c.congruent && c.compare(Comm::self()) == c.congruent);
        println(std::cout, c.label());
        XCTAssert(c.size() == 1 && c.rank() == 0, @"size = %d, rank = %d", c.size(), *c.rank());

        c = c.split(1);
        c.set_label("split comm");
        XCTAssert(c && c.compare(Comm::world()) == c.congruent && c.compare(Comm::self()) == c.congruent);
        println(std::cout, c.label());
        XCTAssert(c.size() == 1 && c.rank() == 0, @"size = %d, rank = %d", c.size(), *c.rank());

        c = c.created(Group{});
        XCTAssert(!c);

        c = Comm::world().split(MPI_UNDEFINED);
        XCTAssert(!c);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testGroup {
    try {
        Comm const world = Comm::world().duplicated();
        Group g;
        XCTAssert(g && *g == MPI_GROUP_EMPTY);

        g = world.group();
        XCTAssert(g && g.size() == 1 && g.rank() && g.rank()() == 0);
        XCTAssert(g.translate(g.rank()(), world.group())() == 0);
        XCTAssert(g.compare(world.group()) == g.identical);
        XCTAssert(g.compare(Group{}) == g.unequal);

        g = world.group() & Group{};
        XCTAssert(g && g.size() == 0 && !g.rank());
        XCTAssert(g.translate(Rank::null(), world.group())() == Rank::null());
        XCTAssert(g.compare(world.group()) == g.unequal);
        XCTAssert(g.compare(Group{}) == g.identical);

        g = world.group() | Group{};
        XCTAssert(g && g.size() == 1 && g.rank() && g.rank()() == 0);
        XCTAssert(g.translate(g.rank()(), world.group())() == 0);
        XCTAssert(g.compare(world.group()) == g.identical);
        XCTAssert(g.compare(Group{}) == g.unequal);

        g = world.group() - Group{};
        XCTAssert(g && g.size() == 1 && g.rank() && g.rank()() == 0);
        XCTAssert(g.translate(g.rank()(), world.group())() == 0);
        XCTAssert(g.compare(world.group()) == g.identical);
        XCTAssert(g.compare(Group{}) == g.unequal);

        g = world.group().included<1>({world.rank()});
        XCTAssert(g && g.size() == 1 && g.rank() && g.rank()() == 0);
        XCTAssert(g.translate(g.rank()(), world.group())() == 0);
        XCTAssert(g.compare(world.group()) == g.identical);
        XCTAssert(g.compare(Group{}) == g.unequal);

        {
            UTL::Vector<Rank, 1> ranks{world.rank()};
            g = world.group().included(ranks.begin(), ranks.end());
        }
        XCTAssert(g && g.size() == 1 && g.rank() && g.rank()() == 0);
        XCTAssert(g.translate(g.rank()(), world.group())() == 0);
        XCTAssert(g.compare(world.group()) == g.identical);
        XCTAssert(g.compare(Group{}) == g.unequal);

        g = world.group().excluded<1>({world.rank()});
        XCTAssert(g && g.size() == 0 && !g.rank());
        XCTAssert(g.translate(Rank::null(), world.group())() == Rank::null());
        XCTAssert(g.compare(world.group()) == g.unequal);
        XCTAssert(g.compare(Group{}) == g.identical);

        {
            UTL::Vector<Rank, 1> ranks{world.rank()};
            g = world.group().excluded(ranks.begin(), ranks.end());
        }
        XCTAssert(g && g.size() == 0 && !g.rank());
        XCTAssert(g.translate(Rank::null(), world.group())() == Rank::null());
        XCTAssert(g.compare(world.group()) == g.unequal);
        XCTAssert(g.compare(Group{}) == g.identical);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

@end
