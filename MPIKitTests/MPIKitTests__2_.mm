//
//  MPIKitTests__2_.mm
//  MPIKitTests
//
//  Created by KYUNGGUK MIN on 7/29/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#import "MPIKitTests__2_.h"

#include <MPIKit/MPIKit.h>

using namespace MPK::__2_;

@implementation MPIKitTests__2_

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.

    int argc = 0;
    char **argv = nullptr;
    Comm::init(&argc, &argv);
}

- (void)tearDown {
    Comm::deinit();

    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

@end
