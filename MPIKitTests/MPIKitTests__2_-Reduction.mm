//
//  MPIKitTests__2_-Reduction.m
//  MPIKitTests
//
//  Created by KYUNGGUK MIN on 8/8/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#import "MPIKitTests__2_.h"

#include <MPIKit/MPIComm__2_.h>
#include <UtilityKit/UtilityKit.h>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>

using namespace MPK::__2_;

@interface MPIKitTests__2_ (Reduction)

@end

@implementation MPIKitTests__2_ (Reduction)

- (void)testReduction_ReduceOp {
    try {
        ReduceOp op1, op2;
        XCTAssert(!op1 && !op2);

        op1 = ReduceOp::max();
        XCTAssert(op1 && MPI_MAX == *op1);
        op2 = std::move(op1);
        XCTAssert(!op1 && op2 && MPI_MAX == *op2);

        op1 = ReduceOp::max();
        XCTAssert(op1 && op1.commutative() && MPI_MAX == *op1);
        op1 = ReduceOp::min();
        XCTAssert(op1 && op1.commutative() && MPI_MIN == *op1);
        op1 = ReduceOp::plus();
        XCTAssert(op1 && op1.commutative() && MPI_SUM == *op1);
        op1 = ReduceOp::prod();
        XCTAssert(op1 && op1.commutative() && MPI_PROD == *op1);
        op1 = ReduceOp::logic_and();
        XCTAssert(op1 && op1.commutative() && MPI_LAND == *op1);
        op1 = ReduceOp::bit_and();
        XCTAssert(op1 && op1.commutative() && MPI_BAND == *op1);
        op1 = ReduceOp::logic_or();
        XCTAssert(op1 && op1.commutative() && MPI_LOR == *op1);
        op1 = ReduceOp::bit_or();
        XCTAssert(op1 && op1.commutative() && MPI_BOR == *op1);
        op1 = ReduceOp::logic_xor();
        XCTAssert(op1 && op1.commutative() && MPI_LXOR == *op1);
        op1 = ReduceOp::bit_xor();
        XCTAssert(op1 && op1.commutative() && MPI_BXOR == *op1);
        op1 = ReduceOp::loc_min();
        XCTAssert(op1 && op1.commutative() && MPI_MINLOC == *op1);
        op1 = ReduceOp::loc_max();
        XCTAssert(op1 && op1.commutative() && MPI_MAXLOC == *op1);

        op1 = ReduceOp(false, [](void *, void *, int *, MPI_Datatype *)->void {});
        XCTAssert(op1 && !op1.commutative());
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testReduction_LowLevel {
    // barrier
    try {
        Comm::world().barrier();
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
    constexpr Rank root{0};

    // reduce local
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        UTL::DynamicArray<T> v1(100), v2(v1), v3(v1);
        for (auto &v : v1) {
            v.at(0) = arc4random();
            v.at(1) = arc4random();
            v.at(2) = arc4random();
        }
        for (auto &v : v2) {
            v.at(0) = arc4random();
            v.at(1) = arc4random();
            v.at(2) = arc4random();
        }
        std::transform(v1.begin(), v1.end(), v2.begin(), v3.begin(), [](T const &a, T const &b) { return a + b; });

        ReduceOp const op(true, [](void *_a, void *_b, int *n, MPI_Datatype *) {
            T const *a = static_cast<T const*>(_a);
            T *b = static_cast<T*>(_b);
            for (int i = 0; i < *n; ++i) {
                b[i] += a[i];
            }
        });
        MPI_Reduce_local(v1.data(), v2.data(), static_cast<int>(v1.size()), *make_type<T>(), *op);
        XCTAssert(std::equal(v3.begin(), v3.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // reduce
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        UTL::DynamicArray<T> v1(100), v2(v1);
        for (auto &v : v1) {
            v.at(0) = arc4random();
            v.at(1) = arc4random();
            v.at(2) = arc4random();
        }

        ReduceOp const op(true, [](void *_a, void *_b, int *n, MPI_Datatype *) {
            T const *a = static_cast<T const*>(_a);
            T *b = static_cast<T*>(_b);
            for (int i = 0; i < *n; ++i) {
                b[i] += a[i];
            }
            MPI_Abort(MPI_COMM_WORLD, -1);
        });

        comm.reduce(op, v1.data(), v2.data(), v1.size(), root);
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));

        if (root == comm.rank()) {
            comm.reduce(op, nullptr, v2.data(), v2.size(), root);
        } else {
            comm.reduce(op, v2.data(), nullptr, v2.size(), root);
        }
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));

        std::fill(v2.begin(), v2.end(), T{});
        comm.reduce(op, v1.data(), v2.data(), v1.size());
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));

        v2 = v1;
        comm.reduce(op, v2.data(), v1.size());
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testReduction_HighLevel {
    // barrier
    try {
        Comm::world().barrier();
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // scalar
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        T const v1{arc4random(), arc4random(), arc4random()};

        ReduceOp const op(true, [](void *_a, void *_b, int *n, MPI_Datatype *) {
            T const *a = static_cast<T const*>(_a);
            T *b = static_cast<T*>(_b);
            for (int i = 0; i < *n; ++i) {
                b[i] += a[i];
            }
            MPI_Abort(MPI_COMM_WORLD, -1);
        });

        T v2;
        comm.reduce(op, v2 = v1);
        XCTAssert(UTL::reduce_bit_and(v1 == v2));
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // padded array
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        UTL::PaddedArray<T, 3> v1(100), v2(v1);
        for (auto first = v1.pad_begin(), last = v1.pad_end(); first != last; ++first) {
            first->at(0) = arc4random();
            first->at(1) = arc4random();
            first->at(2) = arc4random();
        }

        ReduceOp const op(true, [](void *_a, void *_b, int *n, MPI_Datatype *) {
            T const *a = static_cast<T const*>(_a);
            T *b = static_cast<T*>(_b);
            for (int i = 0; i < *n; ++i) {
                b[i] += a[i];
            }
            MPI_Abort(MPI_COMM_WORLD, -1);
        });

        comm.reduce(op, v2 = v1);
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // ND array
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        constexpr long ND = 3, Pad = 2;
        using ArrayND = UTL::ArrayND<T, ND, Pad>;
        ArrayND a1({3, 5, 9}), _a2(a1.dims() + 10L);
        ArrayND::size_vector_type const loc{1}, len = a1.dims();
        auto s2 = UTL::make_slice<ArrayND::pad_size()>(_a2, loc, len);

        for (auto &v : a1.flat_array()) {
            v.at(0) = arc4random();
            v.at(1) = arc4random();
            v.at(2) = arc4random();
        }

        ReduceOp const op(true, [](void *_a, void *_b, int *n, MPI_Datatype *) {
            T const *a = static_cast<T const*>(_a);
            T *b = static_cast<T*>(_b);
            for (int i = 0; i < *n; ++i) {
                b[i] += a[i];
            }
            MPI_Abort(MPI_COMM_WORLD, -1);
        });

        {
            comm.reduce(op, s2 = a1);
            XCTAssert(std::equal(a1.leaf_pad_begin(), a1.leaf_pad_end(), s2.leaf_pad_begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
        a1.fill(T{});
        {
            comm.reduce(op, a1 = s2);
            XCTAssert(std::equal(a1.leaf_pad_begin(), a1.leaf_pad_end(), s2.leaf_pad_begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // dynamic array
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        UTL::DynamicArray<T> v1(100), v2;
        for (auto &v : v1) {
            v.at(0) = arc4random();
            v.at(1) = arc4random();
            v.at(2) = arc4random();
        }

        ReduceOp const op(true, [](void *_a, void *_b, int *n, MPI_Datatype *) {
            T const *a = static_cast<T const*>(_a);
            T *b = static_cast<T*>(_b);
            for (int i = 0; i < *n; ++i) {
                b[i] += a[i];
            }
            MPI_Abort(MPI_COMM_WORLD, -1);
        });

        {
            comm.reduce(op, v2 = v1);
            XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
        v1.clear();
        {
            comm.reduce(op, v2 = v1);
            XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // static array
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        UTL::StaticArray<T, 100> v1(100), v2;
        for (auto &v : v1) {
            v.at(0) = arc4random();
            v.at(1) = arc4random();
            v.at(2) = arc4random();
        }

        ReduceOp const op(true, [](void *_a, void *_b, int *n, MPI_Datatype *) {
            T const *a = static_cast<T const*>(_a);
            T *b = static_cast<T*>(_b);
            for (int i = 0; i < *n; ++i) {
                b[i] += a[i];
            }
            MPI_Abort(MPI_COMM_WORLD, -1);
        });

        {
            comm.reduce(op, v2 = v1);
            XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
        v1.clear();
        {
            comm.reduce(op, v2 = v1);
            XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // std::vector
    try {
        Comm const comm = Comm::world().duplicated();

        using T = UTL::SIMDVector<double, 3>;
        std::vector<T> v1(100), v2;
        for (auto &v : v1) {
            v.at(0) = arc4random();
            v.at(1) = arc4random();
            v.at(2) = arc4random();
        }

        ReduceOp const op(true, [](void *_a, void *_b, int *n, MPI_Datatype *) {
            T const *a = static_cast<T const*>(_a);
            T *b = static_cast<T*>(_b);
            for (int i = 0; i < *n; ++i) {
                b[i] += a[i];
            }
            MPI_Abort(MPI_COMM_WORLD, -1);
        });

        {
            comm.reduce(op, v2 = v1);
            XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
        v1.clear();
        {
            comm.reduce(op, v2 = v1);
            XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }));
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testReduction_LeakCheck {
    try {
        constexpr long n = 30000;
        for (long i = 0; i < n; ++i) {
            @autoreleasepool {
                [self testReduction_ReduceOp];
                [self testReduction_LowLevel];
                [self testReduction_HighLevel];
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

@end
