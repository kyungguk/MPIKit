//
//  MPIKitTests__2_-Type.m
//  MPIKitTests
//
//  Created by KYUNGGUK MIN on 7/29/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#import "MPIKitTests__2_.h"

#include <MPIKit/MPIType__2_.h>
#include <MPIKit/MPITypeMap__2_.h>
#include <UtilityKit/UtilityKit.h>
#include <iostream>
#include <sstream>
#include <fstream>

using namespace MPK::__2_;

@interface MPIKitTests__2_ (Type)

@end

@implementation MPIKitTests__2_ (Type)

- (void)testType_Native {
    std::string (*to_str)(UTL::Optional<long> const&) = [](UTL::Optional<long> const &opt)->std::string {
        if (opt) {
            return std::to_string(*opt);
        } else {
            return "(nil)";
        }
    };

    try {
        Type t0, t1;
        XCTAssert(!t0 && t0.alignment() == 0);

        t0 = Type::native<char>();
        XCTAssert(t0 && MPI_CHAR == *t0 && t0.alignment() == 1);

        t1 = t0;
        XCTAssert(t0 && t1 && MPI_CHAR == *t1 && t1.alignment() == 1);

        t1 = std::move(t0);
        XCTAssert(!t0 && t0.alignment() == 0 && t1 && MPI_CHAR == *t1 && t1.alignment() == 1);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    try {
        Type t;
        UTL::Optional<long> sig_size, lb, extent;

        {
            using T = char;
            t = Type::native<T>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), @"true extent = %s", to_str(extent).c_str());
        }
        {
            using T = unsigned char;
            t = Type::native<T>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), @"true extent = %s", to_str(extent).c_str());
        }

        {
            using T = short;
            t = Type::native<T>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), @"true extent = %s", to_str(extent).c_str());
        }
        {
            using T = unsigned short;
            t = Type::native<T>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), @"true extent = %s", to_str(extent).c_str());
        }

        {
            using T = int;
            t = Type::native<T>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), @"true extent = %s", to_str(extent).c_str());
        }
        {
            using T = unsigned int;
            t = Type::native<T>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), @"true extent = %s", to_str(extent).c_str());
        }

        {
            using T = long;
            t = Type::native<T>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), @"true extent = %s", to_str(extent).c_str());
        }
        {
            using T = unsigned long;
            t = Type::native<T>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), @"true extent = %s", to_str(extent).c_str());
        }

        {
            using T = float;
            t = Type::native<T>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), @"true extent = %s", to_str(extent).c_str());
        }
        {
            using T = double;
            t = Type::native<T>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), @"true extent = %s", to_str(extent).c_str());
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testType_Derived {
    std::string (*to_str)(UTL::Optional<long> const&) = [](UTL::Optional<long> const &opt)->std::string {
        if (opt) {
            return std::to_string(*opt);
        } else {
            return "(nil)";
        }
    };

    try {
        UTL::Optional<long> sig_size, lb, extent;
        Type t;

        // vector
        //
        {
            using T = long;
            t = Type::native<T>().vector<1, 2, 2>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T)*2, @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T)*2, @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T)*2, @"true extent = %s", to_str(extent).c_str());
        }

        // indexed
        //
        {
            using T = long;
            t = Type::native<T>().indexed<2>({1, 1}, {0, 2});
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T)*2, @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T)*3, @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T)*3, @"true extent = %s", to_str(extent).c_str());
        }

        // indexed_block
        //
        {
            using T = long;
            t = Type::native<T>().indexed<2, 1>({0, 2});
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T)*2, @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T)*3, @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T)*3, @"true extent = %s", to_str(extent).c_str());
        }

        // subarray
        //
        {
            using T = long;
            constexpr long ND = 2;
            UTL::Vector<long, ND> const dims{10, 15}, locs{dims - 1L}, lens{1};
            t = Type::native<T>().subarray(dims, locs, lens);
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == long(sizeof(T))*UTL::reduce_prod(lens), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == long(sizeof(T))*UTL::reduce_prod(dims), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == long(sizeof(long))*UTL::reduce_plus(locs*dims.rest().append(1)), @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == long(sizeof(T))*UTL::reduce_prod(lens), @"true extent = %s", to_str(extent).c_str());
        }

        // resized
        //
        {
            using T = long;
            constexpr long Alignment = alignof(T)*2, new_ext = sizeof(T)*2;
            t = Type::native<T>().realigned<Alignment>();
            XCTAssert(t && t.alignment() == Alignment);

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == new_ext, @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), @"true extent = %s", to_str(extent).c_str());
        }

        // struct/composition
        //
        {
            using T1 = char;
            using T2 = int;
            using T3 = char;
            using T = std::tuple<T1, T2, T3>;
            t = {Type::native<T1>(), Type{}, Type::native<T2>(), Type::native<T3>()};
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T1) + sizeof(T2) + sizeof(T3), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(std::tuple<T1, T2>) + sizeof(T3), @"true extent = %s", to_str(extent).c_str());
        }
        {
            using T1 = long;
            using T2 = char;
            using T3 = short;
            using T = std::tuple<T1, T2, T3>;
            t = {Type::native<T1>(), Type::native<T2>(), Type::native<T3>()};
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T1) + sizeof(T2) + sizeof(T3), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T1) + sizeof(std::tuple<T2, T3>), @"true extent = %s", to_str(extent).c_str());
        }
        {
            using T1 = short;
            using T2 = char;
            using T3 = long;
            using T4 = int;
            using T = std::tuple<T1, T2, std::tuple<T3, T4>>;
            t = {Type::native<T3>(), Type::native<T4>()};
            t = {Type::native<T1>(), Type::native<T2>(), std::move(t)};
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T1) + sizeof(T2) + sizeof(T3) + sizeof(T4), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(std::tuple<T1, T2, T3>) + sizeof(T4), @"true extent = %s", to_str(extent).c_str());
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testTypeMap_Native {
    std::string (*to_str)(UTL::Optional<long> const&) = [](UTL::Optional<long> const &opt)->std::string {
        if (opt) {
            return std::to_string(*opt);
        } else {
            return "(nil)";
        }
    };

    try {
        Type t;
        UTL::Optional<long> sig_size, lb, extent;

        {
            using T = char;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), @"true extent = %s", to_str(extent).c_str());
        }
        {
            using T = unsigned char;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), @"true extent = %s", to_str(extent).c_str());
        }

        {
            using T = short;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), @"true extent = %s", to_str(extent).c_str());
        }
        {
            using T = unsigned short;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), @"true extent = %s", to_str(extent).c_str());
        }

        {
            using T = int;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), @"true extent = %s", to_str(extent).c_str());
        }
        {
            using T = unsigned int;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), @"true extent = %s", to_str(extent).c_str());
        }

        {
            using T = long;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), @"true extent = %s", to_str(extent).c_str());
        }
        {
            using T = unsigned long;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), @"true extent = %s", to_str(extent).c_str());
        }

        {
            using T = float;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), @"true extent = %s", to_str(extent).c_str());
        }
        {
            using T = double;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), @"true extent = %s", to_str(extent).c_str());
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testTypeMap_Derived {
    std::string (*to_str)(UTL::Optional<long> const&) = [](UTL::Optional<long> const &opt)->std::string {
        if (opt) {
            return std::to_string(*opt);
        } else {
            return "(nil)";
        }
    };

    try {
        Type t;
        UTL::Optional<long> sig_size, lb, extent;

        {
            using T = double[3];
            T a;
            t = make_type(a);
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), @"true extent = %s", to_str(extent).c_str());
        }

        {
            using T = UTL::Vector<double, 3>;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), @"true extent = %s", to_str(extent).c_str());
        }

        {
            using T = UTL::SIMDVector<float, 3>;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T::value_type)*T::size(), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T::value_type)*T::max_size(), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T::value_type)*T::size(), @"true extent = %s", to_str(extent).c_str());
        }

        {
            using T = UTL::SIMDVector<long, 8>;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), @"true extent = %s", to_str(extent).c_str());
        }

        {
            using T = UTL::SIMDComplex<float>;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), @"true extent = %s", to_str(extent).c_str());
        }

        {
            using T = std::complex<double>;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), @"true extent = %s", to_str(extent).c_str());
        }

        {
            using T = std::array<long, 5>;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), @"true extent = %s", to_str(extent).c_str());
        }

        {
            using T1 = char;
            using T2 = int;
            using T3 = char;
            using T = std::tuple<T1, T2, T3>;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T1) + sizeof(T2) + sizeof(T3), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(std::tuple<T1, T2>) + sizeof(T3), @"true extent = %s", to_str(extent).c_str());
        }

        {
            using T1 = char;
            using T2 = UTL::Vector<short, 2>;
            using T3 = std::tuple<long, T2>;
            using T = std::tuple<T1, T2, T3>;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T1) + sizeof(T2) + sizeof(std::tuple_element<0, T3>::type) + sizeof(std::tuple_element<1, T3>::type), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"extent = %s", to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(std::tuple<T1, T2, std::tuple_element<0, T3>::type>) + sizeof(std::tuple_element<1, T3>::type), @"true extent = %s", to_str(extent).c_str());
        }

        {
            using T1 = char;
            using T2 = short;
            using T3 = UTL::SIMDVector<double, 2>;
            using T4 = std::pair<T1, T2>;
            using T = std::tuple<T4, T3, T1>;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T1)*2 + sizeof(T2) + sizeof(T3), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"sizeof(T) = %ld, extent = %s", sizeof(T), to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(std::tuple<T4, T3>) + sizeof(T1), @"true extent = %s", to_str(extent).c_str());
        }

        {
            using T1 = long;
            using T2 = short;
            using T3 = UTL::SIMDVector<T1, 2>;
            struct T4 { T3 a; T2 b; };
            struct T : T4 { T1 c; };
            t = make_type(std::tuple<T3, T2, T1>{});
            XCTAssert(t && t.alignment() == alignof(T));

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T3) + sizeof(T2) + sizeof(T1), @"sig_size = %s", to_str(sig_size).c_str());

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, @"lb = %s", to_str(lb).c_str());
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), @"sizeof(T) = %ld, extent = %s", sizeof(T), to_str(extent).c_str());

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, @"true lb = %s", to_str(lb).c_str());
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T3) + 2*sizeof(T1), @"true extent = %s", to_str(extent).c_str());
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testTypeMap_LeakCheck {
    try {
        constexpr long n = 20000;
        for (long i = 0; i < n; ++i) {
            @autoreleasepool {
                [self testType_Native];
                [self testType_Derived];
                [self testTypeMap_Native];
                [self testTypeMap_Derived];
            }
        }
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

@end
