//
//  MPIKitTests.m
//  MPIKitTests
//
//  Created by KYUNGGUK MIN on 11/12/17.
//  Copyright © 2017 kyungguk.com. All rights reserved.
//

#import <XCTest/XCTest.h>

#define MPIKIT_INLINE_VERSION 0
#include <MPIKit/MPIKit.h>
#include <UtilityKit/UtilityKit.h>
#include <iostream>
#include <sstream>
#include <string>

@interface MPIKitTests : XCTestCase

@end

namespace MPK { using namespace __1_; }

@implementation MPIKitTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.

    int argc = 0;
    char **argv = nullptr;
    MPK::Communicator::init(&argc, &argv);
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];

    MPK::Communicator::deinit();
}

- (void)testTypeMap {
    XCTAssert(MPI_CHAR == MPK::TypeMap<char>::value);
    XCTAssert(MPI_UNSIGNED_CHAR == MPK::TypeMap<unsigned char>::value);
    XCTAssert(MPI_SHORT == MPK::TypeMap<short>::value);
    XCTAssert(MPI_UNSIGNED_SHORT == MPK::TypeMap<unsigned short>::value);
    XCTAssert(MPI_INT == MPK::TypeMap<int>::value);
    XCTAssert(MPI_UNSIGNED == MPK::TypeMap<unsigned int>::value);
    XCTAssert(MPI_LONG == MPK::TypeMap<long>::value);
    XCTAssert(MPI_UNSIGNED_LONG == MPK::TypeMap<unsigned long>::value);
    XCTAssert(MPI_FLOAT == MPK::TypeMap<float>::value);
    XCTAssert(MPI_DOUBLE == MPK::TypeMap<double>::value);
}

- (void)testEnvelop {
    try {
        constexpr MPK::Envelope env{1, 2};
        XCTAssert(1 == env.rank() && 2 == env.tag());
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testCommunicator {
    try {
        int size, rank;
        MPK::Communicator world = MPK::Communicator::world();
        size = world.size();
        rank = world.rank();
        XCTAssert(1 == size && 0 == rank && 0 == MPK::Communicator::self().rank());
        world.barrier();

        MPK::Communicator comm;
        comm = world.split(0, world.rank());
        XCTAssert(comm.size() == size && comm.rank() == rank);

        comm = std::move(world);
        XCTAssert(comm.size() == size && comm.rank() == rank);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testReduction {
    try {
        MPK::Communicator world = MPK::Communicator::world();
        MPK::Communicator comm{world.split(0, world.rank())};
        long const l = 1;
        XCTAssert(l == comm.min(l));
        XCTAssert(l == comm.max(l));
        XCTAssert(l == comm.plus(l));
        XCTAssert(l == comm.prod(l));
        double const x = 3.;
        XCTAssert(x == comm.mean(x));

        UTL::PaddedArray<int, 2> pa_s(2, 1);
        comm.plus(pa_s);
        for (long i = -pa_s.pad_size(); i < pa_s.size() + pa_s.pad_size(); ++i) {
            XCTAssert(1 == pa_s[i]);
        }

        UTL::ArrayND<UTL::Vector<double, 1>, 3, 1> M_v({2, 3, 4});
        M_v.fill(x);
        comm.plus(M_v);
        XCTAssert(x == M_v[0][0][0].x);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

struct Pair {
    UTL::Vector<double, 3> first;
    UTL::Vector<double, 2> second;
};
- (void)testSendRecvReplace {
    try {
        MPK::Communicator comm = MPK::Communicator::world();
        constexpr MPK::Envelope env{0, 1};

        double x = 1.49898e-3;
        comm.send_recv(x, env, env);
        XCTAssert(1.49898e-3 == x, @"%f", x);

        UTL::Vector<double, 10> v{x};
        comm.send_recv(v, env, env);
        for (long i = 0; i < v.size(); ++i) {
            XCTAssert(x == v[i]);
        }

        UTL::PaddedArray<int, 2> pa_s(2, 10);
        comm.send_recv(pa_s, env, env);
        for (long i = -pa_s.pad_size(); i < pa_s.size() + pa_s.pad_size(); ++i) {
            XCTAssert(10 == pa_s[i]);
        }
        UTL::PaddedArray<UTL::Vector<double, 3>, 2> pa_v(20, UTL::Vector<double, 3>{x});
        comm.send_recv(pa_v, env, env);
        for (long i = -pa_v.pad_size(); i < pa_v.size() + pa_v.pad_size(); ++i) {
            XCTAssert(x == pa_v[i].x);
        }

        UTL::ArrayND<UTL::Vector<double, 1>, 3, 1> M_v({2, 3, 4});
        M_v.fill(x);
        comm.send_recv(M_v, env, env);
        XCTAssert(x == M_v[0][0][0].x);

        UTL::Array<double> a{nullptr, nullptr};
        comm.send_recv(a, env, env);

        UTL::DynamicArray<double> da{1, 2, 3, 4, 5};
        comm.send_recv(da, env, env);
        for (long i = 0; i < da.size(); ++i) {
            XCTAssert(da[i] == i + 1);
        }

        UTL::StaticArray<UTL::Vector<double, 1>, 5> sa{{1}, {2}, {3}, {4}, {5}};
        comm.send_recv(sa, env, env);
        for (long i = 0; i < sa.size(); ++i) {
            XCTAssert(sa[i].x == i + 1);
        }

        UTL::DynamicArray<Pair> c(1);
        c.front().first.fill(1);
        c.front().second.fill(10);
        UTL::DynamicArray<UTL::Vector<double, 5>> &d = reinterpret_cast<decltype(d)>(c);
        comm.send_recv(d, env, env);
        XCTAssert(c.front().first.x == 1 && c.front().first.y == 1 && c.front().first.z == 1);
        XCTAssert(c.front().second.x == 10 && c.front().second.y == 10);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

- (void)testISSendRecv {
    try {
        MPK::Communicator comm = MPK::Communicator::world();
        constexpr MPK::Envelope env{0, 1};
        MPK::Request req;

        UTL::Array<double> a{nullptr, nullptr};
        req = comm.issend(a, env);
        comm.recv(a, env);
        req.wait();

        double x = 9.383847;
        req = comm.issend(x, env);
        double y = 0;
        comm.recv(y, env);
        req.wait();
        XCTAssert(x == y);

        UTL::Vector<float, 3> v1{4.495f, 3.f, 5.f};
        req = comm.issend(v1, env);
        decltype(v1) v2{0.f};
        comm.recv(v2, env);
        req.wait();
        for (long i = 0; i < v1.size(); ++i) {
            XCTAssert(v1[i] == v2[i]);
        }

        UTL::PaddedArray<char, 1> pa1(10, '\0'), pa2(pa1.size());
        for (char i = 0; i < 10; ++i) {
            pa1[i] = 'a' + i;
        }
        req = comm.issend(pa1, env);
        comm.recv(pa2, env);
        req.wait();
        for (long i = 0; i < pa2.size(); ++i) {
            XCTAssert(pa1[i] == pa2[i]);
        }
        printo(std::cout, pa2.begin(), "\n");
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
        return;
    }
}

@end
