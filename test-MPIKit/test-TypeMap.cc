//
//  test-TypeMap.cc
//  test-MPIKit
//
//  Created by KYUNGGUK MIN on 8/8/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "test-main.h"

using namespace MPK::__2_;

static void testTypeMap_Native(Comm const &world) {
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        Type t;
        UTL::Optional<long> sig_size, lb, extent;

        {
            using T = char;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), "true extent = ", extent);
        }
        {
            using T = unsigned char;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), "true extent = ", extent);
        }

        {
            using T = short;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), "true extent = ", extent);
        }
        {
            using T = unsigned short;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), "true extent = ", extent);
        }

        {
            using T = int;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), "true extent = ", extent);
        }
        {
            using T = unsigned int;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), "true extent = ", extent);
        }

        {
            using T = long;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), "true extent = ", extent);
        }
        {
            using T = unsigned long;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), "true extent = ", extent);
        }

        {
            using T = float;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), "true extent = ", extent);
        }
        {
            using T = double;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), "true extent = ", extent);
        }
    } catch (std::exception &e) {
        XCTAssert(false, "", e.what());
    }
}

static void testTypeMap_Derived(Comm const &world) {
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        Type t;
        UTL::Optional<long> sig_size, lb, extent;

        {
            using T = double[3];
            T a;
            t = make_type(a);
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), "true extent = ", extent);
        }

        {
            using T = UTL::Vector<double, 3>;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), "true extent = ", extent);
        }

        {
            using T = UTL::SIMDVector<float, 3>;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T::value_type)*T::size(), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T::value_type)*T::max_size(), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T::value_type)*T::size(), "true extent = ", extent);
        }

        {
            using T = UTL::SIMDVector<long, 8>;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), "true extent = ", extent);
        }

        {
            using T = UTL::SIMDComplex<float>;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), "true extent = ", extent);
        }

        {
            using T = std::complex<double>;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), "true extent = ", extent);
        }

        {
            using T = std::array<long, 5>;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), "true extent = ", extent);
        }

        {
            using T1 = char;
            using T2 = int;
            using T3 = char;
            using T = std::tuple<T1, T2, T3>;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T1) + sizeof(T2) + sizeof(T3), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(std::tuple<T1, T2>) + sizeof(T3), "true extent = ", extent);
        }

        {
            using T1 = char;
            using T2 = UTL::Vector<short, 2>;
            using T3 = std::tuple<long, T2>;
            using T = std::tuple<T1, T2, T3>;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T1) + sizeof(T2) + sizeof(std::tuple_element<0, T3>::type) + sizeof(std::tuple_element<1, T3>::type), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(std::tuple<T1, T2, std::tuple_element<0, T3>::type>) + sizeof(std::tuple_element<1, T3>::type), "true extent = ", extent);
        }

        {
            using T1 = char;
            using T2 = short;
            using T3 = UTL::SIMDVector<double, 2>;
            using T4 = std::pair<T1, T2>;
            using T = std::tuple<T4, T3, T1>;
            t = make_type<T>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T1)*2 + sizeof(T2) + sizeof(T3), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "sizeof(T) = ", sizeof(T), ", extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(std::tuple<T4, T3>) + sizeof(T1), "true extent = ", extent);
        }

        {
            using T1 = long;
            using T2 = short;
            using T3 = UTL::SIMDVector<T1, 2>;
            struct T4 { T3 a; T2 b; };
            struct T : T4 { T1 c; };
            t = make_type(std::tuple<T3, T2, T1>{});
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T3) + sizeof(T2) + sizeof(T1), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "sizeof(T) = ", sizeof(T), ", extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T3) + 2*sizeof(T1), "true extent = ", extent);
        }
    } catch (std::exception &e) {
        XCTAssert(false, "", e.what());
    }
}

void testTypeMap(Comm const &world) {
    world.barrier();

    testTypeMap_Native(world);
    testTypeMap_Derived(world);
}
