//
//  test-Comm.cc
//  test-MPIKit
//
//  Created by KYUNGGUK MIN on 8/8/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "test-main.h"
#include <string>

using namespace MPK::__2_;

static void testComm_Comm(Comm const &world) {
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        Comm c;
        XCTAssert(!c, "");

        c = Comm::self();
        XCTAssert(c && MPI_COMM_SELF == *c && c.compare(Comm::self()) == c.identical && std::string("MPI_COMM_SELF") == c.label(), "");

        {
            Comm c1 = Comm::world();
            c = std::move(c1);
        }
        XCTAssert(c && MPI_COMM_WORLD == *c && c.compare(world) == c.identical && std::string("MPI_COMM_WORLD") == c.label(), "");

        c = world.duplicated();
        std::string label = "duplicated WORLD";
        c.set_label(label.c_str());
        XCTAssert(c && c.compare(world) == c.congruent && label == c.label(), "");
        XCTAssert(c.size() == world.size() && c.rank() == world.rank(), "size = ", c.size(), ", rank = ", *c.rank());

        c = world.split(world.rank());
        label = "split comm";
        c.set_label(label.c_str());
        XCTAssert(c && c.compare(Comm::self()) == c.congruent && label == c.label(), "");
        XCTAssert(c.size() == 1 && c.rank() == 0, "size = ", c.size(), ", rank = ", *c.rank());

        int const color = world.rank() % 2, key = world.size() - world.rank();
        int const size = world.size()/2 + (!color && world.size() % 2 ? 1 : 0);
        c = world.split(color, key);
        auto const translated_rank = c.group().translate(c.rank(), world);
        XCTAssert(c.size() == size && translated_rank && *translated_rank == world.rank(), "size = ", c.size(), ", rank = ", *c.rank());
        Rank prev = c.group().translate(Rank{0}, world)();
        for (int i = 1; i < c.size(); ++i) {
            Rank const cur = c.group().translate(Rank{i}, world)();
            XCTAssert(prev > cur, "i = ", i, ", prev_rank = ", *prev, ", cur_rank = ", *cur);
            prev = cur;
        }

        if (master == world.rank()) {
            c = world.split(master);
            auto const rank = c.group().translate(c.rank(), world);
            XCTAssert(c && rank && master == *rank, "");
        } else {
            c = world.split(MPI_UNDEFINED);
            XCTAssert(!c, "");
        }

        c = world.created(Group{});
        XCTAssert(!c, "");

        c = world.created(world.group().included<1>({master}));
        if (master == world.rank()) {
            auto const rank = c.group().translate(c.rank(), world);
            XCTAssert(c && rank && master == *rank, "");
        } else {
            XCTAssert(!c, "");
        }

        c = world.created(world.group().excluded<1>({master}));
        if (master != world.rank()) {
            auto const rank = c.group().translate(c.rank(), world);
            XCTAssert(c && rank && master != *rank, "");
        } else {
            XCTAssert(!c, "");
        }
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }
}

static void testComm_Group(Comm const &world) {
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        Group g;
        XCTAssert(g && *g == MPI_GROUP_EMPTY, "");

        g = world.group();
        XCTAssert(g && g.size() == world.size() && g.rank()() == world.rank(), "");
        XCTAssert(g.translate(g.rank()(), world)() == world.rank(), "");
        XCTAssert(g.compare(world) == g.identical, "");
        XCTAssert(g.compare(Group{}) == g.unequal, "");

        g = world.group() & Group{};
        XCTAssert(g && g.size() == Group{}.size() && !g.rank(), "");
        XCTAssert(g.translate(Rank::null(), world)() == Rank::null(), "");
        XCTAssert(g.compare(world) == g.unequal, "");
        XCTAssert(g.compare(Group{}) == g.identical, "");

        g = world.group() | Group{};
        XCTAssert(g && g.size() == world.size() && g.rank()() == world.rank(), "");
        XCTAssert(g.translate(g.rank()(), world)() == world.rank(), "");
        XCTAssert(g.compare(world) == g.identical, "");
        XCTAssert(g.compare(Group{}) == g.unequal, "");

        g = world.group() - Group{};
        XCTAssert(g && g.size() == world.size() && g.rank()() == world.rank(), "");
        XCTAssert(g.translate(g.rank()(), world)() == world.rank(), "");
        XCTAssert(g.compare(world) == g.identical, "");
        XCTAssert(g.compare(Group{}) == g.unequal, "");

        g = world.group().included<1>({world.rank()});
        XCTAssert(g && g.size() == 1 && g.rank() && g.rank()() == 0, "");
        XCTAssert(g.translate(g.rank()(), world)() == world.rank(), "");

        g = world.group() - g;
        XCTAssert(g && g.size() == world.size() - 1 && !g.rank(), "");
        XCTAssert(g.translate(Rank::null(), world)() == Rank::null(), "");

        {
            UTL::Vector<Rank, 1> ranks{world.rank()};
            g = world.group().included(ranks.begin(), ranks.end());
        }
        XCTAssert(g && g.size() == 1 && g.rank() && g.rank()() == 0, "");
        XCTAssert(g.translate(g.rank()(), world)() == world.rank(), "");

        g = world.group().excluded<1>({world.rank()});
        XCTAssert(g && g.size() == world.size() - 1 && !g.rank(), "");
        XCTAssert(g.translate(Rank::null(), world)() == Rank::null(), "");

        {
            UTL::Vector<Rank, 1> ranks{world.rank()};
            g = world.group().excluded(ranks.begin(), ranks.end());
        }
        XCTAssert(g && g.size() == world.size() - 1 && !g.rank(), "");
        XCTAssert(g.translate(Rank::null(), world)() == Rank::null(), "");
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }
}

void testComm(Comm const &world) {
    world.barrier();

    testComm_Comm(world);
    testComm_Group(world);
}
