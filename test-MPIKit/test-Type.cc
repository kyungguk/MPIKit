//
//  test-Type.cc
//  test-MPIKit
//
//  Created by KYUNGGUK MIN on 8/8/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "test-main.h"

using namespace MPK::__2_;

static void testType_Native(Comm const &world) {
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        Type t0, t1;
        XCTAssert(!t0 && t0.alignment() == 0, "");

        t0 = Type::native<char>();
        XCTAssert(t0 && MPI_CHAR == *t0 && t0.alignment() == 1, "");

        t1 = t0;
        XCTAssert(t0 && t1 && MPI_CHAR == *t1 && t1.alignment() == 1, "");

        t1 = std::move(t0);
        XCTAssert(!t0 && t0.alignment() == 0 && t1 && MPI_CHAR == *t1 && t1.alignment() == 1, "");
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }

    try {
        Type t;
        UTL::Optional<long> sig_size, lb, extent;

        {
            using T = char;
            t = Type::native<T>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), "true extent = ", extent);
        }
        {
            using T = unsigned char;
            t = Type::native<T>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), "true extent = ", extent);
        }

        {
            using T = short;
            t = Type::native<T>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), "true extent = ", extent);
        }
        {
            using T = unsigned short;
            t = Type::native<T>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), "true extent = ", extent);
        }

        {
            using T = int;
            t = Type::native<T>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), "true extent = ", extent);
        }
        {
            using T = unsigned int;
            t = Type::native<T>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), "true extent = ", extent);
        }

        {
            using T = long;
            t = Type::native<T>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), "true extent = ", extent);
        }
        {
            using T = unsigned long;
            t = Type::native<T>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), "true extent = ", extent);
        }

        {
            using T = float;
            t = Type::native<T>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), "true extent = ", extent);
        }
        {
            using T = double;
            t = Type::native<T>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), "true extent = ", extent);
        }
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }
}

static void testType_Derived(Comm const &world) {
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        UTL::Optional<long> sig_size, lb, extent;
        Type t;

        // vector
        //
        {
            using T = long;
            t = Type::native<T>().vector<1, 2, 2>();
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T)*2, "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T)*2, "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T)*2, "true extent = ", extent);
        }

        // indexed
        //
        {
            using T = long;
            t = Type::native<T>().indexed<2>({1, 1}, {0, 2});
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T)*2, "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T)*3, "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T)*3, "true extent = ", extent);
        }

        // indexed_block
        //
        {
            using T = long;
            t = Type::native<T>().indexed<2, 1>({0, 2});
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T)*2, "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T)*3, "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T)*3, "true extent = ", extent);
        }

        // subarray
        //
        {
            using T = long;
            constexpr long ND = 2;
            UTL::Vector<long, ND> const dims{10, 15}, locs{dims - 1L}, lens{1};
            t = Type::native<T>().subarray(dims, locs, lens);
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == long(sizeof(T))*UTL::reduce_prod(lens), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == long(sizeof(T))*UTL::reduce_prod(dims), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == long(sizeof(long))*UTL::reduce_plus(locs*dims.rest().append(1)), "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == long(sizeof(T))*UTL::reduce_prod(lens), "true extent = ", extent);
        }

        // resized
        //
        {
            using T = long;
            constexpr long Alignment = alignof(T)*2, new_ext = sizeof(T)*2;
            t = Type::native<T>().realigned<Alignment>();
            XCTAssert(t && t.alignment() == Alignment, "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == new_ext, "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T), "true extent = ", extent);
        }

        // struct/composition
        //
        {
            using T1 = char;
            using T2 = int;
            using T3 = char;
            using T = std::tuple<T1, T2, T3>;
            t = {Type::native<T1>(), Type{}, Type::native<T2>(), Type::native<T3>()};
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T1) + sizeof(T2) + sizeof(T3), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(std::tuple<T1, T2>) + sizeof(T3), "true extent = ", extent);
        }
        {
            using T1 = long;
            using T2 = char;
            using T3 = short;
            using T = std::tuple<T1, T2, T3>;
            t = {Type::native<T1>(), Type::native<T2>(), Type::native<T3>()};
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T1) + sizeof(T2) + sizeof(T3), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(T1) + sizeof(std::tuple<T2, T3>), "true extent = ", extent);
        }
        {
            using T1 = short;
            using T2 = char;
            using T3 = long;
            using T4 = int;
            using T = std::tuple<T1, T2, std::tuple<T3, T4>>;
            t = {Type::native<T3>(), Type::native<T4>()};
            t = {Type::native<T1>(), Type::native<T2>(), std::move(t)};
            XCTAssert(t && t.alignment() == alignof(T), "");

            sig_size = t.signature_size();
            XCTAssert(sig_size && *sig_size == sizeof(T1) + sizeof(T2) + sizeof(T3) + sizeof(T4), "sig_size = ", sig_size);

            lb = t.lower_bound();
            XCTAssert(lb && *lb == 0, "lb = ", lb);
            extent = t.extent();
            XCTAssert(extent && *extent == sizeof(T), "extent = ", extent);

            lb = t.lower_bound(true);
            XCTAssert(lb && *lb == 0, "true lb = ", lb);
            extent = t.extent(true);
            XCTAssert(extent && *extent == sizeof(std::tuple<T1, T2, T3>) + sizeof(T4), "true extent = ", extent);
        }
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }
}

void testType(Comm const &world) {
    world.barrier();

    testType_Native(world);
    testType_Derived(world);
}
