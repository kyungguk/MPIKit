//
//  test-main.cc
//  test-MPIKit
//
//  Created by KYUNGGUK MIN on 8/8/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "test-main.h"
#include <exception>

using namespace MPK::__2_;

extern void testType(Comm const &world);
extern void testTypeMap(Comm const &world);
extern void testComm(Comm const &world);
extern void testP2P(Comm const &world);
extern void testCollective(Comm const &world);
extern void testReduction(Comm const &world);

int main(int argc, char * argv[]) {
    Comm::init(&argc, &argv);
    Comm const world = Comm::world();
    try {
        constexpr long n = 1;
        for (long i = 0; i < n; ++i) {
            if (master == world.rank()) println(std::cout, "%% ", __PRETTY_FUNCTION__, " - iteration = ", i);
            testType(world);
            testTypeMap(world);
            testComm(world);
            testP2P(world);
            testCollective(world);
            testReduction(world);
        }
    } catch (std::exception &e) {
        println(std::cerr, "uncaught exception : ", e.what());
        MPI_Abort(MPI_COMM_WORLD, -1);
    }
    Comm::deinit();
    return 0;
}
