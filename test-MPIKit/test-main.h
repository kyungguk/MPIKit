//
//  test-main.h
//  MPIKit
//
//  Created by KYUNGGUK MIN on 8/8/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#ifndef test_main_h
#define test_main_h

#define MPIKIT_INLINE_VERSION 2

#include <MPIKit/MPIKit.h>
#include <UtilityKit/UtilityKit.h>
#include <iostream>

constexpr MPK::Rank master{0};

#define XCTAssert(expression, ...) \
if (!(expression) ) println(std::cerr, '\t', "Line[", __LINE__, "] :: @(", #expression, ") :: ", __VA_ARGS__)

#endif /* test_main_h */
