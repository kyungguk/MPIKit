//
//  test-P2P.cc
//  test-MPIKit
//
//  Created by KYUNGGUK MIN on 8/9/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "test-main.h"
#include <algorithm>

using namespace MPK::__2_;

static void testP2P_LowLevel(Comm const &world) {
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    // persistence
    try {
        Comm const comm = world.duplicated();

        using T = std::tuple<UTL::SIMDVector<double, 3>, std::pair<char, float>>;
        UTL::PaddedArray<T, 0> v1(10), v2(v1);

        int to = comm.rank() + 1;
        if (to >= comm.size()) to = 0;
        Request send_req = comm.ssend_init(v1.data(), v1.size(), std::make_pair(Rank{to}, Tag{1}));
        int from = comm.rank() - 1;
        if (from < 0) from = comm.size() - 1;
        Request recv_req = comm.recv_init(v2.data(), v2.size(), std::make_pair(Rank{from}, Tag::any()));

        UTL::BitReversedRandomReal rng{2, 1};
        constexpr long n = 10;
        for (long i = 0; i < n; ++i) {
            for (auto &v : v1) {
                std::get<0>(v).at(0) = rng();
                std::get<0>(v).at(1) = rng();
                std::get<0>(v).at(2) = rng();
                std::get<1>(v).second = static_cast<float>(rng());
            }
            v2.fill(T{});

            send_req.start();
            recv_req.start();
            recv_req.wait();
            send_req.wait();

            XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) {
                return UTL::reduce_bit_and(std::get<0>(a) == std::get<0>(b)) && std::get<1>(a).second == std::get<1>(b).second;
            }), "");
        }
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }

    // non-blocking send/blocking recv
    try {
        Comm const comm = world.split(world.rank() / 2);
        Rank to{MPI_UNDEFINED}, &from = to;
        if (comm.size() == 1) {
            to = comm.rank();
        } else if (comm.size() == 2) {
            to = Rank{(comm.rank() + 1) % 2};
        } else {
            throw std::domain_error(std::string(__PRETTY_FUNCTION__) + " - split comm size is neither 1 or 2: comm.size() = " + std::to_string(comm.size()));
        }

        using T = UTL::SIMDVector<double, 3>;
        UTL::DynamicArray<T> v1(10), v2;
        UTL::BitReversedRandomReal rng{2, 1};
        for (auto &v : v1) {
            v.at(0) = rng();
            v.at(1) = rng();
            v.at(2) = rng();
        }

        Request req = comm.issend(v1.data(), v1.size(), {to, Tag{1}});
        {
            Type const t = make_type(T{});
            MPI_Message msg;
            v2.resize(t.get_count(comm.probe({from, Tag::any()}, &msg))());
            comm.recv(v2.data(), v2.size(), std::move(msg));
        }
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
        req.wait();
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }

    // blocking send-recv
    try {
        Comm const comm = world.split(world.rank() / 2);
        Rank to{MPI_UNDEFINED}, &from = to;
        if (comm.size() == 1) {
            to = comm.rank();
        } else if (comm.size() == 2) {
            to = Rank{(comm.rank() + 1) % 2};
        } else {
            throw std::domain_error(std::string(__PRETTY_FUNCTION__) + " - split comm size is neither 1 or 2: comm.size() = " + std::to_string(comm.size()));
        }

        using T = UTL::SIMDVector<double, 3>;
        UTL::PaddedArray<T, 0> v1(10), v2(v1);
        UTL::BitReversedRandomReal rng{2 + static_cast<unsigned>(comm.rank()), 1};
        for (auto &v : v1) {
            v.at(0) = rng();
            v.at(1) = rng();
            v.at(2) = rng();
        }

        comm.send_recv(v1.data(), v1.size(), {to, Tag{1}},
                       v2.data(), v2.size(), {from, Tag::any()});

        comm.send_recv(v2.data(), v2.size(), {to, Tag{1}}, {from, Tag::any()});

        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }
}

static void testP2P_NonBlockingSend_BlockingRecv(Comm const &world) {
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    Comm const comm = world.split(world.rank() / 2);
    Rank to{MPI_UNDEFINED}, &from = to;
    UTL::BitReversedRandomReal rng_this, rng_other;
    if (comm.size() == 1) {
        to = comm.rank();
        rng_this = UTL::BitReversedRandomReal{2, 1};
        rng_other = UTL::BitReversedRandomReal{2, 1};
    } else if (comm.size() == 2) {
        to = Rank{(comm.rank() + 1) % 2};
        if (comm.rank() == 0) {
            rng_this = UTL::BitReversedRandomReal{2, 1};
            rng_other = UTL::BitReversedRandomReal{3, 1};
        } else {
            rng_this = UTL::BitReversedRandomReal{3, 1};
            rng_other = UTL::BitReversedRandomReal{2, 1};
        }
    } else {
        XCTAssert(false, __PRETTY_FUNCTION__, " - split comm size is neither 1 or 2: comm.size() = ", comm.size());
        return;
    }
    UTL::NRRandomReal nr_rng(200 + static_cast<unsigned>(world.rank())*10);

    // scalar
    try {
        using T = UTL::SIMDVector<double, 3>;
        T v1, v2;
        for (long i = 0; i < T::size(); ++i) {
            v1.at(i) = rng_this();
            v2.at(i) = rng_other();
        }

        T const tmp{v2};
        Request req = comm.issend(tmp, {to, Tag{1}});
        comm.recv(v2, {from, Tag::any()});
        req.wait();

        XCTAssert(UTL::reduce_bit_and(v1 == v2), "");
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }

    // padded array
    try {
        using T = UTL::SIMDVector<double, 2>;
        UTL::PaddedArray<T, 3> v1(10), v2(v1);
        for (long i = -v1.pad_size(); i < v1.size() + v1.pad_size(); ++i) {
            for (long j = 0; j < T::size(); ++j) {
                v1.at(i)[j] = rng_this();
                v2.at(i)[j] = rng_other();
            }
        }
        decltype(v2) const tmp{v2};
        Request req = comm.issend(tmp, {to, Tag{1}});
        comm.recv(v2, {from, Tag::any()});
        req.wait();

        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }

    // ND array
    try {
        using T = UTL::SIMDVector<double, 3>;
        constexpr long ND = 3, Pad = 1;
        using ArrayND = UTL::ArrayND<T, ND, Pad>;
        ArrayND a1({3, 5, 9}), tmp(a1), _a2(a1.dims() + 10L);
        ArrayND::size_vector_type const loc{1}, len = a1.dims();
        auto s2 = UTL::make_slice<ArrayND::pad_size()>(_a2, loc, len);

        for (long i = 0; i < a1.flat_array().size(); ++i) {
            for (long j = 0; j < T::size(); ++j) {
                a1.flat_array().at(i)[j] = rng_this();
                tmp.flat_array().at(i)[j] = rng_other();
            }
        }
        for (T &v : _a2.flat_array()) {
            for (T::value_type &x : v) {
                x = nr_rng();
            }
        }

        {
            Request req = comm.issend(tmp, {to, Tag{1}});
            comm.recv(s2, {from, Tag::any()});
            req.wait();

            tmp = s2;
            XCTAssert(std::equal(a1.flat_array().begin(), a1.flat_array().end(), tmp.flat_array().begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
        }
        s2.fill(T{});
        {
            Request req = comm.issend(s2, {to, Tag{1}});
            comm.recv(a1, {from, Tag::any()});
            req.wait();

            tmp = s2;
            XCTAssert(std::equal(a1.flat_array().begin(), a1.flat_array().end(), tmp.flat_array().begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
        }
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }

    // ND subarray
    try {
        using T = UTL::SIMDVector<double, 3>;
        constexpr long ND = 3, Pad = 2;
        using ArrayND = UTL::ArrayND<T, ND, Pad>;
        ArrayND v1({3, 5, 9}), v2(v1.dims());
        ArrayND::size_vector_type const loc{1}, len = v1.dims() - 1L;
        auto slice_v1 = UTL::make_slice<ArrayND::pad_size()>(v1, loc, len);
        auto slice_v2 = UTL::make_slice<ArrayND::pad_size()>(v2, loc, len);

        for (long i = 0; i < v1.flat_array().size(); ++i) {
            for (long j = 0; j < T::size(); ++j) {
                v1.flat_array().at(i)[j] = rng_this();
                v2.flat_array().at(i)[j] = rng_other();
            }
        }

        Type const t = make_type<T>().subarray(v1.max_dims(), loc, len + 2*Pad); // including paddings
        {
            ArrayND const tmp{v2};
            Request req = comm.issend({tmp.data(), t}, 1, {to, Tag{1}});
            comm.recv({v2.data(), t}, 1, {from, Tag::any()});
            req.wait();

            ArrayND const tmp1{slice_v1}, tmp2{slice_v2};
            XCTAssert(std::equal(tmp1.flat_array().begin(), tmp1.flat_array().end(), tmp2.flat_array().begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
        }
        slice_v2.fill(T{});
        {
            comm.send_recv({v2.data(), t}, 1, {to, Tag{1}},
                           {v1.data(), t}, 1, {from, Tag::any()});

            ArrayND const tmp1{slice_v1}, tmp2{slice_v2};
            XCTAssert(std::equal(tmp1.flat_array().begin(), tmp1.flat_array().end(), tmp2.flat_array().begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
        }
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }

    // dynamic array
    try {
        using T = UTL::SIMDVector<double, 3>;
        UTL::DynamicArray<T> v1(10), v2(v1), tmp;
        for (long i = 0; i < v1.size(); ++i) {
            for (long j = 0; j < T::size(); ++j) {
                v1.at(i)[j] = rng_this();
                v2.at(i)[j] = rng_other();
            }
        }

        {
            Request req = comm.issend(v2, {to, Tag{1}});
            comm.recv(tmp, {from, Tag::any()});
            req.wait();

            XCTAssert(v1.size() == tmp.size() && std::equal(v1.begin(), v1.end(), tmp.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
        }
        tmp.clear();
        {
            Request req = comm.issend(v1, {to, Tag{1}});
            comm.recv(tmp, {from, Tag::any()});
            req.wait();

            XCTAssert(v2.size() == tmp.size() && std::equal(tmp.begin(), tmp.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
        }
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }

    // static array
    try {
        using T = UTL::SIMDVector<double, 3>;
        UTL::StaticArray<T, 10> v1(10), v2(v1), tmp;
        for (long i = 0; i < v1.size(); ++i) {
            for (long j = 0; j < T::size(); ++j) {
                v1.at(i)[j] = rng_this();
                v2.at(i)[j] = rng_other();
            }
        }

        {
            Request req = comm.issend(v2, {to, Tag{1}});
            comm.recv(tmp, {from, Tag::any()});
            req.wait();

            XCTAssert(v1.size() == tmp.size() && std::equal(v1.begin(), v1.end(), tmp.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
        }
        tmp.clear();
        {
            Request req = comm.issend(v1, {to, Tag{1}});
            comm.recv(tmp, {from, Tag::any()});
            req.wait();

            XCTAssert(v2.size() == tmp.size() && std::equal(tmp.begin(), tmp.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
        }
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }

    // std::vector
    try {
        using T = UTL::SIMDVector<double, 3>;
        std::vector<T> v1(10), v2(v1), tmp;
        for (unsigned i = 0; i < v1.size(); ++i) {
            for (long j = 0; j < T::size(); ++j) {
                v1.at(i)[j] = rng_this();
                v2.at(i)[j] = rng_other();
            }
        }

        {
            Request req = comm.issend(v2, {to, Tag{1}});
            comm.recv(tmp, {from, Tag::any()});
            req.wait();

            XCTAssert(v1.size() == tmp.size() && std::equal(v1.begin(), v1.end(), tmp.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
        }
        tmp.clear();
        {
            Request req = comm.issend(v1, {to, Tag{1}});
            comm.recv(tmp, {from, Tag::any()});
            req.wait();

            XCTAssert(v2.size() == tmp.size() && std::equal(tmp.begin(), tmp.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
        }
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }
}

static void testP2P_BlockingSendRecvReplace(Comm const &world) {
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    Comm const comm = world.split(world.rank() / 2);
    Rank to{MPI_UNDEFINED}, &from = to;
    UTL::BitReversedRandomReal rng_this, rng_other;
    if (comm.size() == 1) {
        to = comm.rank();
        rng_this = UTL::BitReversedRandomReal{2, 1};
        rng_other = UTL::BitReversedRandomReal{2, 1};
    } else if (comm.size() == 2) {
        to = Rank{(comm.rank() + 1) % 2};
        if (comm.rank() == 0) {
            rng_this = UTL::BitReversedRandomReal{2, 1};
            rng_other = UTL::BitReversedRandomReal{3, 1};
        } else {
            rng_this = UTL::BitReversedRandomReal{3, 1};
            rng_other = UTL::BitReversedRandomReal{2, 1};
        }
    } else {
        XCTAssert(false, __PRETTY_FUNCTION__, " - split comm size is neither 1 or 2: comm.size() = ", comm.size());
        return;
    }
    UTL::NRRandomReal nr_rng(200 + static_cast<unsigned>(world.rank())*10);

    // scalar
    try {
        using T = UTL::SIMDVector<double, 3>;
        T v1, v2;
        for (long i = 0; i < T::size(); ++i) {
            v1.at(i) = rng_this();
            v2.at(i) = rng_other();
        }

        comm.send_recv(v2, {to, Tag{1}}, {from, Tag::any()});
        XCTAssert(UTL::reduce_bit_and(v1 == v2), "");
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }

    // padded array
    try {
        using T = UTL::SIMDVector<double, 3>;
        UTL::PaddedArray<T, 3> v1(10), v2(v1);
        for (long i = -v1.pad_size(); i < v1.size() + v1.pad_size(); ++i) {
            for (long j = 0; j < T::size(); ++j) {
                v1.at(i)[j] = rng_this();
                v2.at(i)[j] = rng_other();
            }
        }

        comm.send_recv(v2, {to, Tag{1}}, {from, Tag::any()});
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }

    // ND array
    try {
        using T = UTL::SIMDVector<double, 3>;
        constexpr long ND = 3, Pad = 2;
        using ArrayND = UTL::ArrayND<T, ND, Pad>;
        ArrayND a1({3, 5, 9}), tmp(a1), _a2(a1.dims() + 10L);
        ArrayND::size_vector_type const loc{1}, len = a1.dims();
        auto s2 = UTL::make_slice<ArrayND::pad_size()>(_a2, loc, len);

        for (long i = 0; i < a1.flat_array().size(); ++i) {
            for (long j = 0; j < T::size(); ++j) {
                a1.flat_array().at(i)[j] = rng_this();
                tmp.flat_array().at(i)[j] = rng_other();
            }
        }
        for (T &v : _a2.flat_array()) {
            for (T::value_type &x : v) {
                x = nr_rng();
            }
        }

        {
            comm.send_recv(s2 = tmp, {to, Tag{1}}, {from, Tag::any()});

            tmp = s2;
            XCTAssert(std::equal(a1.flat_array().begin(), a1.flat_array().end(), tmp.flat_array().begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
        }
        tmp.fill(T{});
        {
            comm.send_recv(a1 = tmp, {to, Tag{1}}, {from, Tag::any()});

            XCTAssert(std::equal(a1.flat_array().begin(), a1.flat_array().end(), tmp.flat_array().begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
        }
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }

    // dynamic array
    try {
        using T = UTL::SIMDVector<double, 3>;
        UTL::DynamicArray<T> v1(10), v2(v1);
        for (long i = 0; i < v1.size(); ++i) {
            for (long j = 0; j < T::size(); ++j) {
                v1.at(i)[j] = rng_this();
                v2.at(i)[j] = rng_other();
            }
        }

        comm.send_recv(v2, {to, Tag{1}}, {from, Tag::any()});
        XCTAssert(v1.size() == v2.size() && std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");

        if (comm.rank() == 0) {
            std::fill(v1.begin(), v1.end(), T{});
            v2.clear();
            comm.send_recv(v1, {to, Tag{1}}, {from, Tag::any()});
        } else {
            v1.clear();
            std::fill(v2.begin(), v2.end(), T{});
            comm.send_recv(v1, {to, Tag{1}}, {from, Tag::any()});
        }
        if (comm.size() == 2) {
            XCTAssert(v1.size() == v2.size() && std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
        }
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }

    // static array
    try {
        using T = UTL::SIMDVector<double, 3>;
        UTL::StaticArray<T, 10> v1(10), v2(v1);
        for (long i = 0; i < v1.size(); ++i) {
            for (long j = 0; j < T::size(); ++j) {
                v1.at(i)[j] = rng_this();
                v2.at(i)[j] = rng_other();
            }
        }

        comm.send_recv(v2, {to, Tag{1}}, {from, Tag::any()});
        XCTAssert(v1.size() == v2.size() && std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");

        if (comm.rank() == 0) {
            std::fill(v1.begin(), v1.end(), T{});
            v2.clear();
            comm.send_recv(v1, {to, Tag{1}}, {from, Tag::any()});
        } else {
            v1.clear();
            std::fill(v2.begin(), v2.end(), T{});
            comm.send_recv(v1, {to, Tag{1}}, {from, Tag::any()});
        }
        if (comm.size() == 2) {
            XCTAssert(v1.size() == v2.size() && std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
        }
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }

    // std::vector
    try {
        using T = UTL::SIMDVector<double, 3>;
        std::vector<T> v1(10), v2(v1);
        for (unsigned i = 0; i < v1.size(); ++i) {
            for (long j = 0; j < T::size(); ++j) {
                v1.at(i)[j] = rng_this();
                v2.at(i)[j] = rng_other();
            }
        }

        comm.send_recv(v2, {to, Tag{1}}, {from, Tag::any()});
        XCTAssert(v1.size() == v2.size() && std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");

        if (comm.rank() == 0) {
            std::fill(v1.begin(), v1.end(), T{});
            v2.clear();
            comm.send_recv(v1, {to, Tag{1}}, {from, Tag::any()});
        } else {
            v1.clear();
            std::fill(v2.begin(), v2.end(), T{});
            comm.send_recv(v1, {to, Tag{1}}, {from, Tag::any()});
        }
        if (comm.size() == 2) {
            XCTAssert(v1.size() == v2.size() && std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
        }
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }
}

void testP2P(Comm const &world) {
    world.barrier();

    testP2P_LowLevel(world);
    testP2P_NonBlockingSend_BlockingRecv(world);
    testP2P_BlockingSendRecvReplace(world);
}
