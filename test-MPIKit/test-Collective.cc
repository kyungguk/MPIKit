//
//  test-Collective.cc
//  test-MPIKit
//
//  Created by KYUNGGUK MIN on 8/10/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "test-main.h"
#include <algorithm>

using namespace MPK::__2_;

static void testCollective_LowLevel(Comm const &world) {
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }
    Comm const comm = world.duplicated();
    UTL::BitReversedRandomReal rng{2, 1};

    // broadcast
    try {
        using T = UTL::SIMDVector<double, 3>;
        UTL::PaddedArray<T, 0> v1(100), v2(v1);
        for (long i = 0; i < v1.size(); ++i) {
            for (long j = 0; j < T::size(); ++j) {
                v1.at(i)[j] = rng();
            }
        }

        if (master == comm.rank()) v2 = v1;
        comm.bcast(v2.data(), v2.size(), master);
        XCTAssert(v1.size() == v2.size() && std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }

    // gather
    try {
        using T = UTL::SIMDVector<double, 3>;
        UTL::PaddedArray<T, 0> v1(comm.size()), v2(v1);
        for (long i = 0; i < v1.size(); ++i) {
            for (long j = 0; j < T::size(); ++j) {
                v1.at(i)[j] = rng();
            }
        }

        comm.gather(v1.data() + comm.rank(), 1, v2.data(), 1, Rank{comm.size() - 1});
        comm.bcast(v2.data(), v2.size(), Rank{comm.size() - 1});
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");

        v2.fill(T{});
        if (master == comm.rank()) {
            v2.at(comm.rank()) = v1.at(comm.rank());
            comm.gather(nullptr, {}, v2.data(), 1, master);
        } else {
            comm.gather(v1.data() + comm.rank(), 1, nullptr, {}, master);
        }
        comm.bcast(v2.data(), v2.size(), master);
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");

        v2.fill(T{});
        comm.gather(v1.data() + comm.rank(), 1, v2.data(), 1); // allgather
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");

        comm.gather(v2.data(), 1); // in-place allgather
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }

    // scatter
    try {
        using T = UTL::SIMDVector<double, 3>;
        UTL::PaddedArray<T, 0> v1(comm.size()), v2(v1);
        for (long i = 0; i < v1.size(); ++i) {
            for (long j = 0; j < T::size(); ++j) {
                v1.at(i)[j] = rng();
            }
        }

        comm.scatter(v1.data(), 1, v2.data(), 1, master);
        XCTAssert(UTL::reduce_bit_and(v1.at(comm.rank()) == v2.front()), "");

        if (master == comm.rank()) {
            comm.scatter(v1.data(), 1, nullptr, {}, master);
        } else {
            v2.fill(T{});
            comm.scatter(nullptr, {}, v2.data(), 1, master);
        }
        XCTAssert(UTL::reduce_bit_and(v1.at(comm.rank()) == v2.front()), "");

        std::fill(v2.begin(), v2.end(), T{});
        comm.all2all(v1.data(), 1, v2.data(), 1);
        decltype(v1) const tmp(v1.size(), v1.at(comm.rank()));
        XCTAssert(std::equal(tmp.begin(), tmp.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");

        comm.all2all(v2.data(), 1);
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }
}

static void testCollective_HighLevel(Comm const &world) {
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }
    Comm const comm = world.duplicated();
    UTL::BitReversedRandomReal rng{2, 1};

    // bcast scalar
    try {
        using T = UTL::SIMDVector<double, 3>;
        T v1{rng(), rng(), rng()}, v2;

        if (master == comm.rank()) v2 = v1;
        comm.bcast(v2, master);
        XCTAssert(UTL::reduce_bit_and(v1 == v2), "");
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }

    // bcast padded array
    try {
        using T = UTL::SIMDVector<double, 3>;
        UTL::PaddedArray<T, 1> v1(10), v2(v1);
        for (auto first = v1.pad_begin(), last = v1.pad_end(); first != last; ++first) {
            first->at(0) = rng();
            first->at(1) = rng();
            first->at(2) = rng();
        }

        if (master == comm.rank()) v2 = v1;
        comm.bcast(v2, master);
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }

    // bcast ND array
    try {
        using T = UTL::SIMDVector<double, 3>;
        constexpr long ND = 3, Pad = 2;
        using ArrayND = UTL::ArrayND<T, ND, Pad>;
        ArrayND a1({3, 5, 9}), _a2(a1.dims() + 10L);
        ArrayND::size_vector_type const loc{1}, len = a1.dims();
        auto s2 = UTL::make_slice<ArrayND::pad_size()>(_a2, loc, len);

        for (auto &v : a1.flat_array()) {
            v.at(0) = rng();
            v.at(1) = rng();
            v.at(2) = rng();
        }

        {
            if (master == comm.rank()) s2 = a1;
            comm.bcast(s2, master);
            ArrayND const tmp{s2};
            XCTAssert(std::equal(a1.flat_array().begin(), a1.flat_array().end(), tmp.flat_array().begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
        }
        a1.fill(T{});
        {
            if (master == comm.rank()) a1 = s2;
            comm.bcast(a1, master);
            ArrayND const tmp{s2};
            XCTAssert(std::equal(a1.flat_array().begin(), a1.flat_array().end(), tmp.flat_array().begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
        }
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }

    // bcast dynamic array
    try {
        using T = UTL::SIMDVector<double, 3>;
        UTL::DynamicArray<T> v1(100), v2(v1);
        for (auto &v : v1) {
            v.at(0) = rng();
            v.at(1) = rng();
            v.at(2) = rng();
        }

        if (master == comm.rank()) v2 = v1;
        comm.bcast(v2, master);
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }

    // bcast static array
    try {
        using T = UTL::SIMDVector<double, 3>;
        UTL::StaticArray<T, 100> v1(100), v2(v1);
        for (auto &v : v1) {
            v.at(0) = rng();
            v.at(1) = rng();
            v.at(2) = rng();
        }

        if (master == comm.rank()) v2 = v1;
        comm.bcast(v2, master);
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }

    // std::vector
    try {
        using T = UTL::SIMDVector<double, 3>;
        std::vector<T> v1(100), v2(v1);
        for (auto &v : v1) {
            v.at(0) = rng();
            v.at(1) = rng();
            v.at(2) = rng();
        }

        if (master == comm.rank()) v2 = v1;
        comm.bcast(v2, master);
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }
}

void testCollective(Comm const &world) {
    world.barrier();

    testCollective_LowLevel(world);
    testCollective_HighLevel(world);
}
