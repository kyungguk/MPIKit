//
//  test-Reduction.cc
//  test-MPIKit
//
//  Created by KYUNGGUK MIN on 8/10/18.
//  Copyright © 2018 kyungguk.com. All rights reserved.
//

#include "test-main.h"
#include <algorithm>
#include <numeric>

using namespace MPK::__2_;

static void testReduction_ReduceOp(Comm const &world) {
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        ReduceOp op1, op2;
        XCTAssert(!op1 && !op2, "");

        op1 = ReduceOp::max();
        XCTAssert(op1 && MPI_MAX == *op1, "");
        op2 = std::move(op1);
        XCTAssert(!op1 && op2 && MPI_MAX == *op2, "");

        op1 = ReduceOp::max();
        XCTAssert(op1 && op1.commutative() && MPI_MAX == *op1, "");
        op1 = ReduceOp::min();
        XCTAssert(op1 && op1.commutative() && MPI_MIN == *op1, "");
        op1 = ReduceOp::plus();
        XCTAssert(op1 && op1.commutative() && MPI_SUM == *op1, "");
        op1 = ReduceOp::prod();
        XCTAssert(op1 && op1.commutative() && MPI_PROD == *op1, "");
        op1 = ReduceOp::logic_and();
        XCTAssert(op1 && op1.commutative() && MPI_LAND == *op1, "");
        op1 = ReduceOp::bit_and();
        XCTAssert(op1 && op1.commutative() && MPI_BAND == *op1, "");
        op1 = ReduceOp::logic_or();
        XCTAssert(op1 && op1.commutative() && MPI_LOR == *op1, "");
        op1 = ReduceOp::bit_or();
        XCTAssert(op1 && op1.commutative() && MPI_BOR == *op1, "");
        op1 = ReduceOp::logic_xor();
        XCTAssert(op1 && op1.commutative() && MPI_LXOR == *op1, "");
        op1 = ReduceOp::bit_xor();
        XCTAssert(op1 && op1.commutative() && MPI_BXOR == *op1, "");
        op1 = ReduceOp::loc_min();
        XCTAssert(op1 && op1.commutative() && MPI_MINLOC == *op1, "");
        op1 = ReduceOp::loc_max();
        XCTAssert(op1 && op1.commutative() && MPI_MAXLOC == *op1, "");

        op1 = ReduceOp(false, [](void *, void *, int *, MPI_Datatype *)->void {});
        XCTAssert(op1 && !op1.commutative(), "");
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }
}

static void testReduction_LowLevel(Comm const &world) {
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }
    Comm const comm = world.duplicated();
    UTL::BitReversedRandomReal rng{2, 1};
    using T = UTL::SIMDVector<double, 3>;
    ReduceOp const op(true, [](void *_a, void *_b, int *n, MPI_Datatype *t) {
        T const *a = static_cast<T const*>(_a);
        T *b = static_cast<T*>(_b);
        for (int i = 0; i < *n; ++i) {
            b[i] += a[i];
        }
    });

    // reduce
    try {
        using T = UTL::SIMDVector<double, 3>;
        UTL::DynamicArray<T> v1(100), v2(v1), v3;
        for (auto &v : v1) {
            v.at(0) = rng();
            v.at(1) = rng();
            v.at(2) = rng();
            UTL::DynamicArray<T> const tmp(comm.size(), v);
            v3.push_back(std::accumulate(tmp.begin(), tmp.end(), T{}));
        }

        comm.reduce(op, v1.data(), v2.data(), v1.size(), master);
        comm.bcast(v2, master);
        XCTAssert(std::equal(v3.begin(), v3.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");

        if (master == comm.rank()) {
            v2 = v1;
            comm.reduce(op, nullptr, v2.data(), v2.size(), master);
        } else {
            comm.reduce(op, v1.data(), nullptr, v1.size(), master);
        }
        comm.bcast(v2, master);
        XCTAssert(std::equal(v3.begin(), v3.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");

        std::fill(v2.begin(), v2.end(), T{});
        comm.reduce(op, v1.data(), v2.data(), v1.size());
        XCTAssert(std::equal(v3.begin(), v3.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");

        v2 = v1;
        comm.reduce(op, v2.data(), v1.size());
        XCTAssert(std::equal(v3.begin(), v3.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }
}

static void testReduction_HighLevel(Comm const &world) {
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }
    Comm const comm = world.duplicated();
    UTL::BitReversedRandomReal rng{2, 1};
    using T = UTL::SIMDVector<double, 3>;
    ReduceOp const op(true, [](void *_a, void *_b, int *n, MPI_Datatype *t) {
        T const *a = static_cast<T const*>(_a);
        T *b = static_cast<T*>(_b);
        for (int i = 0; i < *n; ++i) {
            b[i] += a[i];
        }
    });

    // scalar
    try {
        using T = UTL::SIMDVector<double, 3>;
        T const v1{rng(), rng(), rng()};
        UTL::DynamicArray<T> const tmp(comm.size(), v1);
        T const v3 = std::accumulate(tmp.begin(), tmp.end(), T{});

        T v2;
        comm.reduce(op, v2 = v1);
        XCTAssert(UTL::reduce_bit_and(v3 == v2), "");
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }

    // padded array
    try {
        using T = UTL::SIMDVector<double, 3>;
        UTL::PaddedArray<T, 3> v1(100), v2(v1);
        for (long i = -v1.pad_size(); i < v1.size() + v1.pad_size(); ++i) {
            v1[i].at(0) = rng();
            v1[i].at(1) = rng();
            v1[i].at(2) = rng();
            UTL::DynamicArray<T> const tmp(comm.size(), v1[i]);
            v2.at(i) = std::accumulate(tmp.begin(), tmp.end(), T{});
        }

        comm.reduce(op, v1);
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }

    // ND array
    try {
        using T = UTL::SIMDVector<double, 3>;
        constexpr long ND = 3, Pad = 2;
        using ArrayND = UTL::ArrayND<T, ND, Pad>;
        ArrayND a1({3, 5, 9}), a3(a1), _a2(a1.dims() + 10L);
        ArrayND::size_vector_type const loc{1}, len = a1.dims();
        auto s2 = UTL::make_slice<ArrayND::pad_size()>(_a2, loc, len);

        for (long i = 0; i < a1.flat_array().size(); ++i) {
            a1.flat_array()[i].at(0) = rng();
            a1.flat_array()[i].at(1) = rng();
            a1.flat_array()[i].at(2) = rng();
            UTL::DynamicArray<T> const tmp(comm.size(), a1.flat_array()[i]);
            a3.flat_array().at(i) = std::accumulate(tmp.begin(), tmp.end(), T{});
        }

        {
            comm.reduce(op, s2 = a1);
            ArrayND const tmp{s2};
            XCTAssert(std::equal(a3.flat_array().begin(), a3.flat_array().end(), tmp.flat_array().begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
        }
        {
            comm.reduce(op, a1);
            XCTAssert(std::equal(a1.flat_array().begin(), a1.flat_array().end(), a3.flat_array().begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
        }
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }

    // dynamic array
    try {
        using T = UTL::SIMDVector<double, 3>;
        UTL::DynamicArray<T> v1(100), v2;
        for (auto &v : v1) {
            v.at(0) = rng();
            v.at(1) = rng();
            v.at(2) = rng();
            UTL::DynamicArray<T> const tmp(comm.size(), v);
            v2.push_back(std::accumulate(tmp.begin(), tmp.end(), T{}));
        }

        comm.reduce(op, v1);
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }

    // static array
    try {
        using T = UTL::SIMDVector<double, 3>;
        UTL::StaticArray<T, 100> v1(100), v2;
        for (auto &v : v1) {
            v.at(0) = rng();
            v.at(1) = rng();
            v.at(2) = rng();
            UTL::DynamicArray<T> const tmp(comm.size(), v);
            v2.push_back(std::accumulate(tmp.begin(), tmp.end(), T{}));
        }

        comm.reduce(op, v1);
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }

    // std::vector
    try {
        using T = UTL::SIMDVector<double, 3>;
        std::vector<T> v1(100), v2;
        for (auto &v : v1) {
            v.at(0) = rng();
            v.at(1) = rng();
            v.at(2) = rng();
            UTL::DynamicArray<T> const tmp(comm.size(), v);
            v2.push_back(std::accumulate(tmp.begin(), tmp.end(), T{}));
        }

        comm.reduce(op, v1);
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin(), [](T const &a, T const &b) { return UTL::reduce_bit_and(a == b); }), "");
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }
}

void testReduction(Comm const &world) {
    world.barrier();

    testReduction_ReduceOp(world);
    testReduction_LowLevel(world);
    testReduction_HighLevel(world);
}
